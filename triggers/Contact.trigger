/* Trigger: Contact
** Created On: 09/02/2014
** Created by: OpFocus Team
** Description: When a contact is created for one of the E.G. Sawyer Companies (EG Sawyer, LCN, LCN Networks), 
**              create a new job staffing record for that new contact and associate it to the Unassigned Job for that account.
**              
*/
trigger Contact on Contact (after insert) {
	if (Trigger.isAfter && Trigger.isInsert) {
		Id empRTId = [select Id from RecordType where SObjectType='Contact' and DeveloperName='Employee_Contact' limit 1].Id;
		Set<Id> setAccountIds = new Set<Id>();
		Set<Id> setContactIds = new Set<Id>();
		for (Contact c : Trigger.new) {
			if (c.RecordTypeId == empRTId && c.AccountId != null && c.Primary_Role__c != null && !c.Primary_Role__c.contains('Office ')) {
				setAccountIds.add(c.AccountId);
				setContactIds.add(c.Id);
			}
		}
		if (!setAccountIds.isEmpty()) {
			List<Job_Staffing__c> lstJobStaffingsToCreate = new List<Job_Staffing__c>();
			Map<Id, Account> mapAccountsWJob = new Map<Id, Account>([select Id, (select Id, Job_Status__c from Jobs__r 
																	 where Job_Status__c = 'In Progress - Active' 
																	 and IsUnassigned_Job__c=true limit 1) 
																	 from Account where Id in :setAccountIds]);
			for (Contact c : Trigger.new) {
				if (setContactIds.contains(c.Id) && mapAccountsWJob.containsKey(c.AccountId)) {
					Account a = mapAccountsWJob.get(c.AccountId);
					if (a.Jobs__r != null) {
						for (Job__c j : a.Jobs__r) {
							Job_Staffing__c js = new Job_Staffing__c();
							js.IsActive__c = true;
							js.Employee__c = c.Id;
							js.Role__c     = c.Primary_Role__c;
							js.Job_Name__c = j.Id;

							lstJobStaffingsToCreate.add(js);
						}
					}
				}
			}
			if (!lstJobStaffingsToCreate.isEmpty()) insert lstJobStaffingsToCreate;
		}
	}

}