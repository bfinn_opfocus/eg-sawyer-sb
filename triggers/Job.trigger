/* Trigger: Job
** Created On: 07/16/2014
** Created by: OpFocus Team
** Description: 
**              After Insert and After Update Triggers:
** 
**              + When a Job is created or updated, create share records for the General Foreman
**                (the job is owned by the Project Manager, but the General Foreman needs edit)
**  
**              + When a Job is created or updated and has the Eligible to Merge on Job Board = true, find all the Eligible to Merge on Job Board jobs
**                with the same account, Lead General foreman, and is active, then create Job Staffing to match those jobs.
**  
**              + When a Job is created for Division is LCN Networks, create JobStaffing records for 
**                  any employee whose Employee Division = is LCN Networks.
**
**              + When a Job is created with a valid Job Number create a Contact of type Job
**                Record AND associate Job with newlly created Contact. If Job Number is updated,
**                update the Contact's email address to reflect new Job Number
*/
trigger Job on Job__c (after insert, after update) {

    // --------------------------------------------------------------------------------
    // ---------------- Create a contact for a Job when Job Number is defined 12/1/2014
    // --------------------------------------------------------------------------------
    if (Trigger.isAfter) {
        if (Trigger.isUpdate || Trigger.isInsert) {
            List <Job__Share> lstShares = [select Id, UserOrGroupId, ParentId from Job__Share where ParentId in :trigger.newMap.keySet()];
            List <Job__c> lstJobs = 
            	[select Id, OwnerId, General_Foreman__c, General_Foreman__r.Salesforce_User_Account__c 
            	 from Job__c 
            	 where Id in :trigger.newMap.keySet() and General_Foreman__c != null and General_Foreman__r.Salesforce_User_Account__c != null];
            List <Job__Share> lstUpsertShares = new List <Job__Share> ();
            for (Job__c j : lstJobs) {
            	Job__Share thisShare = null;
                for (Job__Share js : lstShares) {
                    if (js.ParentId == j.Id && js.UserOrGroupId == j.General_Foreman__r.Salesforce_User_Account__c) {
                        thisShare = js;
                        break;
                    }
                }
                if (thisShare == null) {
	                lstUpsertShares.add(new Job__Share(ParentId=j.Id, UserOrGroupId=j.General_Foreman__r.Salesforce_User_Account__c, AccessLevel='edit', RowCause=Schema.Job__Share.RowCause.General_Foreman_Access__c));
                }
            }
            if (lstUpsertShares.size() > 0) {
                upsert lstUpsertShares;
            }
        }
    }


    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            Set<String> setAccountIdsandGFIds = new Set<String>();
            Set<Id> setJobIdsForStaffs        = new Set<Id>();
            Set<Id> setJobIdsForLCN           = new Set<Id>(); // for use in creating JobStaffing records

            // Get the list of Employees for LCN Networks.
            List<Contact> lstLCN_NetworkEmployees = null;

            for (Job__c j : Trigger.new) {
                System.debug('==========> Insert Trigger for Job Id = ' + j.Id + ', j.Eligible_to_Merge_on_Job_Board__c  = ' 
                    + j.Eligible_to_Merge_on_Job_Board__c  + ', j.job_Status__c = ' + j.job_Status__c);
                
                if (j.Eligible_to_Merge_on_Job_Board__c && j.Job_Status__c == 'In Progress - Active') {
                    setAccountIdsandGFIds.add(j.Account_and_Foreman__c);
                    setJobIdsForStaffs.add(j.Id);
                }
                // need to keep track of all Jobs for LCN Network so we can create JobStaffing Records to go with them
                if (j.Division__c == 'LCN Networks') {
                    // get the list of LCN Networks Employees now that we know we have Job for that division
                    if (lstLCN_NetworkEmployees == null) {
                        lstLCN_NetworkEmployees = [SELECT Id, Primary_Role__c FROM Contact where Employee_Type__c='LCN Networks' and Employee_Status__c='Active'];
                    }
                    setJobIdsForLCN.add(j.Id);
                }
            }

            // We will create Job Staffings for the Job from the Job Staffings in the map.
            // The map is indexed by Employee Id, so we won't create a new Job Staffings for the same employee twice.
            Map<String, Job_Staffing__c> mapStaffsForNewJobs = new Map<String, Job_Staffing__c>();
            // TODO: remove Job_Number__c as it is only here for debugging
            List<Job__c> lstMergeJobs = [select Id, Job_Number__c, Account_Name__c, General_Foreman__c, Account_and_Foreman__c,
                                                    (select Id, Job_Name__c, IsActive__c, Employee__c, Role__c from Job_Staffing__r where IsActive__c = true)
                                                     from Job__c
                                                     where Account_and_Foreman__c in :setAccountIdsandGFIds
                                                       and Eligible_to_Merge_on_Job_Board__c = true
                                                       and Job_Status__c = 'In Progress - Active'];
            // We need a separate container for the Job Staffing objects created for LCN Networks as there will potentially be
            // multiple objects created for the same Employee record as a JobStaffing record is created for each employee in LCN Networks 
            // division. Then we will merge this set with our map before submitting to database.
            Set<Job_Staffing__c> setJobStaffForLCNNetwork = new Set<Job_Staffing__c>();
            for (Job__c j : Trigger.new) {

                Boolean jobStaffingCreated = false;
                if (setJobIdsForStaffs.contains(j.Id)) {    
                    // Get all the jobs that match the Job's Account and General foreman and is 
                    // Eligible to Merge on Job Board with all the active Staffings
                    for (Job__c mergeJob : lstMergeJobs) {
                        if (j.Account_and_Foreman__c == mergeJob.Account_and_Foreman__c && mergeJob.Job_Staffing__r.size()>0) {
                            // for each Job_Staffing record associated with the mergeJob, create a new JobStaffing record
                            for (Job_Staffing__c js : mergeJob.Job_Staffing__r) {
                                // If this job is also for the LCN Networks Division, then we use different settings for new JobStaffing object
                                if (setJobIdsForLCN.contains(j.Id)) {

                                    System.debug('===== Adding Jobstaffing records for Job ' + j.Id + ', and each LCN Network Employee.');
                                    // Default settings for LCN Networks Job Staffing record
                                    // Create a JobStaffing record for EACH LCN Networks employee           
                                    for (Contact lcnNetworkEmployee : lstLCN_NetworkEmployees) {
                                        // Creating a new JobStaffing record, using mergeJob's JobStaffing IsActive flag 
                                        // rather than default of false as this job needs to be available on the Job Board.
                                        Job_Staffing__c newJs = new Job_Staffing__c(Job_Name__c=j.Id, Employee__c=lcnNetworkEmployee.Id, 
                                            Role__c=lcnNetworkEmployee.Primary_Role__c, IsActive__c=js.IsActive__c);
                                        setJobStaffForLCNNetwork.add(newJs);
                                    }
                                    // now mark our flag so we do not do create another record
                                    jobStaffingCreated = true;
                                } else { // default settings for Eligible For Job Board and General Foreman Lead

                                    System.debug('===== Adding Jobstaffing record for Job ' + j.Id + ', and Employee = ' + js.Employee__c);
                                    Job_Staffing__c newJs = Utils.createNewStaffing(js);
                                    newJs.Job_Name__c     = j.Id;
                                    mapStaffsForNewJobs.put(newJs.Employee__c, newJs);
                                }
                            }
                        }
                    }
                }
                if (!jobStaffingCreated && setJobIdsForLCN.contains(j.Id)) {
                    for (Contact lcnNetworkEmployee : lstLCN_NetworkEmployees) {
                        Job_Staffing__c newJs = new Job_Staffing__c(Job_Name__c=j.Id, 
                            Employee__c=lcnNetworkEmployee.Id, Role__c=lcnNetworkEmployee.Primary_Role__c, IsActive__c=false);
                        setJobStaffForLCNNetwork.add(newJs);
                    }
                }
            }

            // if we have JobStaffing Records in both our Set and our Map, combine them.
            if (!setJobStaffForLCNNetwork.isEmpty() && !mapStaffsForNewJobs.isEmpty()) {
                // add Map and Set values together and perform a singal insert into database
                List<Job_Staffing__c> combinedValues = new List<Job_Staffing__c>();
                combinedValues.addAll(mapStaffsForNewJobs.values());
                combinedValues.addAll(setJobStaffForLCNNetwork);
                
                insert combinedValues;
            } else if (!mapStaffsForNewJobs.isEmpty()) { // LCN Network Set is empty, just add Map values
                insert mapStaffsForNewJobs.values();
            } else if (!setJobIdsForLCN.isEmpty()) { // Map for Merged Job is empty, just use LCN Network Set
                // copy our set over to a list so we can insert it. Needed to use a Set above as we needed contains() functionality
                List<Job_Staffing__c> lstJobStaffForLCN = new List<Job_Staffing__c>();
                lstJobStaffForLCN.addAll(setJobStaffForLCNNetwork);
                insert lstJobStaffForLCN;
            }

        }

        if (Trigger.isUpdate) {
            
            Set<String> setAccountIdsandGFIds                   = new Set<String>();
            Set<Id> setJobIdsForStaffs                          = new Set<Id>();
            Map<Id, Map<Id, Job_Staffing__c>> mapStaffingsByJob = new Map<Id, Map<Id, Job_Staffing__c>>();
            
            for (Job__c j : Trigger.new) {
                // When the Job is changed to Eligible to Merge or become active again
                if (j.Eligible_to_Merge_on_Job_Board__c
                    && j.Job_Status__c == 'In Progress - Active' 
                    && (!Trigger.oldMap.get(j.Id).Eligible_to_Merge_on_Job_Board__c || Trigger.oldMap.get(j.Id).Job_Status__c != 'In Progress - Active')) {

                    setAccountIdsandGFIds.add(j.Account_and_Foreman__c);
                    setJobIdsForStaffs.add(j.Id);
                    if (!mapStaffingsByJob.containsKey(j.Id)) {
                        mapStaffingsByJob.put(j.Id, new Map<Id, Job_Staffing__c>());
                    }
                }
            }
            System.debug('===== setAccountIdsandGFIds: ' + setAccountIdsandGFIds.size());

            // Get a map of the current staffings of the job that being updated
            List<Job_Staffing__c> lstCurrentStaffs = [select Id, Job_Name__c, IsActive__c, Employee__c, Role__c from Job_Staffing__c where Job_Name__c in :setJobIdsForStaffs];
            for (Job_Staffing__c js : lstCurrentStaffs) {
                if (!mapStaffingsByJob.containsKey(js.Job_Name__c)) mapStaffingsByJob.put(js.Job_Name__c, new Map<Id, Job_Staffing__c>());
                mapStaffingsByJob.get(js.Job_Name__c).put(js.Employee__c, js);
            }
            // We will create Job Staffings for the Job from the Job Staffings in the map.
            // The map is indexed by Employee Id, so we won't create a new Job Staffings for the same employee twice.
            Map<String, Job_Staffing__c> mapStaffsForUpdatedJobs = new Map<String, Job_Staffing__c>();
            Map<String, Job_Staffing__c> mapCurrentStaffsToupdate = new Map<String, Job_Staffing__c>();
            List<Job__c> lstMergeJobs = [select Id, Account_Name__c, General_Foreman__c, Account_and_Foreman__c,
                                                    (select Id, Job_Name__c, IsActive__c, Employee__c, Role__c from Job_Staffing__r where IsActive__c = true)
                                                     from Job__c
                                                     where Account_and_Foreman__c in :setAccountIdsandGFIds
                                                       and Eligible_to_Merge_on_Job_Board__c = true
                                                       and Job_Status__c = 'In Progress - Active'];
            for (Job__c j : Trigger.new) {
                if (setJobIdsForStaffs.contains(j.Id)) {                    
                    // Get all the jobs that match the Job's Account and General foreman and is Eligible to Merge on Job Board
                    // with all the active Staffings
                    for (Job__c mergeJob : lstMergeJobs) {
                        System.debug('===== checking mergeJob: '+mergeJob.Id+', with staffings: '+mergeJob.Job_Staffing__r.size());

                        if (j.Account_and_Foreman__c == mergeJob.Account_and_Foreman__c 
                            && mergeJob.Job_Staffing__r.size() > 0
                            && mapStaffingsByJob.containsKey(j.Id)) {

                            Map<Id, Job_Staffing__c> mapCurrentStaffs = new Map<Id, Job_Staffing__c>();
                            mapCurrentStaffs = mapStaffingsByJob.get(j.Id);
                            for (Job_Staffing__c js : mergeJob.Job_Staffing__r) {
                                // If the job doesn't already have this same staff as the merge job, then create it
                                if (!mapCurrentStaffs.containsKey(js.Employee__c)) {
                                    Job_Staffing__c newJs = Utils.createNewStaffing(js);
                                    newJs.Job_Name__c     = j.Id;
                                    mapStaffsForUpdatedJobs.put(newJs.Employee__c, newJs);
                                }
                                // If the job already has the same staff as the merge job but it's inactive, then reactivate it
                                else if (mapCurrentStaffs.containsKey(js.Employee__c) && !mapCurrentStaffs.get(js.Employee__c).IsActive__c) {
                                    Job_Staffing__c currentStaff = mapCurrentStaffs.get(js.Employee__c);
                                    currentStaff.IsActive__c = true;
                                    mapCurrentStaffsToupdate.put(currentStaff.Id, currentStaff);
                                }
                            }
                        }
                    }
                }
            }
            if (!mapStaffsForUpdatedJobs.isEmpty()) insert mapStaffsForUpdatedJobs.values();
            if (!mapCurrentStaffsToupdate.isEmpty()) update mapCurrentStaffsToupdate.values();
        }
    }

    // --------------------------------------------------------------------------------
    // ---------------- Create a contact for a Job when Job Number is defined 12/1/2014
    // --------------------------------------------------------------------------------
    if (Trigger.isAfter) {
        if (Trigger.isUpdate || Trigger.isInsert) {

            // When the job's Job Number field is defined we create a Contact for the Job
            // When the job's Job Number has changed, update existing Contact with new email address
            //      which is set to JobNumber@egsawyer.com.
            List<Contact> lstExistingContacts = [select Id, Email, Job__c from Contact where Job__c in :Trigger.new];

            // list of contacts to create or update for affected Jobs.
            List<Contact> lstContactsToUpsert = new List<Contact>();
            
            // get the Id for Job Record Contact Record Type
            RecordType jobRecordType = [select Id from RecordType where Name='Job Record' and SObjectType='Contact'];

            // Get the EG Sawyer Job Account that we are going to associate the new Contacts with
            Account egSawyerJobAcct = [select Id from Account where Name='EG Sawyer Job Emails' limit 1];


            for (Job__c j : Trigger.new) {
                System.debug('==========> Checking to see if Job_Number__c for Job_Number__c = ' + j.Job_Number__c);

                if (j.Job_Number__c != null) {

                    // since we are going to use the Job_Number__c field as the email address, we need to sanitize it,
                    // ensuring there are no spaces or other illegal characters for an email. Replace all spaces with a _
                    String emailAddress = j.Job_Number__c.replaceAll('[ ]', '_') + '@egsawyer.com';

                    // Either this is an insert and Job_Number is defined OR this is an Update and 
                    // Job_Number__c has changed from previous value. 
                    if (Trigger.isInsert || 
                        (Trigger.isUpdate && j.Job_Number__c != Trigger.oldMap.get(j.Id).Job_Number__c)) {
                     
                        // see if we have an existing Contact for this job already
                        boolean bFoundContact = false; 
                        for (Contact contact : lstExistingContacts) {
                            if (contact.Job__c == j.Id) {
                                System.debug('==========> Found Contact for Job = ' + j.Id + ', updating Email to: ' +
                                        emailAddress);
                                
                                // we have an existing contact - update the email address and add to list.
                                contact.Email = emailAddress;
                                lstContactsToUpsert.add(contact);
                                bFoundContact = true;
                                break;
                            }
                        }
                        // if we did not find a contact for our job, then create one now as either this is
                        // and insert trigger OR it is an update trigger and the create trigger did not 
                        // create one as Job_Number__c was not defined yet.
                        if (!bFoundContact) {
                            // create a new Contact of type Job Record for every new Job that is created.
                            // Define the following attributes:
                            //    1. Last Name = Job Name
                            //    2. Job: The job that triggered the record creation
                            //    3. Email: <job.Number>@egsawyer.com (this is guaranteed to be unique as Job Number is unique)
                            //    4. Account - same Account as Job
                            System.debug('==========> Creating Contact for Job = ' + j.Id + ' with Email : ' +
                                            emailAddress);
                            Contact contact = new Contact(RecordTypeId=jobRecordType.Id, 
                                AccountId=egSawyerJobAcct.Id, LastName=j.Name, 
                                Job__c=j.Id, Email=emailAddress);
                            lstContactsToUpsert.add(contact);
                        }
                    }
                }
            }
            // Add or Update Contacts we worked on above
            if (!lstContactsToUpsert.isEmpty()) upsert lstContactsToUpsert;

            // Now , we can update the Job's
            // Job_Contact_Record__c with the Contact's Id and update the record in the database.

            // Now that the Contacts have been created or updated  in the database, 
            // update the Job's Job_Contact_Record__c field to reference new contact.
            // NOTE: We need to create a separate Job object in memory and update that rather than
            //       updating the Job in the Trigger map as this is an after trigger and that is not allowed.

            List<Job__c> lstJobsToUpdate = new List<Job__c>();
            for (Contact contact : lstContactsToUpsert) {
                // now create job for this contact setting Id to Contact's Job__c and 
                // Contact_Record__c to Contact. Then add to list of jobs to update.
                Job__c job = new Job__c(Id = contact.Job__c, Job_Contact_Record__c = contact.Id);
                lstJobsToUpdate.add(job);   
            }

            // Update Jobs to reference new Contact email address
            if (!lstJobsToUpdate.isEmpty()) update lstJobsToUpdate;
        }
    }
}