/* Trigger: JobQuote
** Created On: 08/28/2015
** Created by: OpFocus Team
** Description: 
**     ++ afterUpdate When a Job Quote is updated with a Status of Approved, we will update
**		the associated Job Change Order fields (Phase 1 Change Order Materials, Phase 1
**		Change Order Equipment, etc) by adding the Job Quote's related Phase fields to them
** 
*/
trigger JobQuote on Job_Quote__c (after insert, after update) {
	// TODO : Do we need to worry about an insert trigger?
	if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
		Map<Id, List<Job_Quote__c>> mapJobQuotesByJobId = new Map<Id, List<Job_Quote__c>>();
		for (Job_Quote__c jobQuote : Trigger.new) {
			String oldQuoteStatus = null; // for insert case
			if (Trigger.isUpdate) {
				oldQuoteStatus = Trigger.oldMap.get(jobQuote.Id).Quote_Status__c;
			}

			if (jobQuote.Quote_Status__c != null)
				if ((Trigger.isInsert &&  jobQuote.Quote_Status__c.equals(JobQuoteTriggerHelper.AWARDED)) ||
				    (Trigger.isUpdate && jobQuote.Quote_Status__c.equals(JobQuoteTriggerHelper.AWARDED) && 
				    (oldQuoteStatus == null || !oldQuoteStatus.equals(JobQuoteTriggerHelper.AWARDED)))) {
						if (mapJobQuotesByJobId.get(jobQuote.Job__c) == null) {
							mapJobQuotesByJobId.put(jobQuote.Job__c, new List<Job_Quote__c>());
						}
						mapJobQuotesByJobId.get(jobQuote.Job__c).add(jobQuote);
			}
		}
		if (!mapJobQuotesByJobId.isEmpty()) {
			JobQuoteTriggerHelper.updateJobChangeOrderFields(mapJobQuotesByJobId);
		}
	}
}