/* Trigger: Job_Staffing
** Created On: 07/01/2014
** Created by: OpFocus Team
** Description: When a job statffing (js) is created or updated (to active) and it is in a job that has eligible to merge on jobboard = true 
**              the trigger will find other jobs that have eligible to merge on jobboard = true, same account as the js's job account 
**              same general foreman as the js's job general foreman and do the following
**              ++ If the eligible to combine jobs are found, the trigger will check if they already have a job staffing with the same person as 
**                 the js.
**                 -- If there is no current job staffing, the trigger will create a new job staffing for the eligible to combine jobs
**                 -- If there is a match for job staffing employee, the trigger will reactivate it if it is inactive.
**              When a job staffing (js) is updated for active to inactive then find other jobs with eligible to merge on jobboard = true, same account and 
**              same General foreman, then update their staffing that has the same employee as the js employee to active (if it is inactive)
*/
trigger Job_Staffing on Job_Staffing__c (after insert, after update) {
	if (Trigger.isAfter) {
		if ((Trigger.isInsert || Trigger.isUpdate) && !Statics.isInJobStaffingTrigger) {
			// Remember that the Job_Staffing trigger is running
			Statics.isInJobStaffingTrigger = true;
			set<Id> setStaffIds               = new Set<Id>();
			Set<Id> setStaffContactIds        = new Set<Id>();
			// A set of AccountIds and General Foreman Ids that we will use to find the eligible to combine Jobs
			Set<String> setAccountIdsandGFIds = new Set<String>();
			for (Job_Staffing__c js : Trigger.new) {
				if (js.JobIsActive__c && js.Job_Is_Eligible_to_Merge__c && ((Trigger.isInsert && js.IsActive__c) || 
					(Trigger.isUpdate && Trigger.oldmap.get(js.Id).IsActive__c != js.IsActive__c))) {
					setStaffIds.add(js.Id);
					if (js.Employee__c != null) setStaffContactIds.add(js.Employee__c);
					if (js.Job_AccountId__c != null && js.Job_General_ForemanId__c != null) {
						setAccountIdsandGFIds.add(js.Job_AccountId__c+'//'+js.Job_General_ForemanId__c);
					}
				}
			}

			List<Job_Staffing__c> lstStaffingsToCreate = new List<Job_Staffing__c>();
			Map<Id, Job_Staffing__c> mapStaffingsToUpdate = new Map<Id, Job_Staffing__c>();

			if (!setAccountIdsandGFIds.isEmpty()) {
				List<Job__c> lstJobsToAddStaff = [select Id, Account_Name__c, General_Foreman__c, Account_and_Foreman__c,
											(select Id, Job_Name__c, IsActive__c, Employee__c from Job_Staffing__r)
											from Job__c
											where  Account_and_Foreman__c in :setAccountIdsandGFIds
											and    Eligible_to_Merge_on_Job_Board__c = true
											and    Job_Status__c = 'In Progress - Active'];
				for (Job_Staffing__c js : Trigger.new) {
					String accIdandGFId = js.Job_AccountId__c+'//'+js.Job_General_ForemanId__c;
					// When Job Staffing is active
					if (setStaffIds.contains(js.Id)) {
						// If the staffing is active
						if (js.IsActive__c) {
							// Get the jobs that are eligible to combine
							for (Job__c job : lstJobsToAddStaff) {
								if (js.Job_Name__c != job.Id && accIdandGFId == job.Account_and_Foreman__c) {
									// If the match job to combine doesn't have any job staffing, create a new one that has the same value as the js
									if (job.Job_Staffing__r.size() == 0) {
										Job_Staffing__c newJs = Utils.createNewStaffing(js);
										newJs.Job_Name__c     = job.Id;
										lstStaffingsToCreate.add(newJs);
									}
									// If the match job to combine has job staffigns
									else if (job.Job_Staffing__r.size() > 0) {
										// A map of Jobs that eligible to combine indexed by EmployeeId
										Map<Id, Job_Staffing__c> mapCurrentActiveStaffsByEmployeeId = new Map<Id, Job_Staffing__c>();
										for (Job_Staffing__c currentStaff : job.Job_Staffing__r) {
											mapCurrentActiveStaffsByEmployeeId.put(currentStaff.Employee__c, currentStaff);
										}
										// If the job staffing has the same employee and it is inactive, then reactivate it
										if (mapCurrentActiveStaffsByEmployeeId.containsKey(js.Employee__c)) {
											Job_Staffing__c currentJs = mapCurrentActiveStaffsByEmployeeId.get(js.Employee__c);
											if (!currentJs.IsActive__c) {
												currentJs.IsACtive__c = true;
												mapStaffingsToUpdate.put(currentJs.Id, currentJs);
											}
										}
										// If the job doesn't have the staff with the same employee, then create a new staffing for it									
										else {
											if (js.Job_Name__c != job.Id && accIdandGFId == job.Account_and_Foreman__c) {
												Job_Staffing__c newJs = Utils.createNewStaffing(js);
												newJs.Job_Name__c     = job.Id;
												lstStaffingsToCreate.add(newJs);
											}
										}
									}
							
								}	
							}
						}
						// If the staffing is changed to inactive
						if (Trigger.isupdate && !js.IsActive__c) {
							// Find all the job staffings in the same cobine jobs, and update them to inactive 
							for (Job_Staffing__c currentStaffings : [select Id, Employee__c, IsActive__c, Job_Name__r.Account_and_Foreman__c from Job_Staffing__c 
								                                     where  Job_Name__r.Eligible_to_Merge_on_Job_Board__c = true 
								                                     and    Job_Name__r.Account_and_Foreman__c in :setAccountIdsandGFIds
								                                     and    IsActive__c = true and JobIsActive__c = true]) {
								if (currentStaffings.Job_Name__r.Account_and_Foreman__c == accIdandGFId && currentStaffings.Id != js.Id &&
									js.Employee__c == currentStaffings.Employee__c) {
									currentStaffings.IsActive__c = false;
									mapStaffingsToUpdate.put(currentStaffings.Id, currentStaffings);
								}
							}
						}
					}
				}
			}
			if (!mapStaffingsToUpdate.isEmpty()) update mapStaffingsToUpdate.values();
			if (!lstStaffingsToCreate.isEmpty()) insert lstStaffingsToCreate;
		}
		Statics.isInJobStaffingTrigger = false;
	}

}