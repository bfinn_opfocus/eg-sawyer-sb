/* Trigger: WeeklyJobCost
** SObject: Weekly_Job_Cost__c
** Created On: 03/27/2015
** Created by: OpFocus Team
** Description: 
**				After Insert/Update/Delete Triggers:
** 
**              + When a find the latest Weekly_Job_Cost for each job and copy utilization fields from it to the job
*/
trigger WeeklyJobCost on Weekly_Job_Cost__c (after delete, after insert, after update) {

	if (trigger.isAfter && (trigger.isInsert || trigger.isUpdate || trigger.isDelete)) {
		
		Set <Id> setJobIds = new Set <Id> ();
		List <Weekly_Job_Cost__c> lstWJC = (trigger.isDelete ? trigger.old : trigger.new);
		for (Weekly_Job_Cost__c wjc : lstWJC) {
			setJobIds.add(wjc.Job_Name__c);
		}
		
		List <Weekly_Job_Cost__c> lstAllWJCs = 
			[
				select 
					Id, Total_Materials__c, Total_Labor__c, Total_Labor_Dollars__c, Total_Equipment__c, Total_Costs__c, Sub_Contractor_Costs__c, 
					Phase_5_Materials__c, Phase_5_Labor__c, Phase_5_Labor_Dollars__c, Phase_5_Equipment__c, 
					Phase_4_Materials__c, Phase_4_Labor__c, Phase_4_Labor_Dollars__c, Phase_4_Equipment__c, 
					Phase_3_Materials__c, Phase_3_Labor__c, Phase_3_Labor_Dollars__c, Phase_3_Equipment__c, 
					Phase_2_Materials__c, Phase_2_Labor__c, Phase_2_Labor_Dollars__c, Phase_2_Equipment__c, 
					Phase_1_Materials__c, Phase_1_Labor__c, Phase_1_Labor_Dollars__c, Phase_1_Equipment__c, 
					Phase_16_Labor_Hours__c, Name, LastModifiedDate, Job_Number__c, Job_Name__c, Job_Name__r.Name, DJE_Costs__c 
				from Weekly_Job_Cost__c
				where Job_Name__c in :setJobIds
				order by Job_Name__c, LastModifiedDate asc
			];
		Map <Id, Weekly_Job_Cost__c> mapJobIdWJC = new Map <Id, Weekly_Job_Cost__c> ();
		for (Weekly_Job_Cost__c wjc : lstAllWJCs) {
			mapJobIdWJC.put(wjc.Job_Name__c, wjc);
			//system.debug('===>>> adding job: Job_Name__r.Name: '+wjc.Job_Name__r.Name+', wjc: '+wjc.Name );
		}

		List <Job__c> lstJobs = new List <Job__c> ();
		for (Id jobId : setJobIds) {
			Weekly_Job_Cost__c wjc = mapJobIdWJC.get(jobId);
			if (wjc != null) {
				lstJobs.add(
						new Job__c(
							Id = jobId,
							Phase_5_Utilized_Materials__c = wjc.Phase_5_Materials__c, 
							Phase_5_Utilized_Labor__c = wjc.Phase_5_Labor__c, 
							Phase_5_Utilized_Labor_Dollars__c = wjc.Phase_5_Labor_Dollars__c, 
							Phase_5_Utilized_Equipment__c = wjc.Phase_5_Equipment__c, 
							Phase_4_Utilized_Materials__c = wjc.Phase_4_Materials__c, 
							Phase_4_Utilized_Labor__c = wjc.Phase_4_Labor__c, 
							Phase_4_Utilized_Labor_Dollars__c = wjc.Phase_4_Labor_Dollars__c, 
							Phase_4_Utilized_Equipment__c = wjc.Phase_4_Equipment__c, 
							Phase_3_Utilized_Materials__c = wjc.Phase_3_Materials__c, 
							Phase_3_Utilized_Labor__c = wjc.Phase_3_Labor__c, 
							Phase_3_Utilized_Labor_Dollars__c = wjc.Phase_3_Labor_Dollars__c, 
							Phase_3_Utilized_Equipment__c = wjc.Phase_3_Equipment__c, 
							Phase_2_Utilized_Materials__c = wjc.Phase_2_Materials__c, 
							Phase_2_Utilized_Labor__c = wjc.Phase_2_Labor__c, 
							Phase_2_Utilized_Labor_Dollars__c = wjc.Phase_2_Labor_Dollars__c, 
							Phase_2_Utilized_Equipment__c = wjc.Phase_2_Equipment__c, 
							Phase_1_Utilized_Materials__c = wjc.Phase_1_Materials__c, 
							Phase_1_Utilized_Labor__c = wjc.Phase_1_Labor__c, 
							Phase_1_Utilized_Labor_Dollars__c = wjc.Phase_1_Labor_Dollars__c, 
							Phase_1_Utilized_Equipment__c = wjc.Phase_1_Equipment__c,
							Total_Utilized_Sub_Contractor_Amount__c = wjc.Sub_Contractor_Costs__c,
							Total_Utilized_Direct_Job_Expenses__c = wjc.DJE_Costs__c
						)
					);				

			}
		}
		
		if (lstJobs.size() > 0) {
			update lstJobs;
		}
		
	}


}