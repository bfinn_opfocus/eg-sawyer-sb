/* Trigger: Time_Entry
** Created On: 02/27/2015
** Created by: OpFocus Team
** Description: Populate the lookup to Job when a Time Entry is inserted/updated
**              
*/
trigger Time_Entry on Time_Entry__c (before insert, before update) {

	if (trigger.isBefore && (trigger.isInsert || trigger.isUpdate)) {
		Set <Id> setJobStaffingIDs = new Set <Id> ();
		for (Time_Entry__c te : trigger.new) {
			setJobStaffingIDs.add(te.Job_Staffing__c);
		}
		Map <Id, Job_Staffing__c> mapJSIDJobStaffing = new Map <Id, Job_Staffing__c> ([select Job_Name__c from Job_Staffing__c where Id in :setJobStaffingIDs]);
		for (Time_Entry__c te : trigger.new) {
			te.Job__c = mapJSIDJobStaffing.get(te.Job_Staffing__c).Job_Name__c;
		}
	}

}