/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_OrderMaterials {

    static testMethod void testOrder() {

		SlipAdjustmentSettings__c settings = new SlipAdjustmentSettings__c();
		settings.ProfilesPricingVisible__c = 'EG Sawyer Executive,EG Sawyer Project Manager,EG Sawyer Purchasing,EG Sawyer System Administrator,LCN Networks Executive,LCN Networks Project Manager,LCN Networks Purchasing,LCN System Administrator,System Administrator';
		insert settings;

        Account a1 = UnitTestUtils.CreateAccount('Test Co1');
        Account a2 = UnitTestUtils.CreateAccount('Test Co2');
        Account specialAcct = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');

        insert new List<Account>{a1, a2, specialAcct};

        Contact emp1 = UnitTestUtils.createContact('testlastname1', a1.Id, '7098908909', empRtId);
        Contact emp2 = UnitTestUtils.createContact('testlastname2', a1.Id, '7778905555', empRtId);
        Contact emp3 = UnitTestUtils.createContact('testlastname3', a1.Id, '5558888909', empRtId);
        Contact emp4 = UnitTestUtils.createContact('testlastname4', a1.Id, '4442221234', empRtId);
        emp4.Project_Manager__c = true;
        //emp4.Employee_Status__c = 'Active';
        Contact emp5 = UnitTestUtils.createContact('testlastname5', a1.Id, '4442220000', empRtId);
        emp5.Primary_Role__c = 'General Foreman';
        emp5.General_Foreman__c = true;
        emp5.Employee_Status__c = 'Active';
        insert new List<Contact>{emp1, emp2, emp3, emp4, emp5};

        Opportunity opp1 = UnitTestUtils.createOpp('opp1', a1.Id);
        Opportunity opp2 = UnitTestUtils.createOpp('opp2', a1.Id);
        Opportunity opp3 = UnitTestUtils.createOpp('opp3', a1.Id);
        Opportunity opp4 = UnitTestUtils.createOpp('opp4', a1.Id);
        opp1.Type = 'E.G. Sawyer';
        opp2.Type = 'LCN Networks';
        insert new List<Opportunity>{opp1, opp2, opp3, opp4};

        Job__c job1 = UnitTestUtils.createJob('Job1', a1.Id, opp1.Id, 'a1 job1');
        job1.Job_Short_Name__c = 'job1';
        //job1.Project_Manager__c = emp4.Id;
        job1.General_Contractor__c = a2.Id;
        job1.General_Foreman__c    = emp5.Id;
        job1.Eligible_to_Merge_on_Job_Board__c = true;
        job1.Job_Status__c = 'In Progress - Active';
        insert job1;

        Job_Staffing__c js1 = UnitTestUtils.createJS(job1.Id, 'General Foreman', emp1.Id, true);
        Job_Staffing__c js2 = UnitTestUtils.createJS(job1.Id, 'Foreman',      emp2.Id, true);
        Job_Staffing__c js3 = UnitTestUtils.createJS(job1.Id, 'Technician',   emp3.Id, true);
        insert new List<Job_Staffing__c>{js1, js2, js3};

		
		// Every Product has to have a Standard Price that is part of the Standard Price Book before you can create any additional prices in other Price Books
		ID standardPBID = Test.getStandardPricebookId();
		
		Pricebook2 pb = new Pricebook2(IsActive=true, Name = 'E.G. Sawyer');
		insert pb;
		
		List <Product2> lstProd = new List <Product2> {
			new Product2(Name='test', IsActive=true, Family='widgets', Product_Type__c='type', Product_Sub_Type__c='subtype'),
			new Product2(Name='testlongname-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahhhhhhhhhhhhhhhhhhhhhhhhhhhhhh', IsActive=true, Family='widgets', Product_Type__c='type', Product_Sub_Type__c='subtype')
		};
		insert lstProd;
		
		List <PricebookEntry> lstStdPBE = new List <PricebookEntry> {
			new PricebookEntry(Pricebook2Id=standardPBID, Product2Id=lstProd[0].Id, UnitPrice=10),
			new PricebookEntry(Pricebook2Id=standardPBID, Product2Id=lstProd[1].Id, UnitPrice=10)
		};
		insert lstStdPBE;
		
		List <PricebookEntry> lstPBE = new List <PricebookEntry> {
			new PricebookEntry(Pricebook2Id=pb.Id, Product2Id=lstProd[0].Id, UnitPrice=10),
			new PricebookEntry(Pricebook2Id=pb.Id, Product2Id=lstProd[1].Id, UnitPrice=10)
		};
		insert lstPBE;
		

        Order theOrder = new Order(AccountId=a1.Id, Job_Name__c=job1.Id, EffectiveDate=Date.today(), Status='Draft', Pricebook2Id=pb.Id);
        insert theOrder;

		List <OrderItem> lstOI = new List <OrderItem> {
			new OrderItem(OrderId=theOrder.Id, PricebookEntryId=lstPBE[0].Id, Quantity=1.0, UnitPrice=10.0),
			new OrderItem(OrderId=theOrder.Id, PricebookEntryId=lstPBE[1].Id, Quantity=1.0, UnitPrice=10.0)
		};
		insert lstOI;


        Test.startTest();
        
        PageReference pg = Page.OrderMaterials;
        Test.setCurrentPage(pg);
        
        OrderMaterials_Controller ctl = new OrderMaterials_Controller(new ApexPages.StandardController(theOrder));
        
        String custJson = '';
        //OrderMaterials_Controller.createCustomProduct(custJson);

        String saveJson = 
        	'{"oid":"' + theOrder.Id + '",'
        	+ '"materials":['
        	+ '{"deleted":false,"sfid":"' + lstOI[0].Id + '","pbeid":"' + lstPBE[0].Id + '","family":"Assemblies","ptype":"Novartis","subtype":"Overtime - Direct for Novartis","name":"(OT) Dual Cat6 Patch Cable - 60\'","pcount":"6","std":"standard","dateneeded":"08/13/2015"},'
        	+ '{"deleted":false,"sfid":"' + lstOI[1].Id + '","pbeid":"' + lstPBE[1].Id + '","family":"Assemblies","ptype":"MIT","subtype":"null","name":"Other - Assemblies, MIT","pcount":"2","std":"standard","dateneeded":"08/13/2015"}'
        	+ ']}';
        OrderMaterials_Controller.save(saveJson);

        OrderMaterials_Controller.saveAll(saveJson);

		OrderMaterials_Controller.isTest = true;
        OrderMaterials_Controller.submitOrder(saveJson);

 
        Test.stopTest();    
    }

    private static Id empRtId;
    
    static {
        empRtId = [Select Id, sObjectType, Name From RecordType where SObjectType = 'Contact' and Name='Employee Contact' limit 1].Id;  
    } 


}