/*
** Class:  SlipAdjustments_Controller
** Created by OpFocus on 7/24/2014
** Description: Controller for the SlipAdjustments VF page
*/
public with sharing class SlipAdjustments_Controller {

	public Slip__c theSlip {get; set;}
	public Boolean showPricing {get; set;}
	public String serviceDate {get; set;}
	public String slipNameLink {get; set;}
	public String jobNameLink {get; set;}
	public String laborData {get; set;}
	public String employeeData {get; set;}
	public String productData {get; set;}
	public String productDataFamily {get; set;}
	public String productDataType {get; set;}
	public String productDataSubtype {get; set;}
	public String productDataSize {get; set;}
	public String tax {get; set;}
	public String materials {get; set;}
	public String otherData {get; set;}
	private ApexPages.Standardcontroller std;
	
	public SlipAdjustments_Controller(ApexPages.Standardcontroller std) {
		this.std = std;
		theSlip = [select 
					Id, Name, Slip_Status__c, Job_Name__c, Job_Name__r.Name, Certified_Payroll__c,
					Job_Name__r.Job_Number__c, Job_Name__r.Division__c, 
					T_M_Labor_Markup__c, T_M_Materials_Markup__c, T_M_Subcontractor_Markup__c, 
					Work_Ordered_By__r.Name
				   from Slip__c 
				   where Id = :std.getId()];
		system.debug('===>>> id: '+theSlip.Id+', div: '+theSlip.Job_Name__r.Division__c);

		slipNameLink = (Utils.isSF1() ? 'javascript:getLink(\''+theSlip.Id+'\');' : '/' + theSlip.Id);
		jobNameLink = (Utils.isSF1() ? 'javascript:getLink(\''+theSlip.Job_Name__c+'\');' : '/' + theSlip.Job_Name__c);

		Set <String> setProfileNames = new Set <String> ();
		SlipAdjustmentSettings__c settings = SlipAdjustmentSettings__c.getInstance();
	    if (settings != null 
	    	&& settings.ProfilesPricingVisible__c != null) {
	    		for (String s : settings.ProfilesPricingVisible__c.split(',')) {
	    			setProfileNames.add(s);
	    		}
	    		
	    }
	    Profile prof = [select Name from Profile where Id = :UserInfo.getProfileId() limit 1];
	    showPricing = setProfileNames.contains(prof.Name);

		// get serviceDate - date of the last time entry on the slip
		List <Time_Entry__c> lstTEByDate = 
			[select Date__c 
			 from Time_Entry__c 
			 where Slip_Number__c = :theSlip.Id
			 order by Date__c desc];
		serviceDate = '';
		if (lstTEByDate.size() > 0) {
			serviceDate = Datetime.newInstance(lstTEByDate[0].Date__c, Time.newInstance(0, 0, 0, 0)).format('MM/dd/yyyy');
		}


		// get labor rates
		List <Labor_Rate__c> lstRates = 
			[select Id, Role__c, Hourly_Rate__c,  Shift__c, Rate__c 
			 from Labor_Rate__c 
			 where Job__c = :theSlip.Job_Name__c
			 order by Role__c, Hourly_Rate__c,  Shift__c];


		// get slip adjustments
		List <Slip_Adjustment__c> lstAdjustments = 
			[select Slip__c, Shift__c, Role__c, Name, Key__c, Id, Hours__c, Hourly_Rate__c
			 from Slip_Adjustment__c  
			 where Slip__c = :theSlip.Id and Adjust_By__c = 'Role'
			 order by Role__c, Key__c];
		
		List <Slip_Adjustment__c> lstAdjustEmp = 
			[select Slip__c, Shift__c, Role__c, Name, Key__c, Id, Hours__c, Hourly_Rate__c, Job_Staffing__c, Job_Staffing__r.Employee__r.Name
			 from Slip_Adjustment__c  
			 where Slip__c = :theSlip.Id and Adjust_By__c = 'Employee'
			 order by Job_Staffing__c, Role__c, Key__c];
		
		// get time entries connected to this slip
		//  Role__c - e.g. Journeyman
		//  Hourly_Rate__c - Regular Time/Over Time/Double Time
		//  Shift__c - e.g. Regular/Second Shift/Third Shift
		List <Time_Entry__c> lstTE = 
			[select 
				Id, Shift__c, Role__c, Phase__c, PTO__c, Job__c, Job_Staffing__c, Job_Staffing__r.Employee__r.Name, 
				Job_Name__c, Hours_Worked__c, Hourly_Rate__c, Employee__c, Day_of_Week__c, Date__c 
			 from Time_Entry__c 
			 where Slip_Number__c = :theSlip.Id
			 order by Role__c, Job_Staffing__c, Hourly_Rate__c, Shift__c];
			 

		// GF / Foreman Hours: Always show these roles and all five rows even if there is no time added 
		//   (Regular Time, Overtime, Double Time, Regular Time - Second Shift, Overtime - Second Shift, Regular Time - Third Shift, Overtime - Third Shift)
		// The above needs to apply for adjust by employee as well. 
		// All employees marked as general foreman or foreman must appear even if they have no time logged in timesheet.
		//
		
		List <String> lstRoles = new List <String> { 'General Foreman', 'Foreman', 'Journeyman', 'Steward', 'Safety Officer', 'Technician General Foreman', 'Technician Foreman', 'Technician', 'Technician Sub Foreman', 'Apprentice'};
		String adjustBy = (lstAdjustEmp.size() > 0 ? 'Employee' : 'Role');
		laborData = 
			'{'
			+ '"adjustBy":"' + adjustBy + '",'
			+ '"laborMarkup":"' + theSlip.T_M_Labor_Markup__c + '",'
			+ '"certifiedPayroll":"' + theSlip.Certified_Payroll__c + '",'
			+ '"roles":[';
		
		for (String s : lstRoles) {
			List <Labor_Rate__c> lstLRs = new List <Labor_Rate__c> ();
			List <Time_Entry__c> lstTEs = new List <Time_Entry__c> ();
			List <Slip_Adjustment__c> lstAdj = new List <Slip_Adjustment__c> ();
			
			for (Labor_Rate__c lr : lstRates) {
				if (lr.Role__c.equals(s)) {
					lstLRs.add(lr);
				}
			}

			for (Time_Entry__c te : lstTE) {
				if (te.Role__c.equals(s)) {
					lstTEs.add(te);
				}
			}
			
			for (Slip_Adjustment__c sa : lstAdjustments) {
				if (sa.Role__c.equals(s)) {
					lstAdj.add(sa);
				}
			}
			system.debug('===>>> role: '+s+', adj: '+lstAdj);
			
			if (s.equals('General Foreman') || s.equals('Foreman') || lstTEs.size() > 0 || lstAdj.size() > 0) {
				LaborHelper rh = new LaborHelper(s, null, null, lstTEs, lstAdj, lstLRs);
				laborData += rh.getJsonRole();
			}
			
		}

		laborData = laborData.removeEnd(',') +
			'],';
		
		system.debug('===>>> laborData: '+ laborData);
		

		// employees
		//
		// get set of all employees connected to this slip
		// get any employees on the job with role general foreman/foreman who do not have time entries
		// get slip adjustments
		Set <Id> setGFFIncluded = new Set <Id> ();
		Map <Id, LaborHelper> mapJSIdHelper = new Map <Id, LaborHelper> ();
		if (lstAdjustEmp.size() > 0) {
			Id currentJS = lstAdjustEmp[0].Job_Staffing__c;
			String currentRole = lstAdjustEmp[0].Role__c;
			String currentName = lstAdjustEmp[0].Job_Staffing__r.Employee__r.Name;
			List <Slip_Adjustment__c> lstSA = new List <Slip_Adjustment__c> ();
			for (Slip_Adjustment__c sa : lstAdjustEmp) {
				setGFFIncluded.add(sa.Job_Staffing__c);
				if (currentJS != sa.Job_Staffing__c) {
					mapJSIdHelper.put(currentJS, new LaborHelper(currentRole, currentJS, currentName, null, lstSA, null));
					lstSA = new List <Slip_Adjustment__c> ();
					currentJS = sa.Job_Staffing__c;
					currentRole = sa.Role__c;
					currentName = sa.Job_Staffing__r.Employee__r.Name;
				}
				lstSA.add(sa);
			}
			mapJSIdHelper.put(currentJS, new LaborHelper(currentRole, currentJS, currentName, null, lstSA, null));			
		}

		// get time entries connected to this slip
		//  Role__c - e.g. Journeyman
		//  Hourly_Rate__c - Regular Time/Over Time/Double Time
		//  Shift__c - e.g. Regular/Second Shift/Third Shift
		List <Time_Entry__c> lstTEEmp = 
			[select 
				Id, Shift__c, Role__c, Phase__c, PTO__c, Job__c, Job_Staffing__c, Job_Staffing__r.Employee__r.Name, 
				Job_Name__c, Hours_Worked__c, Hourly_Rate__c, Employee__c, Day_of_Week__c, Date__c 
			 from Time_Entry__c 
			 where Slip_Number__c = :theSlip.Id
			 order by Job_Staffing__c, Role__c, Hourly_Rate__c, Shift__c];

		if (lstTEEmp.size() > 0) {
			Id currentJS = lstTEEmp[0].Job_Staffing__c;
			String currentRole = lstTEEmp[0].Role__c;
			String currentName = lstTEEmp[0].Job_Staffing__r.Employee__r.Name;
			List <Time_Entry__c> lstTEC = new List <Time_Entry__c> ();
			for (Time_Entry__c te : lstTEEmp) {
				setGFFIncluded.add(te.Job_Staffing__c);
				if (currentJS != te.Job_Staffing__c) {
					LaborHelper lh = mapJSIdHelper.get(currentJS);
					if (lh == null) {
						mapJSIdHelper.put(currentJS, new LaborHelper(currentRole, currentJS, currentName, lstTEC, null, null));
					}
					else {
						lh.addTEs(lstTEC);
					}
					lstTEC = new List <Time_Entry__c> ();
					currentJS = te.Job_Staffing__c;
					currentRole = te.Role__c;
					currentName = te.Job_Staffing__r.Employee__r.Name;
				}
				lstTEC.add(te);
			}
			if (mapJSIdHelper.get(currentJS) == null) {
				mapJSIdHelper.put(currentJS, new LaborHelper(currentRole, currentJS, currentName, lstTEC, null, null));
			}
			else {
				mapJSIdHelper.get(currentJS).addTEs(lstTEC);
			}
		}

		Set <String> setGFF = new Set <String> {'General Foreman', 'Foreman'};
		Map <Id, Job_Staffing__c> mapGFF = new Map <Id, Job_Staffing__c> (
				[select Id, Employee__r.Name, Role__c from Job_Staffing__c where Role__c in :setGFF and Job_Name__c = :theSlip.Job_Name__c and Id not in :setGFFIncluded]
			);


		employeeData = '{"employees":[';
		laborData += '"employees":[';
		
		for (String s : lstRoles) {
			
			List <Labor_Rate__c> lstLRs = new List <Labor_Rate__c> ();
			for (Labor_Rate__c lr : lstRates) {
				if (lr.Role__c.equals(s)) {
					lstLRs.add(lr);
				}
			}

			for (LaborHelper lh : mapJSIdHelper.values()) {
				if (lh.role.equals(s)) {
					lh.addRates(lstLRs);
					employeeData += lh.getJsonEmpl();				
					laborData += lh.getJsonEmpl();				
				}
			}
			if (s.equals('General Foreman') || s.equals('Foreman')) {
				for (Job_Staffing__c js : mapGFF.values()) {
					if (js.Role__c.equals(s)) {
						LaborHelper lh = new LaborHelper(js.Role__c, js.Id, js.Employee__r.Name, null, null, lstLRs);
						employeeData += lh.getJsonEmpl();
						laborData += lh.getJsonEmpl();
					}
				}
			}
		}

		employeeData = employeeData.removeEnd(',') +
			']}';
		laborData = laborData.removeEnd(',') +
			']}';
		




		system.debug('===>>> employeeData: '+ employeeData);
		system.debug('===>>> laborData: '+ laborData);



		// get product data where pricebook.name = job.division
		List <PricebookEntry> lstPBE = 
			[select 
				UnitPrice, Pricebook2Id, ProductCode, Product2Id, Product2.Product_Type__c, Product2.Product_Sub_Type__c, 
				Product2.Name, Product2.Family, Product2.Description, Product2.Product_Size__c
			 from PricebookEntry
			 where 
			 	Pricebook2.Name = :theSlip.Job_Name__r.Division__c
			 	or Pricebook2.Job__c = :theSlip.Job_Name__c
			 order by Pricebook2Id, Product2.Family, Product2.Product_Type__c, Product2.Product_Sub_Type__c, Product2.Product_Size__c];

		Set <String> setFamilies = new Set <String> ();
		Set <String> setTypes = new Set <String> ();
		Set <String> setSubtypes = new Set <String> ();
		Set <String> setNames = new Set <String> ();
		
		system.debug('lstPBE: '+lstPBE);
		system.debug('lstPBE.size(): '+lstPBE.size());	
		String pbId = '';
		if (lstPBE.size() > 0) {
			pbId = lstPBE[0].Pricebook2Id;
		}
		system.debug('pbId: '+pbId);
		
		productData = 
			'{"pricebookId":"' + pbId + '","products":[';
		for (PricebookEntry p : lstPBE) {
			//if (p.Product2.Name.contains('\'')) continue;
			productData
				+= '{'
				+   '"pbeid":"'     + p.Id + '",'
				+   '"pid":"'       + p.Product2Id + '",'
				+   '"family":"'    + nameString(p.Product2.Family) + '",'
				+   '"type":"'      + nameString(p.Product2.Product_Type__c) + '",'
				+   '"subtype":"'   + nameString(p.Product2.Product_Sub_Type__c) + '",'
				+   '"name":"'      + nameString(p.Product2.Name) + '",'
				+   '"listprice":"' + p.UnitPrice + '",'
				+   '"size":"'      + p.Product2.Product_Size__c + '"'
				+  '},';
//productData +='{"pbeid":"' + p.Id + '","pid":"' + p.Product2Id + '","family":"' + p.Product2.Family + '","type":"' + p.Product2.Product_Type__c + '","subtype":"' + p.Product2.Product_Sub_Type__c + '","size":"' + p.Product2.Product_Size__c + '"},';
			
			if (p.Product2.Family != null) setFamilies.add(p.Product2.Family);
			if (p.Product2.Product_Type__c != null) setTypes.add(p.Product2.Product_Type__c);
			if (p.Product2.Product_Sub_Type__c != null) setSubtypes.add(p.Product2.Product_Sub_Type__c);
			if (p.Product2.Name != null) setNames.add(p.Product2.Name);
		}
		productData = productData.removeEnd(',')
			+ '],';

		List <String> lstFamilies = new List <String> (setFamilies);  lstFamilies.sort();
		List <String> lstTypes = new List <String> (setTypes);  lstTypes.sort();
		List <String> lstSubtypes = new List <String> (setSubtypes);  lstSubtypes.sort();
		List <String> lstNames = new List <String> (setNames);  lstNames.sort();

		productData +=
			'"productfamilies":[';
		for (String s : lstFamilies) {
			productData += '"' + nameString(s) + '",';
		}
		productData = productData.removeEnd(',') + '],';

		productData +=
			'"producttypes":[';
		for (String s : lstTypes) {
			productData += '"' + nameString(s) + '",';
		}
		productData = productData.removeEnd(',') + '],';

		productData +=
			'"productsubtypes":[';
		for (String s : lstSubtypes) {
			productData += '"' + nameString(s) + '",';
		}
		productData = productData.removeEnd(',') + '],';

		productData +=
			'"productnames":[';
		for (String s : lstNames) {
			productData += '"' + nameString(s) + '",';
		}
		productData = productData.removeEnd(',') + ']}';

		system.debug('===>>> productData: '+productData);


		// get existing slip materials
		tax = '6.25';//TODO
		
		List <Slip_Materials__c> lstMaterials = 
			[select Units__c, Total__c, Slip__c, Product__c, Product__r.Name, Product__r.Family, Product__r.Product_Type__c, Product__r.Product_Sub_Type__c, Price__c, Markup__c, Name, Id 
			 from Slip_Materials__c 
			 where Slip__c = :theSlip.Id 
			 order by Product__r.Family, Product__r.Product_Type__c,  Product__r.Product_Sub_Type__c];
		
		Boolean materialsUseLineitemMarkup = false;
		String matlist = '';
		for (Slip_Materials__c sm : lstMaterials) {
			if (sm.Markup__c != null) {}
			matlist
				+= '{'
				+   '"sfid":"' + sm.Id + '",'
				+   '"family":"' + sm.Product__r.Family + '",'
				+   '"type":"' + nameString(sm.Product__r.Product_Type__c) + '",'
				+   '"subtype":"' + nameString(sm.Product__r.Product_Sub_Type__c) + '",'
				+   '"name":"'    + nameString(sm.Product__r.Name) + '",'
				+   '"listprice":"' + sm.Price__c + '",'
				+   '"markup":"' + sm.Markup__c + '",'
				+   '"cnt":"' + Integer.valueOf(sm.Units__c) + '",'
				+   '"pid":"' + sm.Product__c + '"'
				+  '},';
		}
		materials = 
			'{"tax":6.25,"markup":' + theSlip.T_M_Materials_Markup__c + ',"materialsUseLineitemMarkup":' + materialsUseLineitemMarkup + ',"materials":['
			+ matlist.removeEnd(',')
			+ ']}';



		// get other charges
		//
		
		// get charge types picklist values
		String chargeTypes = '["--None--",';
		for (Schema.PicklistEntry f : Other_Slip_Charges__c.Charge_Type__c.getDescribe().getPicklistValues()) {
			system.debug(f.getLabel() + ', '+f.getValue());
			chargeTypes += '"' + f.getValue() + '",';
		}
		chargeTypes = chargeTypes.removeEnd(',') + ']';
		
		List <Other_Slip_Charges__c> lstOther = [select Id, Charge_Type__c, Description__c, Cost__c, Markup__c from Other_Slip_Charges__c where Slip__c = :theSlip.Id];
		Decimal otherMark = 0.0;
		if (theSlip.T_M_Materials_Markup__c != null) {
			otherMark = theSlip.T_M_Materials_Markup__c;
		}
		
		Boolean otherUseLineitemMarkup = false;
		otherData = 
			'{"other":{'
			+ '"otherMarkup":' + otherMark + ','
			+ '"otherMarkupSubcontractor":' + theSlip.T_M_Subcontractor_Markup__c + ','
			+ '"otherUseLineitemMarkup":' + otherUseLineitemMarkup + ','
			+ '"chargeTypes":' + chargeTypes + ','
			+ '"charges":[';
		for (Other_Slip_Charges__c osc : lstOther) {
			//String des = osc.Description__c.escapeUnicode();
			otherData += 
				'{'
				+ '"sfid":"' + osc.Id + '",'
				+ '"chargetype":"' + osc.Charge_Type__c + '",'
				+ '"description":"' + osc.Description__c + '",'
				+ '"cost":"' + osc.Cost__c + '",'
				+ '"markup":"' + osc.Markup__c + '"'
				+ '},';
			//otherData += '{"description":"' + EncodingUtil.urlEncode(osc.Description__c, 'UTF-8') + '","cost":"' + osc.Cost__c + '","markup":"' + osc.Markup__c + '"},';
		}
		otherData = otherData.removeEnd(',') + ']}}';
		system.debug('===>>> otherData: '+otherData);
	}

	public static String nameString(String str) {
        if (str == null) {
            str = '';
        }
		if (str.length() > 75) {
			str = str.substring(0, 75) + '...';
		}
		return String.escapeSingleQuotes(str).replace('"', '\\\\"');
		//return str.replace('"', '\\\\"');
	}
	
	// get time entries connected to this slip
	//  Role__c - e.g. Journeyman
	//  Hourly_Rate__c - Regular Time/Over Time/Double Time
	//  Shift__c - e.g. Regular/Second Shift/Third Shift
	//
	// need to turn them into map like this:
	//
	// mapRoleMapKeyHours = new Map <String, Map<String, Decimal>> {
	//   'General Foreman (Frank Smith)'=> new Map <String, Decimal> {
	//     'R' => 5.5,
	//     'OT' => 5.0,
	//     'R - 2nd' => 3.5,
	//     'R - 3rd' => 4.5
	//   }
	// };
	//
	// then into json like this:
	//
	// '{"role":"General Foreman (Frank Smith)","ids":"0123456789","total":"6.5","R":"1.0","OT":"2.5","DT":"3.0"}';
	//
	public static Map <String, String> mapRate = new Map <String, String> {'Regular Time'=>'R', 'Over Time'=>'OT', 'Double Time'=>'DT'};
	public static Map <String, String> mapShift = new Map <String, String> {'Normal'=>'', 'Second Shift'=>'Second Shift', 'Third Shift'=>'Third Shift'};

/*	
	public String getGroupedTimeEntries (List <Time_Entry__c> lstTE) {
		system.debug('===>>> getGroupedTimeEntries, role: '+lstTE[0].Role__c+', size: '+lstTE.size());
		Map<String, Decimal> mapHours = new Map<String, Decimal> ();
		
		for (Time_Entry__c te : lstTE) {
			String key = te.Hourly_Rate__c + (te.Shift__c == 'Normal' ? '' : ' - ' + mapShift.get(te.Shift__c));
			Decimal hours = (mapHours.get(key) == null ? 0.0 : mapHours.get(key));
			hours += te.Hours_Worked__c;
			system.debug('===>>> getGroupedTimeEntries, key: '+key+', hours: '+hours);
			mapHours.put(key, hours);
		}
		
		
		return orderTime(mapHours);
		//return '{"role":"General Foreman (Frank Smith)","ids":"0123456789","total":"6.5","R":"1.0","OT":"2.5","DT":"3.0"}';
	}
	*/
	
	//private List <String> lstOrderTime = new List <String> {'R','OT','DT','R - 2nd','OT - 2nd','DT - 2nd','R - 3rd','OT - 3rd','DT - 3rd'};
	public static List <String> lstOrderTime = new List <String> { 'Regular Time','Over Time','Double Time', 'Regular Time - Second Shift','Over Time - Second Shift','Double Time - Second Shift', 'Regular Time - Third Shift','Over Time - Third Shift','Double Time - Third Shift'};
	public static List <String> lstForemanTime = new List <String> { 'Regular Time','Over Time','Double Time', 'Regular Time - Second Shift','Over Time - Second Shift', 'Regular Time - Third Shift','Over Time - Third Shift'};
	public static List <String> lstShift = new List <String> { 'Normal','Normal','Normal','Second Shift','Second Shift','Second Shift','Third Shift','Third Shift','Third Shift'};
	public static List <String> lstRate =  new List <String> { 'Regular Time','Over Time','Double Time','Regular Time','Over Time','Double Time','Regular Time','Over Time','Double Time'};
	
	/*
	public String orderTime(Map <String, Decimal> mapTime) {
		String r = '[';
		for (Integer i=0; i<9; i++) {
			if (mapTime.get(lstOrderTime[i]) != null) {
				String key = lstOrderTime[i];
				r += 
					'{'
					+ '"key":"' + key + '",'
					+ '"hours":"' + mapTime.get(key) + '",'
					+ '"adjusted":"' + mapTime.get(key) + '",'
					+ '"shift":"' + lstShift[i] + '",'
					+ '"hourlyrate":"' + lstRate[i] + '"'
					+ '},';
			}
		}
		return r.removeEnd(',') + ']';
	}
*/

	
    @RemoteAction
    public static String getRoles(String saveJson) {



    	return null;
    }


	public class LaborHelper {
		public String role;
		public Id jsId;
		public String jsName;
		public Decimal totalActual = 0.0;
		public Decimal totalAdjust = 0.0;
		public Map <String, HourHelper> mapHelpers = new Map <String, HourHelper> ();
		public String jsonRole = '';
		public String jsonEmpl = '';

		public LaborHelper (String role, Id jsId, String jsName, List <Time_Entry__c> lstTEs, List <Slip_Adjustment__c> lstAdj,  List <Labor_Rate__c> lstLRs) {
			this.role = role;
			this.jsId = jsId;
			this.jsName = jsName;
			if (this.jsName != null) this.jsName = this.jsName.replaceAll('\'', '\\\\\'');

			// GF / Foreman Hours: Always show these roles and all five rows even if there is no time added 
			//   (Regular Time, Overtime, Double Time, Regular Time - Second Shift, Overtime - Second Shift, Regular Time - Third Shift, Overtime - Third Shift)
			if (role.equals('General Foreman') || role.equals('Foreman')) {
				for (String s : lstForemanTime) {
					mapHelpers.put(s, new HourHelper(s));
				}
			}
			
			if (lstTEs != null) addTEs(lstTEs);
			if (lstAdj != null) addAdj(lstAdj);
			if (lstLRs != null) addRates(lstLRs);
		}
		
		public void addTEs(List <Time_Entry__c> lstTEs) {
			// build map of actual/adjust hours
			//
			// actual hours
			//
			for (Time_Entry__c te : lstTEs) {
				String key = te.Hourly_Rate__c + (te.Shift__c == 'Normal' ? '' : ' - ' + mapShift.get(te.Shift__c));
				HourHelper hh = mapHelpers.get(key);
				if (hh == null) {
					hh = new HourHelper(key);
					mapHelpers.put(key, hh);
				}
				hh.hoursActual += te.Hours_Worked__c;
				totalActual += te.Hours_Worked__c;
			}
		}
		
		public void addAdj(List <Slip_Adjustment__c> lstAdj) {
			// adjusted hours
			//
			for (Slip_Adjustment__c sa : lstAdj) {
				HourHelper hh = mapHelpers.get(sa.Key__c);
				if (hh == null) {
					hh = new HourHelper(sa.Key__c);
					mapHelpers.put(sa.Key__c, hh);
				}
				hh.hoursAdjust += sa.Hours__c;
				totalAdjust += sa.Hours__c;
			}
		}
		
		public void addRates(List <Labor_Rate__c> lstRates) {
			for (Labor_Rate__c lr : lstRates) {
				String key = lr.Hourly_Rate__c + (lr.Shift__c == 'Normal' || lr.Shift__c == 'Regular' ? '' : ' - ' + mapShift.get(lr.Shift__c));
				HourHelper hh = mapHelpers.get(key);
				if (hh != null) {
					hh.laborRate = lr.Rate__c;
				}
			}
		}
		
		public String getJsonRole() {
			// if adjusted hours are zero (no adjustments saved yet), set default adjusted to actual
			if (totalActual > totalAdjust) {
				for (HourHelper hh : mapHelpers.values()) {
					hh.hoursAdjust = hh.hoursActual;
				}
				totalAdjust = totalActual;
			}
			
			// build json
			// '{"role":"General Foreman (Frank Smith)","ids":"0123456789","total":"6.5","R":"1.0","OT":"2.5","DT":"3.0"}';
			// {"key":"Regular Time","hours":"16.0","adjusted":"16.0","shift":"Normal","hourlyrate":"Regular Time"}
			jsonRole += 
				  '{"role":"' + role + '",'
				+ '"rolename":"' + role + '",'
				+ '"ids":"asdf",'
				+ '"totalactual":"' + totalActual + '",'
				+ '"totaladjust":"' + totalAdjust + '",'
				+ '"hours":[';
			
			for (Integer i=0; i<9; i++) {
				String key = lstOrderTime[i];
				HourHelper hh = mapHelpers.get(key);
				if (hh != null) {
					jsonRole += 
						'{'
						+ '"key":"' + key + '",'
						+ '"hours":"' + hh.hoursActual + '",'
						+ '"adjusted":"' + hh.hoursAdjust + '",'
						+ '"shift":"' + lstShift[i] + '",'
						+ '"hourlyrate":"' + lstRate[i] + '",'
						+ '"laborrate":"' + hh.laborRate + '"'
						+ '},';
				}
			}
			jsonRole = jsonRole.removeEnd(',') + ']},';
			return jsonRole;
		}
		
		public String getJsonEmpl() {
			// if adjusted hours are zero (no adjustments saved yet), set default adjusted to actual
			if (totalActual > totalAdjust) {
				for (HourHelper hh : mapHelpers.values()) {
					hh.hoursAdjust = hh.hoursActual;
				}
				totalAdjust = totalActual;
			}

			jsonEmpl =  
				'{"name":"' + jsName +' (' + role + ')",'
				+ '"rolename":"' + role + '",'
				+ '"jobStaffing":"' + jsId + '",'
				+ '"totalactual":"' + totalActual + '",'
				+ '"totaladjust":"' + totalAdjust + '",'
				+ '"hours":[';
			
			for (Integer i=0; i<9; i++) {
				String key = lstOrderTime[i];
				HourHelper hh = mapHelpers.get(key);
				if (hh != null) {
					jsonEmpl += 
						'{'
						+ '"key":"' + key + '",'
						+ '"hours":"' + hh.hoursActual + '",'
						+ '"adjusted":"' + hh.hoursAdjust + '",'
						+ '"shift":"' + lstShift[i] + '",'
						+ '"hourlyrate":"' + lstRate[i] + '",'
						+ '"laborrate":"' + hh.laborRate + '"'
						+ '},';
				}
			}
			jsonEmpl = jsonEmpl.removeEnd(',') + ']},';
			return jsonEmpl;
		}
	}



	// {"key":"Regular Time","hours":"16.0","adjusted":"16.0","shift":"Normal","hourlyrate":"Regular Time"}
	public class HourHelper {
		public String key;
		public Decimal hoursActual = 0.0;
		public Decimal hoursAdjust = 0.0;
		public Decimal laborRate = 0.00;
		public HourHelper(String key) {
			this.key = key;
		}
	}








	/* save string is json, like this
		
		labor:
			role:
			
			employee:
		
		materials:
		
		other:
	*/

    @RemoteAction
    public static String saveAll(String saveJson) {
    	
        JSONParser parser = JSON.createParser(saveJson);
		SaveData sd = (SaveData)parser.readValueAs(SaveData.class);
		system.debug('===>>> sd.sid: '+sd.sid+', sd: '+sd);

		System.SavePoint sp = Database.setSavePoint();
		try {

			Id sid = Id.valueOf(sd.sid);
			Slip__c theSlip = [select Id, Job_Name__c, Job_Name__r.Name from Slip__c where Id = :sid limit 1];
			
			// save job changes
			if (sd.laborMarkupPercent != null && !sd.laborMarkupPercent.equals('')) {
				Job__c updateJob = new Job__c(Id=theSlip.Job_Name__c);
				updateJob.T_M_Markup_Labor__c = Decimal.valueOf(sd.laborMarkupPercent.remove(','));
				update updateJob;
			}

			// save slip changes
			//Select s.Total_Materials_Cost__c, s.Total_Materials_Billed__c, s.Total_Labor_Hours__c, s.Total_Labor_Cost__c, s.Total_Labor_Billed__c, 
			//s.T_M_Subcontractor_Markup__c, s.T_M_Materials_Markup__c, s.T_M_Labor_Markup__c, s.Materials_Mark_Up__c From Slip__c s
			Slip__c updateSlip = new Slip__c(Id=sid);
			if (sd.laborTotalHours != null && !sd.laborTotalHours.equals('')) updateSlip.Total_Labor_Hours__c = Decimal.valueOf(sd.laborTotalHours.remove(','));
			if (sd.laborSubtotal != null && !sd.laborSubtotal.equals('')) updateSlip.Total_Labor_Cost__c = Decimal.valueOf(sd.laborSubtotal.remove(','));
			if (sd.laborTotal != null && !sd.laborTotal.equals('')) updateSlip.Total_Labor_Billed__c = Decimal.valueOf(sd.laborTotal.remove(','));
			
			if (sd.materialsMarkup != null && !sd.materialsMarkup.equals('')) updateSlip.Materials_Mark_Up__c = Decimal.valueOf(sd.materialsMarkup.remove(','));
			if (sd.materialsTax != null && !sd.materialsTax.equals('')) updateSlip.Tax__c = Decimal.valueOf(sd.materialsTax.remove(','));
			//if (sd.materialsSubtotal != null && !sd.materialsSubtotal.equals('')) updateSlip.Total_Materials_Cost__c = Decimal.valueOf(sd.materialsSubtotal);
			if (sd.materialsTotal != null && !sd.materialsTotal.equals('')) updateSlip.Total_Materials_Billed__c = Decimal.valueOf(sd.materialsTotal.remove(','));

			if (sd.otherChargesTotal != null && !sd.otherChargesTotal.equals('')) updateSlip.Total_Other_Charges_Billed__c = Decimal.valueOf(sd.otherChargesTotal.remove(','));

			update updateSlip;

			

			// save any changes by labor
			List <Slip_Adjustment__c> lstAdjustDeletes = new List <Slip_Adjustment__c> ();
			for (Slip_Adjustment__c a : [select Id from Slip_Adjustment__c where Slip__c = :sid]) {
				lstAdjustDeletes.add(new Slip_Adjustment__c(Id=a.Id));
			}
			if (lstAdjustDeletes.size() > 0) {
				delete lstAdjustDeletes;
			}
			
			List <Slip_Adjustment__c> lstAdjustInserts = new List <Slip_Adjustment__c> ();
			List <Labor_Rate__c> lstLaborRateUpserts = new List <Labor_Rate__c> ();
			if (sd.hours.size() > 0) {
				String adjustBy = sd.hours[0].adjustBy;
				for (HourData hd : sd.hours) {
					Decimal lr = null;
					if (hd.laborRate != null && !hd.laborRate.equals('')) {
						lr = Decimal.valueOf(hd.laborRate);
						if (lr != 0.0) {
							lstLaborRateUpserts.add( new Labor_Rate__c(Job__c=theSlip.Job_Name__c, Role__c=hd.role, Rate__c=lr, Shift__c=hd.shift, Hourly_Rate__c=hd.hourlyRate));
						}
					}
					else {
						lr = 0.0;
					}
					if (hd.hours != null && !hd.hours.equals('')) {
						system.debug('===>>> role: '+hd.role);
						Id js = null;
						if (hd.jobStaffing != null && !hd.jobStaffing.equals('xxx')) {
							js = Id.valueOf(hd.jobStaffing);
						}
						Decimal hrs = Decimal.valueOf(hd.hours);
						Decimal sub = hrs * lr;
						lstAdjustInserts.add( new Slip_Adjustment__c(Slip__c=sid, Key__c=hd.key, Adjust_By__c=adjustBy, Role__c=hd.role, Job_Staffing__c=js, Hours__c=hrs, Shift__c=hd.shift, Hourly_Rate__c=hd.hourlyRate, Labor_Rate__c=lr, Labor_Subtotal__c=sub));
					}
				}
				if (lstAdjustInserts.size() > 0) {
					insert lstAdjustInserts;
				}
				if (lstLaborRateUpserts.size() > 0) {
					upsert lstLaborRateUpserts;
				}
			}
			
			// save product changes

			// check if we need a custom pricebook
			Boolean custPB = false;
			for (ProductData pd : sd.materials) {
				if (!pd.deleted && pd.std != null && pd.std.equals('custom')) {
					custPB = true;
					break;
				}
			}
			Id pbId;
			if (custPB) {
				List <Pricebook2> lstCustPB = [select Id from Pricebook2 where Job__c = :theSlip.Job_Name__c limit 1];
				if (lstCustPB.size() == 1) {
					pbId = lstCustPB[0].Id;
				}
				else {
					Pricebook2 cpb = new Pricebook2(Name='Custom Pricebook - '+theSlip.Job_Name__r.Name,Job__c=theSlip.Job_Name__c);
					insert cpb;
					pbId = cpb.Id;
				}
			}

			List <Slip_Materials__c> lstMatDeletes = new List <Slip_Materials__c> ();
			List <Slip_Materials__c> lstMatUpserts = new List <Slip_Materials__c> ();
			List <Product2> lstProductInserts = new List <Product2> ();
			Map <String, Slip_Materials__c> mapCustProdNameSM = new Map <String, Slip_Materials__c> ();
			for (ProductData pd : sd.materials) {
				Slip_Materials__c sm;
				if (pd.sfid != null && !pd.sfid.equals('')) {
					sm = new Slip_Materials__c(Id=Id.valueOf(pd.sfid));
					if (pd.deleted) {
						lstMatDeletes.add(sm);
					}
				}
				else {
					sm = new Slip_Materials__c(Slip__c=Id.valueOf(sid));
				}

				if (!pd.deleted) {
					if (sd.showPricing) {
						sm.Units__c = Decimal.valueOf(pd.pcount);
						sm.Price__c = Decimal.valueOf('1.5');
					}
					
					if (pd.pid != null && !pd.pid.equals('custom')) {
						sm.Product__c = Id.valueOf(pd.pid);
						lstMatUpserts.add(sm);
					}
					else if (pd.pid != null && pd.pid.equals('custom') && pd.std != null && pd.std.equals('custom')) {
						mapCustProdNameSM.put(pd.name, sm);
						lstProductInserts.add(new Product2(Name=pd.name, Job__c=theSlip.Job_Name__c, IsActive=true));
					}
				}	

/*zzzzzzzzzzzzzzzzzzzzz
				if (pd.sfid != null) {
					Slip_Materials__c sm = new Slip_Materials__c(Id=Id.valueOf(pd.sfid));
					if (sd.showPricing) {
						sm.Units__c = Decimal.valueOf(pd.pcount);
						sm.Price__c = Decimal.valueOf('1.5');
					}
					
					if (pd.deleted) {
						system.debug('===>>> deleting: '+pd.sfid);
						lstMatDeletes.add(sm);
					}
					else if (pd.pid != null && !pd.pid.equals('custom')) {
						sm.Product__c = Id.valueOf(pd.pid);
						lstMatUpserts.add(sm);
					}
					else if (pd.pid != null && pd.pid.equals('custom') && pd.std != null && pd.std.equals('custom')) {
						mapCustProdNameSM.put(pd.name, sm);
						lstProductInserts.add(new Product2(Name=pd.name, Job__c=theSlip.Job_Name__c, IsActive=true));
					}
				}
				else {
					//Slip_Materials__c sm = new Slip_Materials__c(Slip__c=sid, Product__c=Id.valueOf(pd.pid), Units__c=Decimal.valueOf(pd.pcount), Price__c=Decimal.valueOf('1.5'));
					Slip_Materials__c sm = new Slip_Materials__c(Slip__c=Id.valueOf(sid));
					if (sd.showPricing) {
						sm.Units__c = Decimal.valueOf(pd.pcount);
						sm.Price__c = Decimal.valueOf('1.5');
					}


					if (!pd.deleted && pd.pid != null && !pd.pid.equals('custom')) {
						sm.Product__c = Id.valueOf(pd.pid);
						lstMatUpserts.add(sm);
					}
					else if (!pd.deleted && pd.pid != null && pd.pid.equals('custom') && pd.std != null && pd.std.equals('custom')) {
						mapCustProdNameSM.put(pd.name, sm);
						lstProductInserts.add(new Product2(Name=pd.name, Job__c=theSlip.Job_Name__c, IsActive=true));
					}
					
				}
*/

			}
			if (lstMatDeletes.size() > 0) {
				delete lstMatDeletes;
			}
			if (lstMatUpserts.size() > 0) {
				upsert lstMatUpserts;
			}
			if (lstProductInserts.size() > 0) {
				insert lstProductInserts;
				List <Pricebook2> lstStdPB = [select Id from Pricebook2 where IsStandard = true limit 1];
				List <PricebookEntry> lstPBEStandardInserts = new List <PricebookEntry> ();
				List <PricebookEntry> lstPBECustomInserts = new List <PricebookEntry> ();
				for (Product2 p : lstProductInserts) {
					mapCustProdNameSM.get(p.Name).Product__c = p.Id;
					lstPBEStandardInserts.add(new PricebookEntry(Pricebook2Id=lstStdPB[0].Id, Product2Id=p.Id, UnitPrice=1.0));//, UseStandardPrice=false, IsActive=true));
					lstPBECustomInserts.add(new PricebookEntry(Pricebook2Id=pbId, Product2Id=p.Id, UnitPrice=1.0));//, UseStandardPrice=false, IsActive=true));
				}
				insert lstPBEStandardInserts;
				insert lstPBECustomInserts;
				upsert mapCustProdNameSM.values();
			}

	
	
			// save other charges
			//
			// delete any existing 
			List <Other_Slip_Charges__c> lstDeleteOther = new List <Other_Slip_Charges__c> ();
			//for (Other_Slip_Charges__c osc : [select Id from Other_Slip_Charges__c where Slip__c = :sid]) {
			//	lstDeleteOther.add(new Other_Slip_Charges__c(Id=osc.Id));
			//}
			
			List <Other_Slip_Charges__c> lstOther = new List <Other_Slip_Charges__c> ();
			List <String> lstRowIDs = new List <String> ();
			for (OtherChargesData rd : sd.other) {
				system.debug('===>>> sd.sid: '+sd.sid+', rd: '+rd);
				Other_Slip_Charges__c osc;
				Decimal mark = (rd.markup != null && !rd.markup.equals('') ? Decimal.valueOf(rd.markup) : null);
				if (rd.sfid != null && !rd.sfid.equals('')) {
					osc = new Other_Slip_Charges__c(Id=Id.valueOf(rd.sfid));
					if (rd.deleted) {
						lstDeleteOther.add(osc);
					}
				}
				else {
					osc = new Other_Slip_Charges__c(Slip__c=Id.valueOf(sid), Charge_Type__c=rd.chargeType, Description__c=rd.description, Markup__c=mark);
				}

				if (!rd.deleted) {
					if (sd.showPricing) {
						 osc.Cost__c = Decimal.valueOf(rd.cost);
						 osc.Markup__c = mark;
					}
					lstOther.add(osc);
					lstRowIDs.add(rd.rowid);
				}				
				
//				Other_Slip_Charges__c osc = new Other_Slip_Charges__c(Slip__c=sid, Charge_Type__c=rd.chargeType, Description__c=rd.description, Cost__c=Decimal.valueOf(rd.cost), Markup__c=Decimal.valueOf(rd.markup))
//				lstOther.add();
				
			}
			
			if (lstDeleteOther.size() > 0) {
				delete lstDeleteOther;
			}
			String otherjson = '{"othercharges":[';
			if (lstOther.size() > 0) {
				upsert lstOther;
				Integer counter = 0;
				for (Database.Upsertresult ur : Database.upsert(lstOther)) {
					otherjson += '{"rowid":"' + lstRowIDs[counter++] + '","sfid":"' + ur.getId() + '"},';
				}
			}
			otherjson = otherjson.removeEnd(',') +
			']' + ',"messages":"saved, hours: '+sd.hours.size()+', rates: '+lstLaborRateUpserts.size()+', materials: '+sd.materials.size()+', other, saved: '+lstOther.size()+', deleted: '+lstDeleteOther.size() + '"}';
			
			return otherjson;//'saved, hours: '+sd.hours.size()+', materials: '+sd.materials.size()+', other, saved: '+lstOther.size()+', deleted: '+lstDeleteOther.size();
		}
		catch(Exception e) {
			Database.rollback(sp);
			return 'Error: '+e;
		}
    }



	// parsed from json	
	public class SaveData {
		public String sid;
		public Boolean showPricing;

		public String laborTotalHours;
		public String laborMarkupPercent;
		public String laborSubtotal;
		public String laborTotal;
		public List <HourData> hours;

		public Boolean materialsUseLineitemMarkup;
		public String materialsMarkup;
		public String materialsTax;
		public String materialsSubtotal;
		public String materialsTotal;
		public List <ProductData> materials;

		public Boolean otherUseLineitemMarkup;
		public String subcontractorMarkup;
		public String otherChargesTotal;
		public List <OtherChargesData> other;
	}

	public class HourData {
		public String adjustBy;
		public String role;
		public String jobStaffing;
		public String hourlyRate;
		public String shift;
		public String hours;
		public String key;
		public String laborRate;
	}

	public class ProductData {
		public Boolean deleted;
		public String sfid;
		public String pid;
		public String family;
		public String ptype;
		public String subtype;
		public String name;
		public String size;
		public String pcount;
		public String listprice;
		public String std;
		public String markup;
	}
	
	public class OtherChargesData {
		public Boolean deleted;
		public String sfid;
		public String rowid;
		public String chargeType;
		public String description;
		public String cost;
		public String markup;
	}
	
	

}