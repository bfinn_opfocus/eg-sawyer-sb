/*
** Class:  OpportunityUtils
** Created by OpFocus on 4/29/2014
** Description: This class implements a webservice to create a Job__c record from a button on Opportunity.
*/
global with sharing class OpportunityUtils {

    static webservice CreateJobResult createJobWebservice(String oppID) {
        return createJobFromOpportunity(oppID);
    }
    
    public static CreateJobResult createJobFromOpportunity(String oppID) {
        
        CreateJobResult cjr = new CreateJobResult();
        
        try {
            Opportunity opp = [select Id, AccountId, Name, StageName from Opportunity where Id = :Id.valueOf(oppID) limit 1];
        
            List <Job__c> lstJob = [select Id, Name, Account_Name__c, Opportunity__c from Job__c where Opportunity__c = :opp.Id];
            if (opp.StageName == null || !opp.StageName.equals('Awarded')) {
                cjr.success = false;
                cjr.msg = 'This Opportunity must be set to Awarded before you can create a Job.';
            }
            else if (lstJob.size() > 1) {
                cjr.success = false;
                cjr.msg = 'Error: there are more than 1 Jobs for this Opportunity';
            }
            else if (lstJob.size() == 1) {
                cjr.jobID = lstJob[0].Id;
                if (opp.Name != lstJob[0].Name || lstJob[0].Account_Name__c == null) {
                    update new Job__c(Id=lstJob[0].Id, Name=opp.Name, Account_Name__c=opp.AccountId);
                }
            }
            else {
                // create new Job record
                Job__c newJob = new Job__c(Name=opp.Name, Account_Name__c=opp.AccountId, Opportunity__c=opp.Id);
                insert newJob;
                cjr.msg = 'Created new job';
                cjr.jobID = newJob.Id;
            }
            
        }
        catch (Exception e) {
            cjr.success = false;
            cjr.msg = 'Error while creating job: ' + e;
        }

        return cjr;
    }

    global class CreateJobResult {
        webService Boolean success  {get; set;}
        webService String msg       {get; set;}
        webService String jobID     {get; set;}
        
        CreateJobResult() {
            this.success = true;
            this.msg = '';
        }
    }


    static testmethod void testCreateJob() {
        
        Account acc1 = new Account(
                Name = 'Johnson',
                BillingStreet = '123 Street St',
                BillingState = 'MA',
                BillingCity = 'Burlington',
                BillingPostalCode = '90210'
            );
        Account specialAcct = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');
        insert new List<Account> {acc1, specialAcct};

        Opportunity opp1 = new Opportunity(
            Name = 'Test Opp 1',
            StageName = 'Pending',
            CloseDate = Date.today(),
            AccountId = acc1.Id
        );
        Opportunity opp2 = new Opportunity(
            Name = 'Test Opp 2',
            StageName = 'Awarded',
            CloseDate = Date.today(),
            AccountId = acc1.Id
        );
        Opportunity opp3 = new Opportunity(
            Name = 'Test Opp 3',
            StageName = 'Awarded',
            CloseDate = Date.today(),
            AccountId = acc1.Id
        );
        Opportunity opp4 = new Opportunity(
            Name = 'Test Opp 4',
            StageName = 'Awarded',
            CloseDate = Date.today(),
            AccountId = acc1.Id
        );
        insert new List <Opportunity> {opp1, opp2, opp3, opp4};
        
        Job__c job2a = new Job__c(
            Name = 'asdf',
            Opportunity__c = opp2.Id
        );
        Job__c job2b = new Job__c(
            Name = 'asdf',
            Opportunity__c = opp2.Id
        );
        Job__c job3 = new Job__c(
            Name = 'asdf',
            Opportunity__c = opp3.Id
        );
        insert new List <Job__c> {job2a, job2b, job3 };
        
        Test.startTest();

        OpportunityUtils.createJobFromOpportunity(opp1.Id);
        OpportunityUtils.createJobFromOpportunity(opp2.Id);
        OpportunityUtils.createJobFromOpportunity(opp3.Id);
        OpportunityUtils.createJobFromOpportunity(opp4.Id);
        
        Test.stopTest();
    }       
    

}