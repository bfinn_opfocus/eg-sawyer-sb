/*
** Class:  Test_JobQuoteTriggerHelper
** Created by OpFocus on 08/31/2015
** Description: Class contains unit tests for JobQuote Trigger and its helper class that
**		sums up Phase values on all Job Quotes that are AWARDED to the corresponding
**		Phase Change Order fields on the associated Job.
*/
@isTest
private class Test_JobQuoteTriggerHelper {

	private static final Boolean testAWARDEDJobQuote = true;
	private static final Boolean testAddingValuesToJob = true;


	@IsTest
	static void testAWARDEDJobQuote() {
		System.assert(testAWARDEDJobQuote, 'Test disabled.');
		initData();

		Test.startTest();
		for (Job_Quote__c jobQuote :  lstJobQuotes) {
			jobQuote.Quote_Status__c = JobQuoteTriggerHelper.AWARDED;
		}
		update lstJobQuotes; // should fire trigger
		Test.stopTest();

		// verify that Job Change Order fields were updated
		Job__c jobWithData = [select Phase_1_Change_Order_Equipment__c, Phase_1_Change_Order_Materials__c 
			from Job__c where Id=:job.Id limit 1];
		System.assertEquals(1500, jobWithData.Phase_1_Change_Order_Materials__c, 
			'Phase_1_Change_Order_Materials__c not set properly');
		System.assertEquals(1500, jobWithData.Phase_1_Change_Order_Equipment__c, 
			'Phase_1_Change_Order_Equipment__c not set properly');
	}

	@IsTest
	static void testAddingValuesToJob() {
		System.assert(testAddingValuesToJob, 'Test disabled.');
		initData();

		for (Job_Quote__c jobQuote :  lstJobQuotes) {
			jobQuote.Quote_Status__c = JobQuoteTriggerHelper.AWARDED;
		}
		update lstJobQuotes; // should fire trigger

		// now update Job Quotes to not be AWARDED so we can re-fire trigger
		for (Job_Quote__c jobQuote :  lstJobQuotes) {
			jobQuote.Quote_Status__c = 'Pending';
		}
		update lstJobQuotes; // should  not fire trigger

		Test.startTest();
			for (Job_Quote__c jobQuote :  lstJobQuotes) {
				jobQuote.Quote_Status__c = JobQuoteTriggerHelper.AWARDED;
			}
			update lstJobQuotes; // should  not fire trigger
		Test.stopTest();

		// verify that Job Change Order fields were updated
		Job__c jobWithData = [select Phase_1_Change_Order_Equipment__c, Phase_1_Change_Order_Materials__c 
			from Job__c where Id=:job.Id limit 1];
		System.assertEquals(3000, jobWithData.Phase_1_Change_Order_Materials__c, 
			'Phase_1_Change_Order_Materials__c not set properly');
		System.assertEquals(3000, jobWithData.Phase_1_Change_Order_Equipment__c, 
			'Phase_1_Change_Order_Equipment__c not set properly');
	}

	private static void initData() {
		Account acct = UnitTestUtils.createAccount('Test Account');
		Account specialAcct = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');
		insert new List<Account> {acct,specialAcct};

		Opportunity opp = UnitTestUtils.createOpp('Test Opp', acct.Id);
		insert opp;
		job = UnitTestUtils.createJob('J01', acct.Id, opp.Id, 'JN-100');
		insert job;

		Integer numMappings = 0;
		for (Integer i = 1; i <= 5; i++) {
			String srcFieldName = 'Phase_' + i + '_Equipment__c';
			String targetFieldName = 'Phase_' + i + '_Change_Order_Equipment__c';

			lstMappings.add(UnitTestUtils.createPhaseMapping(numMappings++, srcFieldName, targetFieldName));
			
			srcFieldName = 'Phase_' + i + '_Materials__c';
			targetFieldName = 'Phase_' + i + '_Change_Order_Materials__c';
			lstMappings.add(UnitTestUtils.createPhaseMapping(numMappings++,srcFieldName, targetFieldName));

			srcFieldName = 'Phase_' + i + '_Labor_Hours__c';
			targetFieldName = 'Phase_' + i + '_Change_Order_Labor_Hours__c';
			lstMappings.add(UnitTestUtils.createPhaseMapping(numMappings++,srcFieldName, targetFieldName));

			lstJobQuotes.add(UnitTestUtils.createJobQuote('JQ01', job.Id, i*100));
		}
		insert lstMappings;
		insert lstJobQuotes;
	}

	private static Job__c job = null;
	private static List<Job_Quote__c> lstJobQuotes = new List<Job_Quote__c>();
	private static List<Quote_Job_Phase_Field_Mappings__c> lstMappings = new List<Quote_Job_Phase_Field_Mappings__c>();
}