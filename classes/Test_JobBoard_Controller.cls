/*
** Class:  Test_JobBoard_Controller
** Created by OpFocus on 06/5/2014
** Description: Class contains unit tests for JobBoard_Controller
*/
@isTest
private class Test_JobBoard_Controller{

	static testMethod void testController() {

		JobRoleColor__c jobColor1= new JobRoleColor__c(Name='Lead Foreman', HEX__c='#00FFFF', ColorName__c='Cyan', Division_s__c='All, E.G.Sawyer');
		JobRoleColor__c jobColor2= new JobRoleColor__c(Name='Technician', HEX__c='#DCDCDC', ColorName__c='Gainsboro', Division_s__c='All, LCN');
		JobRoleColor__c jobColor3= new JobRoleColor__c(Name='Project Manager', HEX__c='#FFE4C4', ColorName__c='Bisque', Division_s__c='All, LCN, LCN Networks');
		JobRoleColor__c jobColor4= new JobRoleColor__c(Name='Foreman', HEX__c='#F0F8FF', ColorName__c='AliceBlue', Division_s__c='All, E.G.Sawyer');
		insert new List<JobRoleColor__c>{jobColor1, jobColor2, jobColor3, jobColor4};

		Account a1 = UnitTestUtils.CreateAccount('Test Co1');
		Account a2 = UnitTestUtils.CreateAccount('Test Co2');
		Account a3 = UnitTestUtils.CreateAccount('Test Co3');
        Account specialAcct = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');

		insert new List<Account>{a1, a2, a3, specialAcct};

		Contact emp1 = UnitTestUtils.createContact('testlastname1', a3.Id, '7098908909', empRtId);
		Contact emp2 = UnitTestUtils.createContact('testlastname2', a3.Id, '7778905555', empRtId);
		Contact emp3 = UnitTestUtils.createContact('testlastname3', a3.Id, '5558888909', empRtId);
		Contact emp4 = UnitTestUtils.createContact('testlastname4', a3.Id, '4442221234', empRtId);
		emp4.Project_Manager__c = true;
		emp4.Primary_Role__c = 'Office - Project Manager';
		emp4.Secondary_Role__c = 'Office - Project Manager';
		emp4.Tertiary_Role__c = 'Office - Project Manager';
		emp4.Employee_Status__c = 'Active';
		Contact emp5 = UnitTestUtils.createContact('testlastname5', a3.Id, '4442220000', empRtId);
		emp5.Primary_Role__c = 'General Foreman';
		emp5.Secondary_Role__c = 'General Foreman';
		emp5.Tertiary_Role__c = 'General Foreman';
		emp5.Employee_Status__c = 'Active';
		emp5.General_Foreman__c = true;
		insert new List<Contact>{emp1, emp2, emp3, emp4, emp5};

		Opportunity opp1 = UnitTestUtils.createOpp('opp1', a1.Id);
		Opportunity opp2 = UnitTestUtils.createOpp('opp2', a1.Id);
		Opportunity opp3 = UnitTestUtils.createOpp('opp3', a2.Id);
		Opportunity opp4 = UnitTestUtils.createOpp('opp4', a2.Id);
		insert new List<Opportunity>{opp1, opp2, opp3, opp4};

		Job__c job1 = UnitTestUtils.createJob('Job1', a1.Id, opp1.Id, 'a1 job1');
		job1.Job_Short_Name__c = 'job1';
		job1.Project_Manager__c = emp4.Id;
		job1.General_Contractor__c = a3.Id;
		job1.General_Foreman__c    = emp5.Id;
		job1.Eligible_to_Merge_on_Job_Board__c = true;
		job1.Job_Status__c = 'In Progress - Active';

		Job__c job2 = UnitTestUtils.createJob('Job2', a1.Id, opp2.Id, 'a1 job2');
		job2.General_Foreman__c    = emp5.Id;
		job2.Eligible_to_Merge_on_Job_Board__c = true;
		job2.Job_Status__c = 'In Progress - Active';

		Job__c job3 = UnitTestUtils.createJob('Job3', a2.Id, opp3.Id, 'a2 job1');
		job3.Job_Status__c = 'In Progress - Active';
		Job__c job4 = UnitTestUtils.createJob('Unassigned', a2.Id, opp4.Id, 'a2 job2');
		job4.Job_Status__c = 'In Progress - Active';
		insert new List<Job__c>{job1, job2, job3, job4};

		Job_Staffing__c js1 = UnitTestUtils.createJS(job1.Id, 'Lead Foreman',    emp1.Id, true);
		Job_Staffing__c js2 = UnitTestUtils.createJS(job1.Id, 'Foreman',         emp2.Id, true);
		Job_Staffing__c js3 = UnitTestUtils.createJS(job2.Id, 'Project Manager', emp3.Id, true);
		Job_Staffing__c js4 = UnitTestUtils.createJS(job3.Id, 'Lead Foreman',    emp4.Id, true);
		Job_Staffing__c js5 = UnitTestUtils.createJS(job3.Id, 'Technician',      emp5.Id, true);
		insert new List<Job_Staffing__c>{js1, js2, js3, js4, js5};

        PageReference pgRef = Page.JobBoard;
        Test.setCurrentPage(pgRef);
        
        JobBoard_Controller ctl = new JobBoard_Controller();
        ctl.divParam = 'All';
        ctl.division = 'All';
        ctl.doRun();
       	ctl.getJobWrappers();
        ctl.getListJobs();
        ctl.getMapJobsNew();
        ctl.getInternalJobs();
        ctl.droppedClient = job1.Id;
        ctl.droppedStaff  = js4.Id;
        ctl.droppedStaffOnTarget();
     	System.assertEquals(2, ctl.lstJobColors1.size());
     	System.assertEquals(2, ctl.lstJobColors2.size());
     	System.assert(ctl.lstDivisions.size()>0);
     	System.assert(ctl.getJobWrappers().size()>0);

     	List<JobStaffingWrapper> lstStaffWrappers = new List<JobStaffingWrapper>();
     	lstStaffWrappers.add(new JobStaffingWrapper(js1));
     	lstStaffWrappers.add(new JobStaffingWrapper(js2));
     	lstStaffWrappers.add(new JobStaffingWrapper(js3));
     	lstStaffWrappers.add(new JobStaffingWrapper(js4));

     	lstStaffWrappers.sort();
	}


	private static Id empRtId;
	
	static {
 		empRtId = [Select Id, sObjectType, Name From RecordType where SObjectType = 'Contact' and Name='Employee Contact' limit 1].Id;	
	} 

}