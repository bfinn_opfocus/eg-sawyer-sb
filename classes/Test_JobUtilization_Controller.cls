/*
** Class:  Test_JobUtilization_Controller
** Created by OpFocus on 10/22/2014
** Description: Class contains unit tests for JobUtilization_Controller
*/
@isTest
private class Test_JobUtilization_Controller{

	static testMethod void testJobUtilization() {
		Account a1 = UnitTestUtils.CreateAccount('Test Co1');
		Account a2 = UnitTestUtils.CreateAccount('Test Co2');
        Account specialAcct = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');
		insert new List<Account>{a1,a2,specialAcct};

		Contact emp1 = UnitTestUtils.createContact('testlastname1', a1.Id, '7098908909', empRtId);
		Contact emp2 = UnitTestUtils.createContact('testlastname2', a1.Id, '7778905555', empRtId);
		Contact emp3 = UnitTestUtils.createContact('testlastname3', a1.Id, '5558888909', empRtId);
		Contact emp4 = UnitTestUtils.createContact('testlastname4', a1.Id, '4442221234', empRtId);
		emp4.Primary_Role__c = 'Foreman';
		Contact emp5 = UnitTestUtils.createContact('testlastname5', a1.Id, '4442220000', empRtId);
		emp5.Primary_Role__c = 'General Foreman';
		emp5.Secondary_Role__c = 'General Foreman';
		emp5.Tertiary_Role__c = 'General Foreman';
		emp5.Employee_Status__c = 'Active';
		emp5.General_Foreman__c = true;
		insert new List<Contact>{emp1, emp2, emp3, emp4, emp5};

		Opportunity opp1 = UnitTestUtils.createOpp('opp1', a1.Id);
		insert opp1;

		Job__c job1 = UnitTestUtils.createJob('Job1', a1.Id, opp1.Id, 'a1 job1');
		job1.Job_Short_Name__c = 'job1';
		job1.General_Contractor__c = a2.Id;
		job1.General_Foreman__c    = emp5.Id;
		job1.Eligible_to_Merge_on_Job_Board__c = true;
		job1.Job_Status__c = 'In Progress - Active';
		job1.Budgeted_Equipment_for_Phase_1__c = 10;
		job1.Phase_1_Original_Labor_Hours__c   = 10;
		job1.Budgeted_Materials_Phase_1__c     = 10;
		job1.Phase_2_Original_Equipment_Budget__c = 10;
		job1.Phase_2_Original_Labor_Hours_Budget__c = 10;
		job1.Phase_2_Original_Materials_Budget__c   = 10;
		job1.Phase_3_Original_Equipment_Budget__c   = 10;
		job1.Phase_3_Original_Labor_Hours_Budget__c = 10;
		job1.Phase_3_Original_Materials_Budget__c   = 10;
		job1.Phase_4_Original_Equipment_Budget__c   = 10;
		job1.Phase_4_Original_Labor_Hours_Budget__c = 10;
		job1.Phase_4_Original_Materials_Budget__c   = 10;
		insert job1;

		Weekly_Job_Cost__c wjc = new Weekly_Job_Cost__c();
		wjc.Job_Name__c          = job1.Id;
		wjc.Phase_1_Equipment__c = 1;
		wjc.Phase_1_Labor__c     = 1;
		wjc.Phase_1_Materials__c = 1;
		wjc.Phase_2_Equipment__c = 1;
		wjc.Phase_2_Labor__c     = 1;
		wjc.Phase_2_Materials__c = 1;
		wjc.Phase_3_Equipment__c = 1;
		wjc.Phase_3_Labor__c     = 1;
		wjc.Phase_3_Materials__c = 1;
		wjc.Phase_4_Equipment__c = 1;
		wjc.Phase_4_Labor__c     = 1;
		wjc.Phase_4_Materials__c = 1;
		insert wjc;

        PageReference pageRef = Page.JobUtilization_Dashboard;
        Test.setCurrentPage(pageRef);
      		
		ApexPages.StandardController std = new ApexPages.StandardController(job1);
		JobUtilization_Controller ctl = new JobUtilization_Controller(std);
	}

	private static Id empRtId;
	
	static {
 		empRtId = [Select Id, sObjectType, Name From RecordType where SObjectType = 'Contact' and Name='Employee Contact' limit 1].Id;	
	} 

}