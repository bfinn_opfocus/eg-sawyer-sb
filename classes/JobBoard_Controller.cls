/*
** Class:  JobBoard_Controller
** Created by OpFocus on 5/15/2014
** Description: A controller for JobBoard VF page
*/
global class JobBoard_Controller{

    public String  division      {get; set;}
    public String  divParam      {get; set;}
	public String showHeader 	 {get; set;}
	public String draggable 	 {get; set;}
	public String titleText      {get; set;}
	public String activeEmpCnt   {get; set;}


	// the list of Jobs that will be displayed in the page
	private List<Job__c> lstJobs;

    private Map<String, List<Job__c>> mapJobsbyLocation;
    // We use this map to combine all the job staffings in one column if the Account (location) and General foreman are the same
    private Map<String, List<Job__c>> mapJobsbyLocationandForeman;
	
	// the list of job wrappers that contain a transposed view of the Job__c
	private List<JobWrapper> rows;
	
	// the headings for the job wrappers table
	private JobWrapper headings;
	
    public JobBoard_Controller() {
        divParam = ApexPages.currentPage().getParameters().get('division');
        if (divParam != null) {
            division = divParam;
        } else division = 'All';
        draggable = 'true';
        User theUser = [select Profile.Name from User where Id = :UserInfo.getUserId() limit 1];
        if (theUser.Profile.Name.endsWithIgnoreCase('Foreman')) {
        	draggable = 'false';
        }
        String showParam = ApexPages.currentPage().getParameters().get('showheader');
        if (showParam != null) {
	        showHeader = showParam;        	
        }
        else {
	        showHeader = 'true';        	
        }
        titleText = 'Job Board';
        activeEmpCnt = '';
    }
    
    public PageReference showHeader() {
    	showHeader = 'true';
    	return null;
    }
    
    public PageReference hideHeader() {
    	showHeader = 'false';
    	return null;
    }
    

	// Determine if this is SF1 app
	public static Boolean isSF1() {
		if(!checkNullOrBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
			!checkNullOrBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
			ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
			(ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
		)
		{
			return true;
		}
		else {
			return false;
		}
	}
	
	public static Boolean checkNullOrBlank(String str) {
		return (str == null || str.equals(''));
	}
	
	// retrieves the list of Jobs backing the page
    public List<Job__c> getListJobs() {
    	if (lstJobs==null) {

            lstJobs = new List<Job__c>();

            String jobsSoql = ' select ';
            jobsSoql +=       '    Id, Name, Account_Name__c, Account_Name__r.Name, General_Contractor__c, General_Contractor__r.Name, OwnerId, Owner.Name, ';
            jobsSoql +=       '    Project_Manager__c, Project_Manager__r.Name, Job_Number__c, Job_Short_Name__c, Job_Status__c, Estimated_Start_Date__c, ';
            jobsSoql +=       '    Eligible_to_Merge_on_Job_Board__c, General_Foreman__c, General_Foreman__r.Name, General_Foreman__r.Personal_Phone__c, General_Foreman__r.Company_Phone__c, ';
            jobsSoql +=       '    (select Id, Name, Employee__c, Job_Name__c, Role__c, Employee__r.Name, Employee__r.Phone, RoleColor__c, RoleOrder__c ';
            jobsSoql +=       '     from Job_Staffing__r ';
            jobsSoql +=       '     where IsActive__c = true and Employee__r.Employee_Status__c = \'Active\' and Employee__r.Primary_Role__c != \'Office\'';
            jobsSoql +=       '     order by RoleOrder__c ASC)';
            jobsSoql +=       ' from Job__c ';

            if (divParam == 'All') {
                jobsSoql += ' where  Job_Status__c = \'In Progress - Active\' and IsUnassigned_Job__c = false ';
                jobsSoql += ' order by Project_Manager__r.Name ASC NULLS LAST, Job_Number__c ASC ';
            }
            else {
                jobsSoql += ' where  Job_Status__c = \'In Progress - Active\' and Division__c = :divParam and IsUnassigned_Job__c = false ';
                jobsSoql += ' order by Project_Manager__r.Name ASC NULLS LAST, Job_Number__c ASC ';
            }
            lstJobs = Database.query(jobsSoql);
    	}
    	
    	return lstJobs; 
    }

    // retrieves the record for the internal job account, which we will use as a holder for unassigned staffs
    public List<Job__c> getInternalJobs() {
        List<Job__c> lstInternalJobs = new List<Job__c>();

        String jobsSoql = 'select ';
        jobsSoql +=       '    Id, Name, Account_Name__c, Account_Name__r.Name, General_Contractor__c, General_Contractor__r.Name, OwnerId, Owner.Name, ';
        jobsSoql +=       '    Project_Manager__c, Project_Manager__r.Name, Job_Number__c, Job_Short_Name__c, Job_Status__c, Estimated_Start_Date__c, ';
        jobsSoql +=       '    Eligible_to_Merge_on_Job_Board__c, General_Foreman__c, General_Foreman__r.Name, General_Foreman__r.Personal_Phone__c, General_Foreman__r.Company_Phone__c, ';
        jobsSoql +=       '    (select Id, Name, Employee__c, Job_Name__c, Role__c, Employee__r.Name, Employee__r.Phone, RoleColor__c, RoleOrder__c ';
        jobsSoql +=       '     from Job_Staffing__r ';
        jobsSoql +=       '     where IsActive__c = true and Employee__r.Employee_Status__c = \'Active\' and Employee__r.Primary_Role__c != \'Office\' ';
        jobsSoql +=       '     order by RoleOrder__c ASC) from Job__c ';

        if (divParam == 'All') {
            jobsSoql += ' where  Job_Status__c = \'In Progress - Active\' and IsUnassigned_Job__c = true ';
            jobsSoql += ' order by Name ASC, Estimated_Start_Date__c ASC ';
                     
        }
        else {
            jobsSoql += ' where  Job_Status__c = \'In Progress - Active\' and Division__c = :divParam and IsUnassigned_Job__c = true ';
            jobsSoql += ' order by Name ASC, Estimated_Start_Date__c ASC ';
           
        }
 
        lstInternalJobs = Database.query(jobsSoql);
        if (lstInternalJobs.isEmpty()) return null;
        else return lstInternalJobs;
    }

    
    // Get the Map of Jobs indexed by Account (Location) and General Foreman
    // This how we combine jobs in one column
    public Map<String, List<Job__c>> getMapJobsNew() {
        if (mapJobsbyLocationandForeman == null) {
            mapJobsbyLocationandForeman = new Map<String, List<Job__c>>();
            List<Job__c> lstJobsForMap = getListJobs();
            for (Integer i = 0; i<lstJobsForMap.size(); i++) {
                Job__c jb = lstJobsForMap[i];
                if (jb.Eligible_to_Merge_on_Job_Board__c && jb.General_Foreman__c != null && jb.Account_Name__c != null) {
                    String mergeKey = jb.Account_Name__r.Name+'//'+jb.General_Foreman__c;
                    if (!mapJobsbyLocationandForeman.containsKey(mergeKey))
                        mapJobsbyLocationandForeman.put(mergeKey, new List<Job__c>{jb});
                    else {
                        mapJobsbyLocationandForeman.get(mergeKey).add(jb);
                    }
                }
                else {
                    String mergeKey = jb.Account_Name__r.Name+'//'+i;
                        mapJobsbyLocationandForeman.put(mergeKey, new List<Job__c>{jb});
                }
            }
        }

        return mapJobsbyLocationandForeman;
    }


    // Select option items for the Division
    public List<SelectOption> lstDivisions {
        get {
            if (lstDivisions == null) {
                // Get a list of Division (Type) picklist values from Opportunity object
                lstDivisions = new List<SelectOption>();
                                
                List<Schema.PicklistEntry> lstTypeEntries = Opportunity.Type .getDescribe().getPicklistValues();
                
                lstDivisions.add(new SelectOption('All', 'All')); // value, label

                for (Schema.PicklistEntry se : lstTypeEntries) {
                    if (se.getValue() != 'TEGG')
                    lstDivisions.add(new SelectOption(se.getValue(), se.getLabel())); // value, label
                }
            }
            return lstDivisions;
        }
        set;                
    }


    public String droppedClient {public get; public set;}
    public String droppedStaff {public get; public set;}
    public PageReference droppedStaffOnTarget() {
        // Add Job Staffing to the new job
		if (droppedClient != null && droppedStaff != null) {
			String staffID = droppedStaff.substring(droppedStaff.lastIndexOf('/')+1);
			Job_Staffing__c thisStaff = [select Id, Name, Employee__c, Job_Name__c, Role__c, Employee__r.Name, Employee__r.Phone, RoleColor__c, RoleOrder__c from Job_Staffing__c where Id = :staffID limit 1];
			
			update new Job_Staffing__c(Id=Id.valueOf(staffID), IsActive__c=false);
			
            Job_Staffing__c thatStaff;
            // Check to see if there is already a staffing in the Job that the employee is dropped to
			for (Job_Staffing__c js : [select Id, Employee__c, RoleOrder__c from Job_Staffing__c where Job_Name__c = :droppedClient and IsActive__c = false]) {
                if (js.Employee__c == thisStaff.Employee__c) {
                    js.IsActive__c = true;
                    thatStaff = js;
                }
            }
            System.debug('===== droppedClient:'+droppedClient);
            // If the employee was in the job, then re-activate the employee staffing record
            if (thatStaff != null) update thatStaff;
            // If not create a new staffing record for the employee
            else insert new Job_Staffing__c(Employee__c=thisStaff.Employee__c, Job_Name__c=droppedClient, Role__c=thisStaff.Role__c, IsActive__c=true);
		}
		
		PageReference pgRef = new PageReference('/apex/JobBoard');
        pgRef.getParameters().put('division', division);
        pgRef.setRedirect(true);
    	return pgRef;
    }

        
    // retrieves the list of row wrappers
    public List<JobWrapper> getJobWrappers() {
    	if (null==rows) {

    		rows=new List<JobWrapper>();
    		    		
    		// create a row for each field - there are 7 of these: General Contractor, Job Number, Project Manager, Job Short Name, , General Foreman and Lead Foreman's phone number 
    		// and a row for all the staffings 
     		for (Integer i=0; i < 8; i++) {
   				rows.add(new JobWrapper(i));
    		}
            Map<String, List<Job__c>> mapJobs = getMapJobsNew();

            // Create a list that lets us order the Jobs by Project Manager & Job #
            // Key = pm+jobnum,  Value=mapJobs key
            Map<String, String> orderedJobs = new Map<String, String>();
            for (String key : mapJobs.keyset()) {
                Job__c job = mapJobs.get(key)[0];
            //    String pm = (job.Project_Manager__c==null) ? '' : job.Project_Manager__c;
                String pm = job.Owner.Name;
                String jobnum = job.Job_Number__c;
                orderedJobs.put(pm + jobnum, key);
            }
            List<String> orderedJobsKeys = new List<String>();
            orderedJobsKeys.addAll(orderedJobs.keyset());
            orderedJobsKeys.sort();

            for (String key : orderedJobsKeys) {
                String loc = orderedJobs.get(key);
//            for (String loc : mapJobs.keyset()) {
                String jGCs         = '';
                String jNumbers     = '';
                String jShorts      = '';
                String jManager     = '';
                String jForeMan     = '';
                String foremanPhone = '';
                String jID          = '';
                String jobLocation  = '';

                String jManagerText = '';
                String jForeManText = '';
                String jNumbersText = '';

                Map<Id, Job_Staffing__c> mapJSbyEmployeedId = new Map<Id, Job_Staffing__c>();
                List<Job_Staffing__c> lstjStaffs = new List<Job_Staffing__c>();
                for (Job__c job : mapJobs.get(loc)) {
                    if (loc != null) jobLocation = loc.substringBefore('//');
                    if (job.General_Contractor__c != null && job.General_Contractor__r.Name != jGCs) {
                        if (jGCs == '') jGCs = job.General_Contractor__r.Name;
                        else jGCs += ', ' + job.General_Contractor__r.Name;
                    }

                    if (job.Job_Number__c != null) {
                    	String jobLink = (isSF1() ? 'javascript:getLink(\''+job.Id+'\');' : '/' + job.Id);
                        if (jNumbers == '') {
                        	if (draggable.equals('true')) {
	                        	jNumbers = '<a href="' + jobLink + '" target="_self"><strong ondrop="drop(event, \''+job.Id+'\')" ondragover="allowDrop(event)">' + job.Job_Number__c+'</strong></a>';
                        	}
                        	else {
	                        	jNumbers = '<a href="' + jobLink + '" target="_self"><strong>' + job.Job_Number__c+'</strong></a>';
                        	}
	                        jNumbersText = job.Job_Number__c;
                        }
                        else {
                        	if (draggable.equals('true')) {
	                    		jNumbers += ', ' + '<a href="' + jobLink + '" target="_self"><strong ondrop="drop(event, \''+job.Id+'\')" ondragover="allowDrop(event)">' + job.Job_Number__c+'</strong></a>';
                        	}
                        	else {
	                    		jNumbers += ', ' + '<a href="' + jobLink + '" target="_self"><strong>' + job.Job_Number__c+'</strong></a>';
                        	}
	                        jNumbersText += ',' + job.Job_Number__c;
                        }
                    }

                    if (jManager == '') {
	                   	String link = (isSF1() ? 'javascript:getLink(\''+job.OwnerId+'\');' : '/' + job.OwnerId);
	                   	jManager = '<a href="' + link + '" target="_self">' + job.Owner.Name+'</a>';
                    	jManagerText = job.Owner.Name;
                    }

                    if (job.General_Foreman__c != null) {
                        if (jForeMan == '') {
	                    	String link = (isSF1() ? 'javascript:getLink(\''+job.General_Foreman__c+'\');' : '/' + job.General_Foreman__c);
	                    	jForeMan = '<a href="' + link + '" target="_self">' + job.General_Foreman__r.Name+'</a>';
                        	jForeManText = job.General_Foreman__r.Name;
                        }
                    }

                    //if (job.General_Foreman__c != null && job.General_Foreman__r.Company_Phone__c != null) {
                    //    if (foremanPhone == '') foremanPhone = job.General_Foreman__r.Company_Phone__c;
                    //}

                    if (job.Job_Short_Name__c != null) {
                        if (jShorts == '') jShorts = job.Job_Short_Name__c;
                        else jShorts += ', ' + job.Job_Short_Name__c;
                    }

                    if (job.Job_Staffing__r.size() > 0) {
                        for (Job_Staffing__c js : job.Job_Staffing__r) {
                            mapJSbyEmployeedId.put(js.Employee__c, js);
                        }
                    }
                    
                    jID = job.Id;
                }
                if (!mapJSbyEmployeedId.isEmpty()) {
                    JobStaffingWrapper[] staffsList = new List<JobStaffingWrapper>();
                    for (Job_Staffing__c js : mapJSbyEmployeedId.values()) {
                        staffsList.add(new JobStaffingWrapper(js));                               
                    }
                    // Sort the wrapper objects using the implementation of the 
                    // compareTo method.
                    staffsList.sort();

                    for (JobStaffingWrapper jw : staffsList) {
                        lstjStaffs.add(jw.theStaff);
                    }
                 //   lstjStaffs = new List<Job_Staffing__c>(setStaffs);
                }

                rows[0].addValue(jobLocation); // Add the job location to the first row, so we can use it as a header
                rows[1].addValue(jGCs);
                rows[2].addValue(jManager);
                rows[3].addValue(jForeMan);
                //rows[4].addValue(foremanPhone);
                rows[4].addValue(jShorts);
                rows[5].addValue(jNumbers);
                
                rows[0].text.add(jobLocation);
                rows[1].text.add(jGCs);
                rows[2].text.add(jManagerText);
                rows[3].text.add(jForeManText);
                rows[4].text.add(jShorts);
                rows[5].text.add(jNumbersText);                

                // Add the list of Staffings related to the job
                if (lstjStaffs.size() > 0) {
                    rows[6].addStaffs(lstjStaffs);
                }
                // If no related staffings, then add blank value to Row 6 (list of Staffs)
                else {
                    rows[6].addStaffs(new List<Job_Staffing__c>());
                }                
            }

            // Add the Internal Job after we get all the other jobs, so it shows as the last column
            if (getInternalJobs() != null) {
                Map<Id, Job_Staffing__c> mapStaffsbyEmployeedId = new Map<Id, Job_Staffing__c>();
                
                List<Job__c> theInternalJobs = getInternalJobs();
                
                for (Job__c theJob : theInternalJobs) {
                	String jobLink = (isSF1() ? 'javascript:getLink(\''+theJob.Id+'\');' : '/' + theJob.Id);
                	
                    rows[0].addValue('Unassigned'); 
                    rows[1].addValue(theJob.General_Contractor__r.Name);
                    rows[2].addValue(theJob.Owner.Name);
                    rows[3].addValue(theJob.General_Foreman__r.Name);
                    //rows[4].addValue(theJob.General_Foreman__r.Company_Phone__c);
                    rows[4].addValue(theJob.Job_Short_Name__c);
                    rows[5].addValue('<a href="' + jobLink + '" target="_self"><strong ondrop="drop(event, \''+theJob.Id+'\')" ondragover="allowDrop(event)">' + theJob.Job_Number__c+'</strong></a>');

	                rows[0].text.add('Unassigned');
	                rows[1].text.add(theJob.General_Contractor__r.Name);
	                rows[2].text.add(theJob.Owner.Name);
	                rows[3].text.add(theJob.General_Foreman__r.Name);
	                rows[4].text.add(theJob.Job_Short_Name__c);
	                rows[5].text.add(theJob.Job_Number__c);                

                    List<Job_Staffing__c> lstjStaffs = new List<Job_Staffing__c>();
                    if (theJob.Job_Staffing__r.size() > 0) {
                        JobStaffingWrapper[] staffsList = new List<JobStaffingWrapper>();
                        for (Job_Staffing__c js : theJob.Job_Staffing__r) {
                            staffsList.add(new JobStaffingWrapper(js)); 
                        }
                        staffsList.sort();

                        for (JobStaffingWrapper jw : staffsList) {
                            lstjStaffs.add(jw.theStaff);
                        }
                    }
                    // Add the list of Staffings related to the job
                    if (lstjStaffs.size() > 0) {                    
                        rows[6].addStaffs(lstjStaffs);
                    }
                    // If no related staffings, then add blank value toRow 6 (list of Staffs)
                    else {
                        rows[6].addStaffs(new List<Job_Staffing__c>());
                    }                
                }               
            }
    	}

        titleText = 'Job Board - ' + divParam;
    	
		// update the total active employee count
		if (divParam == 'All') {
			List <Contact> lst = [select Id from Contact where RecordType.DeveloperName = 'Employee_Contact' and Employee_Status__c = 'Active'];    	
    	    activeEmpCnt = lst.size() + ' active employees';
		}
		else {
			List <Contact> lst = [select Id from Contact where RecordType.DeveloperName = 'Employee_Contact' and Employee_Status__c = 'Active' and Employee_Type__c = :divParam];    	
    	    activeEmpCnt = lst.size() + ' active employees';
		}
    	
    	return rows;
    }
    
 
	// Get a map of JobRoleColor custom setting 
	private static  Map<String, JobRoleColor__c> mapJobColors;
	private static  List<String> lstRoles;

	static {
       		mapJobColors = JobRoleColor__c.getAll();	
       		lstRoles = new List<String>();
       		for (JobRoleColor__c jc : mapJobColors.values()) {
                String roleOrder = String.valueOf(jc.Order__c)+'//'+jc.Name;
       			lstRoles.add(roleOrder);
       		}
       		lstRoles.sort();		
	}

	// Split the JobRoleColor in 2 lists, so we can use in the legend block with 2 columns
	// Create a list of the first half of Job Role Colors
	public list<JobColor> lstJobColors1 {
		get {
            List<String> lstDivisionJobs = new List<String>();
            for (String divRole : lstRoles) {
                String theRole = divRole.substringAfter('//');
                if (mapJobColors.get(theRole).Division_s__c != null && mapJobColors.get(theRole).Division_s__c.contains(division)) {
                    lstDivisionJobs.add(theRole);
                }
            }
			if (lstJobColors1 == null) {
				lstJobColors1 = new List<JobColor>();
				
				Integer counts = 0;
				counts = lstDivisionJobs.size()/2;
				
				// For each Role defined as a key in mapJobColors, add Name and HEX to the lstJobColors
				for (Integer i=0; i<counts; i++) {
					String fc = lstDivisionJobs[i];
					lstJobColors1.add(new JobColor(fc, mapJobColors.get(fc).HEX__c));
				}
			}
			return lstJobColors1;
		}
		set;
	}
		
	// Create a list of the second set of Job Role Colors
	public list<JobColor> lstJobColors2 {
		get {
            List<String> lstDivisionJobs = new List<String>();
            for (String divRole : lstRoles) {
                String theRole = divRole.substringAfter('//');
                if (mapJobColors.get(theRole).Division_s__c.contains(division)) {
                    lstDivisionJobs.add(theRole);
                }
            }
			if (lstJobColors2 == null) {
				lstJobColors2 = new List<JobColor>();
				
				Integer counts = 0;
				counts = lstDivisionJobs.size()/2;
				
				// For each Role defined as a key in mapJobColors, add Name and HEX to the lstJobColors
				for (Integer i=counts; i<lstDivisionJobs.size(); i++) {
					String fc = lstDivisionJobs[i];
					lstJobColors2.add(new JobColor(fc, mapJobColors.get(fc).HEX__c));
				}
			}
			return lstJobColors2;
		}
		set;
	}

    public PageReference doRun() {
        PageReference pgRef = new PageReference('/apex/JobBoard');
        pgRef.getParameters().put('division', division);
        pgRef.getParameters().put('showHeader', showHeader);
        pgRef.setRedirect(true);
        return pgRef;
    }




	public String jobBoardCSV {
		public get {
			String output = '';
			List <JobWrapper> lst = getJobWrappers();

			for (Integer i=0; i<6; i++) {
				if (i == 0) output += '"Customer Account",';
				if (i == 1) output += '"General Contractor",';
				if (i == 2) output += '"Project Manager",';
				if (i == 3) output += '"General Foreman",';
				if (i == 4) output += '"Job Short Name",';
				if (i == 5) output += '"Job Number",';
				for (String s : lst[i].text) {
					output += '"' + (s == null ? '' : s) + '",';
				}
				output += '\n';
			}
			
			// get the count of the longest list of staffers (max)
			//
			Integer max = 0;
			for (JobStaffListHelper js : lst[6].staffHelpers) {
				if (max < js.lstJobStaffings.size()) max = js.lstJobStaffings.size();
			}

			for (Integer i=0; i<max; i++) {
				output += '"Job Staffing '+(i+1)+'",';
				for (Integer j=0; j<lst[6].staffHelpers.size(); j++) {
					system.debug('===>>> i: '+i+', j: '+j+', lst[6].staffHelpers[j].size(): '+lst[6].staffHelpers[j].lstJobStaffings.size());
					if (lst[6].staffHelpers[j].lstJobStaffings.size() > i) {
						output += '"' + lst[6].staffHelpers[j].lstJobStaffings[i].Employee__r.Name + '",';
					}
					else {
						output += '"",';
					}
				}
				output += '\n';
			}
			return output;
		} 
		private set;
	}








    // Nested class that wraps information about a row  
    public class JobWrapper {

    	// The values (cells) making up this row () --- i.e. Contractor, Project Manager and etc.
    	public List<String>                 values        {get; set;}

    	// The plain text values making up this row for export
    	public List<String>                 text          {get; set;}

    	// The list of Jobs that we will use for Header values
    	public List<Job__c>                 lstJobs       {get; set;}

    	// The values of all the staffings per job
    	public List<List<Job_Staffing__c>>  lstListStaffs {get; set;}
    	public List <String> lstStaffCount {get; set;}
    	public List <JobStaffListHelper> staffHelpers {get; set;}
    	
        public Integer                      rowNum        {get; set;}
        public List <String>                jobIds        {get; set;}

    	// constructor
    	public JobWrapper(Integer i) {
    		values        = new List<String>();
    		text          = new List<String>();
    		lstJobs       = new List<Job__c>();
    		lstListStaffs = new List<List<Job_Staffing__c>>();
            rowNum        = i;
            jobIds        = new List <String> ();
            lstStaffCount = new List <String> ();
            staffHelpers   = new List <JobStaffListHelper> ();
    	}
    	
    	// A method to append a value (cell) to the row
    	public void addValue(String value) {
    		values.add(value);
    	}

    	// A method to append a job value
    	public void addJob(Job__c job) {
    		lstJobs.add(job);
    	}


    	// A method to append a value of Job Staff to the row
    	//
    	public void addStaffs(List<Job_Staffing__c> jobstaffs) {
    		staffHelpers.add(new JobStaffListHelper(jobstaffs));
    	}
    	
    	
    }
    
    public class JobStaffListHelper {
    	public List <Job_Staffing__c> lstJobStaffings {get; set;}
    	public String staffCount {get; set;}
    	public JobStaffListHelper(List<Job_Staffing__c> jobstaffs) {
    		lstJobStaffings = jobstaffs;
    		if (jobstaffs.size() == 0) {
	    		staffCount = '';
    		}
    		else if (jobstaffs.size() == 1) {
	    		staffCount = '1 staffer';
    		}
    		else {
	    		staffCount = jobstaffs.size() + ' staffers';
    		}
    	}
    }
  
  	
    // A helper class for Job role color, which will be used for the color legend
    // This will dynamically change the job role Color in the dashboard based on the values the admin put in the custom setting
    public class JobColor {
    	public String job {get; set;}
    	public String color  {get; set;}
    	
    	public JobColor (String job, String color) {
    		this.job    = job;
    		this.color  = color;
    	}
    }
   
}