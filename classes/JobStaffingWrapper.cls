/*
** Class:  JobStaffingWrapper
** Created by OpFocus on 7/15/2014
** Description: A helper class that we use for sort Job Staffings by Role Order number
*/
global class JobStaffingWrapper implements Comparable {

    public Job_Staffing__c theStaff;
    
    // Constructor
    public JobStaffingWrapper(Job_Staffing__c js) {
        theStaff = js;
    }
    
    // Compare Job staffings based on the Role Order number.
    global Integer compareTo(Object compareTo) {
        // Cast argument to JobStaffingWrapper
        JobStaffingWrapper compareToStaff = (JobStaffingWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (theStaff.RoleOrder__c > compareToStaff.theStaff.RoleOrder__c) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (theStaff.RoleOrder__c < compareToStaff.theStaff.RoleOrder__c) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}