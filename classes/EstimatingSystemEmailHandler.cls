/*
** Class:  EstimatingSystemEmailHandler
** Created by OpFocus on 08/10/2015
** Description: Class contains email handler for Estimating System to SF Export CSV file
** 	that is submitted to SF from external system. Map columns in CSV file to one or more
**  target SF objects: Opportunity, Job, and/or Job Quote. 
**
**	Assumptions
** 	1. This email handler relies on the  custom setting to get the mappings
**		from Column -> Opportunity/Job/Job Quote fields. See Setup->Develop->Custom Settings
**	2. It expects the input CSV file to have columnn headings that match 
**		what is in the above Custom Setting.
**	3. The only column heading names that are not customizable are the lookup columns. So for this
**		report, those are Estimate Id, Job Number, and CO #. All other headings are configurable
**		through custom setting.
**	4. There is no requirement on the ordering of the columns.
** 	5. If a Job Number is not found in SF, it is ignored.
** 	6. If a row has extra columns, they are ignored.
**  7. If an email is received with no attachment or more than 1, it is not processed.
**  8. If a row does not have enough columns, the objects are still created or updated only populating
**		the fields from the columns that exist.
**	9. There is an EmailService associated with this EmailHandler that will invoke this Email Handler
**		when it receives an Email. See Setup->Develop->Email Services for details.
** 	10. Following each import, an email is generated and sent to the recipients specified in custom
**		setting. 
**
** 	The list of recipients of the Status message are defined in the CSV Mapping 
** 	custom setting. It defaults to activations@opfocus.com. The record with Is_Lookup_Field__c
**  set to true should define the Email_Recipients__c field. 
*/
global class EstimatingSystemEmailHandler implements Messaging.InboundEmailHandler {
	private CSVEmailHandlerHelper helper = null;
	private Map<String, Schema.SObjectField> mapJobFieldsByName = new Map<String, Schema.SObjectField>();
	private Map<String, Schema.SObjectField> mapOppFieldsByName = new Map<String, Schema.SObjectField>();
	private Map<String, Schema.SObjectField> mapJobQuoteFieldsByName = new Map<String, Schema.SObjectField>();
	private List<List<String>> lstOppRows = new List<List<String>>();
	private List<List<String>> lstOppAndJobRows = new List<List<String>>();
	private List<List<String>> lstJobAndQuoteRows = new List<List<String>>();


	public EstimatingSystemEmailHandler()   {
	}

	// handle email received from Email Service.
 	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
        	Messaging.InboundEnvelope envelope) {
 		Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

		helper = new CSVEmailHandlerHelper(REPORT_NAME, 
			new CSVImportFileStatusEmail(REPORT_NAME));
		// map column headings to field names using custom setting. Order by Name which is column position in file
		helper.createFieldMappings(new String[]{'Opportunity', 'Job__c', 'Job_Quote__c'});

		Messaging.InboundEmail.TextAttachment attachment = helper.getAttachment(email);
		// if we are missing an attachment or have more than 1, return failure now.
		if (attachment == null) {
			result.success = false;
			return result;	
		}

		// read the CSV file and get the target field names
		helper.readCsvFile(attachment);
		
		mapOppFieldsByName = Opportunity.getSObjectType().getDescribe().fields.getMap();
		mapJobFieldsByName = Job__c.getSObjectType().getDescribe().fields.getMap();
		mapJobQuoteFieldsByName = Job_Quote__c.getSObjectType().getDescribe().fields.getMap();

		// now determine our destination object(s) and dispatch work to helpers.

		// get our ids to determine which object to create as follows
		// 1. Estimate ID defined, EGS Job# undefined, Job Quote# undefined : update Opportunity only            
		// 2. Estimate ID and EGS Job# defined: Opportunity AND Job are updated
		// 3. Change Order# is defined: update Job Quote. (CO Desc may or may not be defined.) IF 
		//		CO# does not correspond to an existing Job Quote record in SF, create one 

		// TODO : What do we do if columns are not found for lookup fields?
		// TODO : What do we do if a cell value is not formatted correctly - SF will throw exception
		Integer colNum = 0;
		for (String columnHeading : helper.columnHeadings) {
			if (columnHeading.normalizeSpace().equalsIgnoreCase(ESTIMATE_ID)) {
				estimateIdColNum = colNum;
			}
			if (columnHeading.normalizeSpace().equalsIgnoreCase(JOB_NUMBER)) {
				jobNumColNum = colNum;
			}
			if (columnHeading.normalizeSpace().equalsIgnoreCase(JOB_QUOTE_NUMBER)) {
				jobQuoteNumColNum = colNum;
			}
			colNum++;
		}

		// now walk through the rows and figure out what is our destination object.		
		for (List<String> lstCells : helper.lstRows) {
			Boolean blnFoundMatch = false;
			// NOTE: Assumption is that the lookup fields are in the beginning of the CSV file, otherwise
			//  	 we run the risk of going past the end of the List of Cells if a particular
			///		 lookup field is not defined, there will not be an entry for it in the Cell

			// update Opportunity only
			if ((lstCells.get(estimateIdColNum) != null && lstCells.get(estimateIdColNum)  != '') && 
			    (lstCells.get(jobNumColNum) == null || lstCells.get(jobNumColNum) == ''))  {
					lstOppRows.add(lstCells);
					blnFoundMatch = true;
			}
			// update Opportunity and Job
			if ((lstCells.get(estimateIdColNum) != null && lstCells.get(estimateIdColNum) != '') && 
				(lstCells.get(jobNumColNum) != null && lstCells.get(jobNumColNum) != '') &&
				(lstCells.get(jobQuoteNumColNum) == null || lstCells.get(jobQuoteNumColNum) == '')) {
					lstOppAndJobRows.add(lstCells);
					blnFoundMatch = true;
			}

			// update or insert Job Quote and Job
			if (lstCells.get(jobQuoteNumColNum) != null && lstCells.get(jobQuoteNumColNum) != '') {
				lstJobAndQuoteRows.add(lstCells);
				blnFoundMatch = true;
			}

			if (!blnFoundMatch) {
				// we have a row that does not meet one of the lookup criteria - add it to error message
				helper.statusEmail.addError('Incorrect lookup fields for Row: \'' + lstCells + '\'');
			}
		}
		System.debug('==========> There are ' + lstOppRows.size() + ' Opportunity rows to process.');
		helper.mapFieldsByName = mapOppFieldsByName;
		if (!lstOppRows.isEmpty()) {
			handleOpportunityRows();
		}

		System.debug('==========> There are ' + lstOppAndJobRows.size() + ' Job rows to process.');
		// first reset helper's map of Fields
		helper.mapFieldsByName = mapJobFieldsByName;
		if (!lstOppAndJobRows.isEmpty()) {
			handleJobRows();
		}
		
		// first reset helper's map of Fields
		helper.mapFieldsByName = mapJobQuoteFieldsByName;
		System.debug('==========> There are ' + lstJobAndQuoteRows.size() + ' Job Quote rows to process.');
		if (!lstJobAndQuoteRows.isEmpty()) {
			handleJobQuoteRows();
		}

		helper.statusEmail.send();
 		result.success = true;
		return result; 				
 	}

 	private void handleOpportunityRows() {
 		// get the Opportunities for these rows
 		List<SObject> lstOpps = helper.getTargetObjects(
 			'select Id, Name, Estimate_Id__c from Opportunity where Estimate_Id__c in (', estimateIdColNum, 
 				lstOppRows, false);  
		Map<String, Id> mapOppIdsByEstimateId = new Map<String, Id>();
		for (SObject sobj : lstOpps) {
			Opportunity opp = (Opportunity)sobj;
			System.debug('==========> Found Target Object for Lookup Id = ' + opp.Estimate_Id__c);
			mapOppIdsByEstimateId.put(opp.Estimate_Id__c, opp.Id);
		}		
		updateOpportunities(mapOppIdsByEstimateId);
 	}

 	private void updateOpportunities(Map<String, Id> mapOppIdsByEstimateId) {
		List<Opportunity> lstOpps = new List<Opportunity>();
		Integer rowIndex = 0;
		for (List<String> lstCells : lstOppRows) {
			CSVEmailHandlerHelper.TargetObject target = helper.getTargetObject(lstCells, estimateIdColNum, 
				rowIndex, mapOppIdsByEstimateId);
			// error with this row already marked - just ignore.
			if (target == null) {
				rowIndex++;
				continue;
			}
			// failed to find Opportunity object
			if (!target.blnFoundObject) {
				helper.statusEmail.addError('Failed to find Opportunity for Estimate Id ' + 
					target.lookupId);
				rowIndex++;
				continue;
			}
			rowIndex++;
		    Opportunity opp = new Opportunity(Id = target.Id, Estimate_Id__c = target.lookupId);
		    System.debug('==========> Created Opportunity with Id= ' + opp.Id);
		    if (!helper.mapFields(opp, 'Opportunity', ESTIMATE_ID, lstCells)) {
		    	helper.statusEmail.addError('Failed to map fields for Opportunity for Estimate Id ' + 
		    		target.lookupId);
		    } else {
		    	System.debug('==========> Opp StageName = ' + opp.StageName);
		    	if (opp.StageName == null || (!opp.StageName.equals(AWARDED) && 
		    		!opp.StageName.equals(LOST) && !opp.StageName.equals(SUBMITTED))) {
					helper.statusEmail.addError('Failed to import budget details for Opportunity ' + opp.Id + 
						' (StageName = ' + opp.StageName + 
						') as that is only done for Submitted, Awarded or Lost Opportunities.');
		    		continue; // do not update this opportunity.
		    	}
			    helper.statusEmail.addSuccess('Estimate Id ' + target.lookupId);
			    lstOpps.add(opp);
		    }
		}
		if (!lstOpps.isEmpty()) {
			update lstOpps;
		}
		helper.setRowsToIgnore = new Set<Integer>(); // empty its contents for next object
 	}

 	/********** JOB PROCESSING **********/
 	private void handleJobRows() {
 		// get the Jobs for these rows
  		List<SObject> lstJobs = helper.getTargetObjects(
 			'select Id, Job_Number__c  from Job__c where Job_Number__c in (', jobNumColNum, 
 			lstOppAndJobRows, false);
  		List<SObject> lstOpps = helper.getTargetObjects(
 			'select Id, Estimate_Id__c from Opportunity where Estimate_Id__c in (', estimateIdColNum, 
 			lstOppAndJobRows, false);
		
		Map<String, Id> mapOppIdsByEstimateId = new Map<String, Id>();
		for (SObject sobj : lstOpps) {
			Opportunity opp = (Opportunity)sobj;
			System.debug('==========> Found Target Object for Lookup Id = ' + opp.Estimate_Id__c);
			mapOppIdsByEstimateId.put(opp.Estimate_Id__c, opp.Id);
		}		

		Map<String, Id> mapJobIdsByNumber = new Map<String, Id>();
		for (SObject sobj : lstJobs) {
			Job__c job = (Job__c)sobj;
			System.debug('==========> Found Job Number = ' + job.Job_Number__c);
			mapJobIdsByNumber.put(job.Job_Number__c, job.Id);
		}
		updateJobs(mapJobIdsByNumber, mapOppIdsByEstimateId);
 	}

 	private void updateJobs(Map<String, Id> mapJobIdsByNumber, Map<String, Id> mapOppIdsByEstimateId) {
		List<Job__c> lstJobs = new List<Job__c>();
		List<Opportunity> lstOpps = new List<Opportunity>();

		Integer rowIndex = 0;
		for (List<String> lstCells : lstOppAndJobRows) {
			CSVEmailHandlerHelper.TargetObject jobTarget = helper.getTargetObject(lstCells, jobNumColNum, 
				rowIndex, mapJobIdsByNumber);
			if (jobTarget == null) {
				rowIndex++;
				continue;
			}
			if (!jobTarget.blnFoundObject) {
		    	helper.statusEmail.addError('Failed to find Job for Job Number ' + jobTarget.lookupId);				
				rowIndex++;
				continue;
			}
			CSVEmailHandlerHelper.TargetObject oppTarget = helper.getTargetObject(lstCells, estimateIdColNum, 
				rowIndex, mapOppIdsByEstimateId);
			if (oppTarget == null) {
				rowIndex++;
				continue;
			}
			if (!oppTarget.blnFoundObject) {
				rowIndex++; 
		    	helper.statusEmail.addError('Failed to find Opportunity for Estimate Id ' + oppTarget.lookupId);
				continue;
			}
			rowIndex++;

		    System.debug('==========> Updating Job object for Job Number ' + jobTarget.lookupId);
		    Job__c job = new Job__c(Id = jobTarget.Id, Job_Number__c = jobTarget.lookupId);
		    Boolean blnJobAdded = helper.mapFields(job, 'Job__c', JOB_NUMBER, lstCells);

		    // now map Opportunity fields
		    helper.mapFieldsByName = mapOppFieldsByName;
			Opportunity opp = new Opportunity(Id = oppTarget.Id, Estimate_Id__c = oppTarget.lookupId);
		    Boolean blnOppAdded = helper.mapFields(opp, 'Opportunity', ESTIMATE_ID, lstCells);
		    String emailMessage = 'Estimate Id ' + oppTarget.lookupId + 
			    	', Job Number ' + jobTarget.lookupId;
		    if (blnJobAdded && blnOppAdded) {
			    helper.statusEmail.addSuccess(emailMessage);
		    } else {
			    helper.statusEmail.addError('Failed to map fields for ' + emailMessage);    	
		    }
		  	if (blnJobAdded) lstJobs.add(job);
		  	if (blnOppAdded) lstOpps.add(opp);
		}
		if (!lstJobs.isEmpty()) {
			update lstJobs;
		}
		if (!lstOpps.isEmpty()) {
			update lstOpps;
		}
		helper.setRowsToIgnore = new Set<Integer>(); // empty its contents for next object
 	}

 	/********** JOB QUOTE PROCESSING **********/
 	private void handleJobQuoteRows() {
 		// get the associated Jobs for our Job Quotes
  		List<SObject> lstJobs = helper.getTargetObjects(
 			'select Id, Job_Number__c  from Job__c where Job_Number__c in (', jobNumColNum, 
 			lstJobAndQuoteRows, true);

		// get the Job Quotes for these rows
  		List<SObject> lstJobQuotes = helper.getTargetObjects(
 			'select Id, EGS_Job_Quote__c, EGS_Job__c, Job__c from Job_Quote__c where EGS_Job_Quote__c in (', jobQuoteNumColNum, 
 			lstJobAndQuoteRows, false);
		
		Map<String, Id> mapJobQuoteIdsByNumber = new Map<String, Id>();
		Map<String, Job_Quote__c> mapJobQuotesByNumber = new Map<String, Job_Quote__c>();
		for (SObject sobj : lstJobQuotes) {
			Job_Quote__c jobQuote = (Job_Quote__c)sobj;
			System.debug('==========> Found Job Quote = ' + jobQuote.EGS_Job_Quote__c);
			mapJobQuoteIdsByNumber.put(jobQuote.EGS_Job_Quote__c, jobQuote.Id);
			mapJobQuotesByNumber.put(jobQuote.EGS_Job_Quote__c, jobQuote);
		}

		Map<String, Id> mapJobIdsByNumber = new Map<String, Id>();
		for (Sobject obj : lstJobs) {
			Job__c job = (Job__c)obj;
			mapJobIdsByNumber.put(job.Job_Number__c, job.Id);
		}

		// See if we have a corresponding Job Quote record. If not, create an empty
		// one now so we can populate it and insert it into the database later.
		for (List<String> row : lstJobAndQuoteRows) {
			System.debug('==========> seeing if we have a record in db for ' + row.get(jobQuoteNumColNum));
			if (mapJobQuotesByNumber.get(row.get(jobQuoteNumColNum)) == null) {
				// create a Job Quote record now for insertion later
				Id jobId = mapJobIdsByNumber.get(row.get(jobNumColNum));
				System.debug('==========> Setting jobId = ' + jobId + ' for Job Quote ' + row.get(jobQuoteNumColNum));
				Job_Quote__c jobQuote = new Job_Quote__c(EGS_Job_Quote__c = row.get(jobQuoteNumColNum),
					Job__c = jobId);
				System.debug('=======> jobQuote = ' + jobQuote);
				mapJobQuoteIdsByNumber.put(jobQuote.EGS_Job_Quote__c, null);
				System.debug('==========> Putting row in mapJobQuotesByNumber with key = ' + 
					jobQuote.EGS_Job_Quote__c);
				mapJobQuotesByNumber.put(jobQuote.EGS_Job_Quote__c, jobQuote);
			}
		}	
		updateJobQuotes(mapJobQuoteIdsByNumber, 
			mapJobQuotesByNumber, mapJobIdsByNumber); 
 	}

 	private void updateJobQuotes(Map<String, Id> mapJobQuoteIdsByNum, 
 			Map<String, Job_Quote__c> mapJobQuotesByNumber,
 			Map<String, Id> mapJobIdsByNumber) {

		List<Job_Quote__c> lstJobQuotes = new List<Job_Quote__c>();
		List<Job__c> lstJobs = new List<Job__c>();
		Integer rowIndex = 0;

		for (List<String> lstCells : lstJobAndQuoteRows) {
			CSVEmailHandlerHelper.TargetObject jobQuoteTarget = helper.getTargetObject(lstCells, jobQuoteNumColNum, 
				rowIndex, mapJobQuoteIdsByNum);
			CSVEmailHandlerHelper.TargetObject jobTarget = helper.getTargetObject(lstCells, jobNumColNum, 
				rowIndex, mapJobIdsByNumber);

			// error in row from before so ignore and carry on
			if (jobQuoteTarget == null || jobTarget == null) {
				rowIndex++;
				continue;
			}
			// if we failed to find the Job for this Job Quote
			if (!jobTarget.blnFoundObject) {
				rowIndex++;
		    	helper.statusEmail.addError('Failed to find Job for Job Number ' + jobTarget.lookupId);
				continue;				
			}

			rowIndex++;
		    System.debug('==========> Updating Job Quote object for Job Quote Number ' + 
		    	jobQuoteTarget.lookupId + ' and Job Number ' + jobTarget.lookupId);
		    Job_Quote__c jobQuote = new Job_Quote__c(Id = jobQuoteTarget.Id,
		    	 EGS_Job_Quote__c = jobQuoteTarget.lookupId,
		    	 Job__c = mapJobQuotesByNumber.get(jobQuoteTarget.lookupId).Job__c);
		    Boolean blnJobQuoteAdded = helper.mapFields(jobQuote, 'Job_Quote__c', JOB_QUOTE_NUMBER, lstCells);

		    String emailMessage = 'Job Number ' + jobTarget.lookupId + 
			    	', Job Quote Number ' + jobQuoteTarget.lookupId;

		    if (blnJobQuoteAdded) {
			    helper.statusEmail.addSuccess(emailMessage);
		    } else {
			    helper.statusEmail.addError('Failed to map fields for ' + emailMessage);    	
		    }
		  	if (blnJobQuoteAdded) lstJobQuotes.add(jobQuote);
		}
		if (!lstJobQuotes.isEmpty()) {
			// doing an upsert as we want to create new records if a Job Quote does not exist for this CO#
			upsert lstJobQuotes;
		}
		helper.setRowsToIgnore = new Set<Integer>(); // empty its contents for next object
 	}

	private Integer estimateIdColNum = 0;
	private Integer jobNumColNum = 0;
	private Integer jobQuoteNumColNum = 0;

 	private static final String ESTIMATE_ID = 'Estimate ID';
 	private static final String JOB_NUMBER = 'Job Number';
 	private static final String JOB_QUOTE_NUMBER = 'CO #';
 	private static final String REPORT_NAME = 'Estimating System';
 	private static final String AWARDED = 'Awarded';
 	private static final String LOST = 'Lost';
 	private static final String SUBMITTED = 'Submitted';
}