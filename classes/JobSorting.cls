/*
** Class:  JobSorting
** Created by OpFocus on 7/24/2014
** Description: A helper class that we use for sort Jobs by PM and Job Number
*/
global class JobSorting implements Comparable {
	public Job__c theJob;
	// Contructor
	public JobSorting(Job__c job) {
		theJob = job;
	}
    // Compare Jobs based on the PM and JobNumber field.
    global Integer compareTo(Object compareTo) {
        // Cast argument to JobSorting
        JobSorting compareToJob = (JobSorting)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (theJob.PMandJobNumber__c > compareToJob.theJob.PMandJobNumber__c) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (theJob.PMandJobNumber__c < compareToJob.theJob.PMandJobNumber__c) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }

}