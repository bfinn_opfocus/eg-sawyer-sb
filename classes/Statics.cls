/*	
**	Class:    Statics	
**  Created:  July, 2014 by OpFocus
**	Description: Check if the trigger is only running in the current trigger
*/
public class Statics {

	// isInJobStaffingTrigger is true when the Job_Staffing trigger is running
	public static Boolean isInJobStaffingTrigger = false;
	
}