/*
** Class:  CSVImportFileStatusEmail
** Created by OpFocus on 08/10/2015
** Description: Class contains email handler for WeeklyJobCosts CSV file
** 	that is submitted to SF from external system. Map columns in CSV
**/
public with sharing class CSVImportFileStatusEmail {
	public Integer successCount     = 0;
	public Integer failureCount     = 0;
	public Integer numRows          = 0;
	public String emailRecipients   = null;
	public String reportName        = null;

	public List<String> lstSuccesses = new List<String>();
	private List<String> lstErrors = new List<String>();
	private Messaging.SingleEmailMessage emailMsg  = null;
 	private static final String ACTIVATIONS = 'activations@opfocus.com';
 	private static final String SEMICOLON   = ';';
 	private static final String NEWLINE     = '\n';

	public CSVImportFileStatusEmail(String reportName) {
		this.reportName      = reportName;
	}

	public void addError(String errorMsg) {
		lstErrors.add(errorMsg);
		failureCount++;
	}

	public void addSuccess(String message) {
		lstSuccesses.add(message);
		successCount++;
	}

 	// Create and send the Email with Status of this CSV import.
 	public void send() {
 		emailMsg = new Messaging.SingleEmailMessage();
		emailMsg.setSubject(reportName + ' Import Results for ' + Date.today().format());
		
		addBody();
		addRecipients();
		List<Messaging.SendEmailResult> results = 
			Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{emailMsg});
	}

	private void addBody() {
		String errorDetails = '';
		for (String errorMsg : lstErrors) {
			errorDetails += errorMsg + NEWLINE;
		}

		String successDetails = '';
		for (String successMsg : lstSuccesses) {
			successDetails += successMsg + NEWLINE;
		}
		String body = 'This email is to notify you of the results for your ' + reportName + ' Import for ' + 
			Date.today().format() + '.' + NEWLINE + NEWLINE;
		if (successCount > 0) {
			body += 'There were ' + successCount + ' out of ' + numRows + ' records successfully imported as listed below.' + 
			NEWLINE + successDetails + NEWLINE;
		} 
		if (failureCount > 0) {
			body += 'There were ' + failureCount + ' out of ' + numRows + ' records that failed to be imported. Details listed below.' + 
				NEWLINE + errorDetails;
		}
		// if no records were processed, just add errorDetails
		if (successCount == 0 && failureCount == 0) {
			body += errorDetails;
		}
		emailMsg.setPlaintextBody(body);
	}

	private void addRecipients() {
		if (emailRecipients == null || emailRecipients.length() == 0) {
 			// we have no email recipients - just default to activations&@opfocus.com
 			System.debug('==========> There are no Email Recipients specified for Status Email ' + 
 				'so defaulting to activations@opfocus.com. Update Email_Recipients__c field in ' +
 				'CSV_Mapping__c custom setting.');
 			emailRecipients = ACTIVATIONS;
 		}
		emailMsg.setToAddresses(emailRecipients.split(SEMICOLON));
	}
}