/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
** Class:  Test_SlipAdjustments
** Created by OpFocus on 7/24/2014
** Description: Unit test for the SlipAdjustments VF page/controller
*/
@isTest
private class Test_SlipAdjustments {

    static testMethod void testSlip() {

		SlipAdjustmentSettings__c settings = new SlipAdjustmentSettings__c();
		settings.ProfilesPricingVisible__c = 'EG Sawyer Executive,EG Sawyer Project Manager,EG Sawyer Purchasing,EG Sawyer System Administrator,LCN Networks Executive,LCN Networks Project Manager,LCN Networks Purchasing,LCN System Administrator,System Administrator';
		insert settings;

        Account a1 = UnitTestUtils.CreateAccount('Test Co1');
        Account a2 = UnitTestUtils.CreateAccount('Test Co2');
        Account specialAcct = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');
        insert new List<Account>{a1, a2, specialAcct};

        Contact emp1 = UnitTestUtils.createContact('testlastname1', a1.Id, '7098908909', empRtId);
        Contact emp2 = UnitTestUtils.createContact('testlastname2', a1.Id, '7778905555', empRtId);
        Contact emp3 = UnitTestUtils.createContact('testlastname3', a1.Id, '5558888909', empRtId);
        Contact emp4 = UnitTestUtils.createContact('testlastname4', a1.Id, '4442221234', empRtId);
        emp4.Project_Manager__c = true;
        //emp4.Employee_Status__c = 'Active';
        Contact emp5 = UnitTestUtils.createContact('testlastname5', a1.Id, '4442220000', empRtId);
        emp5.Primary_Role__c = 'General Foreman';
        emp5.General_Foreman__c = true;
        emp5.Employee_Status__c = 'Active';
        insert new List<Contact>{emp1, emp2, emp3, emp4, emp5};

        Opportunity opp1 = UnitTestUtils.createOpp('opp1', a1.Id);
        Opportunity opp2 = UnitTestUtils.createOpp('opp2', a1.Id);
        Opportunity opp3 = UnitTestUtils.createOpp('opp3', a1.Id);
        Opportunity opp4 = UnitTestUtils.createOpp('opp4', a1.Id);
        opp1.Type = 'E.G. Sawyer';
        opp2.Type = 'LCN Networks';
        insert new List<Opportunity>{opp1, opp2, opp3, opp4};

        Job__c job1 = UnitTestUtils.createJob('Job1', a1.Id, opp1.Id, 'a1 job1');
        job1.Job_Short_Name__c = 'job1';
        //job1.Project_Manager__c = emp4.Id;
        job1.General_Contractor__c = a2.Id;
        job1.General_Foreman__c    = emp5.Id;
        job1.Eligible_to_Merge_on_Job_Board__c = true;
        job1.Job_Status__c = 'In Progress - Active';
        insert job1;

        Job_Staffing__c js1 = UnitTestUtils.createJS(job1.Id, 'General Foreman', emp1.Id, true);
        Job_Staffing__c js2 = UnitTestUtils.createJS(job1.Id, 'Foreman',      emp2.Id, true);
        Job_Staffing__c js3 = UnitTestUtils.createJS(job1.Id, 'Technician',   emp3.Id, true);
        insert new List<Job_Staffing__c>{js1, js2, js3};

        Job__c job2 = UnitTestUtils.createJob('Job2', a1.Id, opp2.Id, 'a1 job2');
        job2.Job_Short_Name__c = 'job2';
        //job2.Project_Manager__c = emp4.Id;
        job2.General_Contractor__c = a2.Id;
        job2.General_Foreman__c    = emp5.Id;
        job2.Eligible_to_Merge_on_Job_Board__c = true;
        job2.Job_Status__c = 'In Progress - Active';
        insert job2;

        Slip__c slip1 = new Slip__c(Slip_Type__c='Phase 5', Job_Name__c=job1.Id, Slip_Status__c='Open');
        insert slip1;

		List <Labor_Rate__c> lstLR = new List <Labor_Rate__c> {
			new Labor_Rate__c(Role__c='General Foreman', Job__c=job1.Id, Rate__c=150.00, Hourly_Rate__c='Regular Time', Shift__c='Second Shift'),
			new Labor_Rate__c(Role__c='Technician',   Job__c=job1.Id, Rate__c=125.00, Hourly_Rate__c='Regular Time', Shift__c='Second Shift')
		};
		insert lstLR;

		List <Slip_Adjustment__c> lstSA = new List <Slip_Adjustment__c> {
			new Slip_Adjustment__c(Slip__c=slip1.Id, Role__c='General Foreman', Hours__c=10.00, Hourly_Rate__c='Regular Time', Shift__c='Second Shift', Adjust_By__c='Role'),
			new Slip_Adjustment__c(Slip__c=slip1.Id, Role__c='General Foreman', Hours__c=10.00, Hourly_Rate__c='Regular Time', Shift__c='Second Shift', Adjust_By__c='Employee', Job_Staffing__c=js1.Id),
			new Slip_Adjustment__c(Slip__c=slip1.Id, Role__c='General Foreman', Hours__c=10.00, Hourly_Rate__c='Regular Time', Shift__c='Second Shift', Adjust_By__c='Employee', Job_Staffing__c=js2.Id)
		};
		insert lstSA;

		List <Timesheet__c> lstTS = new List <Timesheet__c> {
			new Timesheet__c(Job_Name__c=job1.Id, Start_Date__c=Date.today().addDays(-7), End_Date__c=Date.today())
		};
		insert lstTS;

		List <Time_Entry__c> lstTE = new List <Time_Entry__c> {
			new Time_Entry__c(Job__c=job1.Id, Timesheet__c=lstTS[0].Id, Slip_Number__c=slip1.Id, Job_Staffing__c=js1.Id, Date__c=Date.today(), Hours_Worked__c=8.00, Hourly_Rate__c='Over Time', Shift__c='Second Shift'),
			new Time_Entry__c(Job__c=job1.Id, Timesheet__c=lstTS[0].Id, Slip_Number__c=slip1.Id, Job_Staffing__c=js2.Id, Date__c=Date.today(), Hours_Worked__c=8.00, Hourly_Rate__c='Over Time', Shift__c='Second Shift'),
			new Time_Entry__c(Job__c=job1.Id, Timesheet__c=lstTS[0].Id, Slip_Number__c=slip1.Id, Job_Staffing__c=js3.Id, Date__c=Date.today(), Hours_Worked__c=8.00, Hourly_Rate__c='Over Time', Shift__c='Second Shift')
		};
		insert lstTE;

		// Every Product has to have a Standard Price that is part of the Standard Price Book before you can create any additional prices in other Price Books
		ID standardPBID = Test.getStandardPricebookId();
		
		Pricebook2 pb = new Pricebook2(IsActive=true, Name = 'E.G. Sawyer');
		insert pb;
		
		List <Product2> lstProd = new List <Product2> {
			new Product2(Name='test', IsActive=true, Family='widgets', Product_Type__c='type', Product_Sub_Type__c='subtype'),
			new Product2(Name='testlongname-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahhhhhhhhhhhhhhhhhhhhhhhhhhhhhh', IsActive=true, Family='widgets', Product_Type__c='type', Product_Sub_Type__c='subtype')
		};
		insert lstProd;
		
		List <PricebookEntry> lstStdPBE = new List <PricebookEntry> {
			new PricebookEntry(Pricebook2Id=standardPBID, Product2Id=lstProd[0].Id, UnitPrice=10),
			new PricebookEntry(Pricebook2Id=standardPBID, Product2Id=lstProd[1].Id, UnitPrice=10)
		};
		insert lstStdPBE;
		
		List <PricebookEntry> lstPBE = new List <PricebookEntry> {
			new PricebookEntry(Pricebook2Id=pb.Id, Product2Id=lstProd[0].Id, UnitPrice=10),
			new PricebookEntry(Pricebook2Id=pb.Id, Product2Id=lstProd[1].Id, UnitPrice=10)
		};
		insert lstPBE;
		

		List <Slip_Materials__c> lstSM = new List <Slip_Materials__c> {
			new Slip_Materials__c(Slip__c=slip1.Id, Product__c=lstProd[0].Id, Units__c=10)
		};
		insert lstSM;

		List <Other_Slip_Charges__c> lstOSC = new List <Other_Slip_Charges__c> {
			new Other_Slip_Charges__c(Slip__c=slip1.Id, Charge_Type__c='Other', Markup__c=10.0)
		};
		insert lstOSC;

		
        List<Job_Staffing__c> lstJob2Staffs = [select Id from Job_Staffing__c where Job_Name__c = :job2.Id];

        //System.assertEquals(3, lstJob2Staffs.size());

        job2.Eligible_to_Merge_on_Job_Board__c = false;
        update job2;

        Job_Staffing__c js4 = UnitTestUtils.createJS(job1.Id, 'Project Manager', emp4.Id, true);
        Job_Staffing__c js5 = UnitTestUtils.createJS(job1.Id, 'Foreman', emp5.Id, true);
        insert new List<Job_Staffing__c>{js4, js5};


        job2.Eligible_to_Merge_on_Job_Board__c = true;
        update job2;

        lstJob2Staffs = [select Id from Job_Staffing__c where Job_Name__c = :job2.Id];
        //System.assertEquals(5, lstJob2Staffs.size());   
            
        Test.startTest();
        
        PageReference pg = Page.SlipAdjustments;
        Test.setCurrentPage(pg);
        
        SlipAdjustments_Controller ctl = new SlipAdjustments_Controller(new ApexPages.StandardController(slip1));

		// {"sid":"a0911000005lD43AAE","showPricing":true,"laborTotalHours":"60.5","laborMarkupPercent":"6.00","laborSubtotal":"1,936.00","laborTotal":"2,052.16","hours":[{"adjustBy":"Employee","role":"General Foreman","jobStaffing":"a0811000003uOKoAAM","hourlyRate":"Regular Time","shift":"Normal","hours":"16","laborrate":"88","key":"Regular Time"},{"adjustBy":"Employee","role":"General Foreman","jobStaffing":"a0811000003uOKoAAM","hourlyRate":"Over Time","shift":"Normal","hours":"2","laborrate":"0","key":"Over Time"},{"adjustBy":"Employee","role":"General Foreman","jobStaffing":"a0811000003uOKoAAM","hourlyRate":"Double Time","shift":"Normal","hours":"6","laborrate":"0","key":"Double Time"},{"adjustBy":"Employee","role":"General Foreman","jobStaffing":"a0811000003uOKoAAM","hourlyRate":"Regular Time","shift":"Second Shift","hours":"0","laborrate":"150","key":"Regular Time - Second Shift"},{"adjustBy":"Employee","role":"Journeyman","jobStaffing":"a0811000003tGiuAAE","hourlyRate":"Regular Time","shift":"Normal","hours":"8","laborrate":"33","key":"Regular Time"},{"adjustBy":"Employee","role":"Journeyman","jobStaffing":"a0811000003tGiuAAE","hourlyRate":"Over Time","shift":"Second Shift","hours":"4.5","laborrate":"0","key":"Over Time - Second Shift"},{"adjustBy":"Employee","role":"Journeyman","jobStaffing":"a0811000003tGiuAAE","hourlyRate":"Regular Time","shift":"Third Shift","hours":"8","laborrate":"0","key":"Regular Time - Third Shift"},{"adjustBy":"Employee","role":"Journeyman","jobStaffing":"a0811000004g8GoAAI","hourlyRate":"Regular Time","shift":"Normal","hours":"8","laborrate":"33","key":"Regular Time"},{"adjustBy":"Employee","role":"Steward","jobStaffing":"a0811000004gBJNAA2","hourlyRate":"Over Time","shift":"Normal","hours":"8","laborrate":"0","key":"Over Time"}],"materialsUseLineitemMarkup":false,"materialsMarkup":"4,922.25","materialsTax":"4,101.88","materialsSubtotal":"65,630.00","materialsTotal":"74,654.13","materials":[{"deleted":false,"sfid":"a0E1100000KmVIXEA3","pid":"01t11000003h1XOAAY","family":"null","ptype":"","subtype":"","name":"Custom Product","size":"1","pcount":"60","markup":"","std":"standard"},{"deleted":false,"sfid":"a0E1100000HuHmFEAV","pid":"01t11000002btCeAAI","family":"Cable","ptype":"Copper","subtype":"","name":"Other - Cable, Copper","size":"1","pcount":"1","markup":"","std":"standard"},{"deleted":false,"sfid":"a0E1100000KmVIWEA3","pid":"01t11000002btChAAI","family":"Cable","ptype":"Coax","subtype":"","name":"Other - Cable, Coax","size":"1","pcount":"30","markup":"","std":"standard"},{"deleted":false,"sfid":"a0E1100000HuBjZEAV","pid":"01t11000002bt9tAAA","family":"Assemblies","ptype":"MIT","subtype":"","name":"Dual Cat6A PVC 1091 - per Assembly","size":"1","pcount":"111","markup":"","std":"standard"},{"deleted":false,"sfid":"a0E1100000HuBjaEAF","pid":"01t11000002btFtAAI","family":"Patch Cord","ptype":"Fiber","subtype":"","name":"10G Duplex 1M","size":"1","pcount":"5","markup":"","std":"standard"}],"otherUseLineitemMarkup":false,"subcontractorMarkup":"10","otherChargesTotal":"1,157.72","other":[{"deleted":false,"sfid":"a0D110000074gqNEAQ","rowid":"zzz","chargeType":"Asbestos Equipment","description":"Asbestos Equipment","markup":"","cost":"110.00"},{"deleted":false,"sfid":"a0D110000074gp0EAA","rowid":"zzz","chargeType":"Lift","description":"Lift","markup":"","cost":"333.00"},{"deleted":false,"sfid":"a0D11000007AKxjEAG","rowid":"zzz","chargeType":"Subcontractor LCN","description":"Subcontractor LCN","markup":"","cost":"100.00"},{"deleted":false,"sfid":"a0D11000007aCGREA2","rowid":"zzz","chargeType":"Parking","description":"Parking","markup":"","cost":"20.00"},{"deleted":false,"sfid":"a0D11000007alU0EAI","rowid":"zzz","chargeType":"Other","description":"test","markup":"","cost":"0.00"},{"deleted":false,"sfid":"a0D110000077tELEAY","rowid":"zzz","chargeType":"Subcontractor Other","description":"Subcontractor Other","markup":"","cost":"500.00"}]}
		String saveJson = 
			'{"sid":"' + slip1.Id + '","showPricing":true,"laborTotalHours":"60.5","laborMarkupPercent":"6.00","laborSubtotal":"1,936.00","laborTotal":"2,052.16",'
			+ '"hours":['
			+ '{"adjustBy":"Employee","role":"General Foreman","jobStaffing":"' + lstSA[0].Id + '","hourlyRate":"Regular Time","shift":"Normal","hours":"16","laborrate":"88","key":"Regular Time"}'
			+ '],'
			+ '"materialsUseLineitemMarkup":false,"materialsMarkup":"4,922.25","materialsTax":"4,101.88","materialsSubtotal":"65,630.00","materialsTotal":"74,654.13",'
			+ '"materials":['
			+ '{"deleted":false,"sfid":"' + lstSM[0].Id + '","pid":"' + lstProd[0].Id + '","family":"null","ptype":"","subtype":"","name":"Custom Product","size":"1","pcount":"60","markup":"","std":"standard"},'
			+ '{"deleted":false,"sfid":"","pid":"' + lstProd[1].Id + '","family":"null","ptype":"","subtype":"","name":"Custom Product","size":"1","pcount":"60","markup":"","std":"standard"}'
			+ '],'
			+ '"otherUseLineitemMarkup":false,"subcontractorMarkup":"10","otherChargesTotal":"1,157.72",'
			+ '"other":['
			+ '{"deleted":false,"sfid":"' + lstOSC[0].Id + '","rowid":"zzz","chargeType":"Other","description":"test","markup":"","cost":"0.00"},'
			+ '{"deleted":false,"sfid":"","rowid":"zzz","chargeType":"Other","description":"test","markup":"","cost":"0.00"}'
			+ ']'
			+ '}';
		SlipAdjustments_Controller.saveAll(saveJson);




        Test.stopTest();    
    }

    private static Id empRtId;
    
    static {
        empRtId = [Select Id, sObjectType, Name From RecordType where SObjectType = 'Contact' and Name='Employee Contact' limit 1].Id;  
    } 


}