/*
** Class:  Test_EstimatingSystemEmailHandler
** Created by OpFocus on 08/20/2015
** Description: Class contains unit tests for Estimatin System Email Handler that maps
** 	values from an attached CSV file into either an Opportunity, Job or Job Quote record 
**  based on different column values in file.
*/
@IsTest
private  class Test_EstimatingSystemEmailHandler {
	private static final Boolean testNoAttachment = true;
	private static final Boolean testNoCSVAttachment = true;
	private static final Boolean testBinaryAttachment = true;
	private static final Boolean testSuccess = true;
	private static final Boolean testExtraColumns = true;
	private static final Boolean testDuplicateEstimateIds = true;
	private static final Boolean testEmptyEmailRecipients = true;
	private static final Boolean testInvalidJobId = true;
	private static final Boolean testSameJobNum = true;

	@IsTest
	static void testNoAttachment() {
		System.assert(testNoAttachment, 'Test Disabled');
		initData(false);
		EstimatingSystemEmailHandler emailHandler = new EstimatingSystemEmailHandler();
		
		Messaging.InboundEmailResult result = null;
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

		Test.startTest();
			result = emailHandler.handleInboundEmail(email, envelope) ;
		Test.stopTest();
		System.assertEquals(false, result.success, 'Did not return false when there was no attachment.');
	}

	@IsTest
	static void testNoCSVAttachment() {
		System.assert(testNoCSVAttachment, 'Test Disabled');
		initData(false);
		EstimatingSystemEmailHandler emailHandler = new EstimatingSystemEmailHandler();
		
		Messaging.InboundEmailResult result = null;
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEmail.TextAttachment textFile = new Messaging.InboundEmail.TextAttachment();
		textFile.body = 'this is a test';
		textFile.mimeTypeSubType = 'text';
		email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
		email.textAttachments.add(textFile);
		Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

		Test.startTest();
			result = emailHandler.handleInboundEmail(email, envelope);
		Test.stopTest();
		System.assertEquals(false, result.success, 'Did not return false when there was no CSV type of attachment.');
	}


	@IsTest
	static void testSuccess() {
		System.assert(testSuccess, 'Test Disabled');
		initData(true);

		EstimatingSystemEmailHandler emailHandler = new EstimatingSystemEmailHandler();
		
		Messaging.InboundEmailResult result = null;
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEmail.TextAttachment csvFile = new Messaging.InboundEmail.TextAttachment();
		csvFile.body = CSV_FILE_CONTENTS;
		csvFile.mimeTypeSubType = 'text/csv';
		email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
		email.textAttachments.add(csvFile);
		Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

		Test.startTest();
			result = emailHandler.handleInboundEmail(email, envelope) ;
		Test.stopTest();
		System.assertEquals(true, result.success, 'Did not return true when there was a valid attachment.');
		System.assertEquals(1, [select Id, Hours__c from Opportunity where Hours__c=100].size(),
			'Did not create Opportunity as expected.');
	}

	@IsTest
	static void testBinaryAttachment() {
		System.assert(testBinaryAttachment, 'Test Disabled');
		initData(true);

		EstimatingSystemEmailHandler emailHandler = new EstimatingSystemEmailHandler();
		
		Messaging.InboundEmailResult result = null;
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEmail.BinaryAttachment csvFile = new Messaging.InboundEmail.BinaryAttachment();
		csvFile.body = Blob.valueOf(CSV_FILE_CONTENTS);
		csvFile.mimeTypeSubType = 'text/csv';
		email.binaryAttachments = new List<Messaging.InboundEmail.BinaryAttachment>();
		email.binaryAttachments.add(csvFile);
		Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

		Test.startTest();
			result = emailHandler.handleInboundEmail(email, envelope) ;
		Test.stopTest();
		System.assertEquals(true, result.success, 'Did not return true when there was a valid attachment.');
		System.assertEquals(1, [select Id, Hours__c from Opportunity where Hours__c=100].size(),
			'Did not create Opportunity as expected.');
	}
	@IsTest
	static void testExtraColumns() {
		System.assert(testExtraColumns, 'Test disabled.');
		initData(true);
		System.debug('==========> testExtraColumns START');
		EstimatingSystemEmailHandler emailHandler = new EstimatingSystemEmailHandler();
		
		Messaging.InboundEmailResult result = null;
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEmail.TextAttachment csvFile = new Messaging.InboundEmail.TextAttachment();
		csvFile.body = CSV_WITH_EXTRA_COLUMN_FILE_CONTENTS;
		csvFile.mimeTypeSubType = 'text/csv';
		email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
		email.textAttachments.add(csvFile);
		Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

		Test.startTest();
			result = emailHandler.handleInboundEmail(email, envelope) ;
		Test.stopTest();
		System.debug('==========> testExtraColumns STOP');

		System.assertEquals(1, [select Id, Hours__c from Opportunity where Hours__c=200].size(), 
			'Did not create Opportunity objects as expected when row had extra columns.');
	}
	
	@IsTest
	static void testEmptyEmailRecipients() {
		System.assert(testEmptyEmailRecipients, 'Test Disabled');
		initData(true);
		CSV_Mapping__c mapping =[select Id from CSV_Mapping__c 
			where Use_Email_Recipients__c =true limit 1];
		mapping.Email_Recipients__c = 'somebody@somewhere.com';
		update mapping;
		EstimatingSystemEmailHandler emailHandler = new EstimatingSystemEmailHandler();
		
		Messaging.InboundEmailResult result = null;
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEmail.TextAttachment csvFile = new Messaging.InboundEmail.TextAttachment();
		csvFile.body = CSV_FILE_CONTENTS;
		csvFile.mimeTypeSubType = 'text/csv';
		email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
		email.textAttachments.add(csvFile);
		Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

		Test.startTest();
			result = emailHandler.handleInboundEmail(email, envelope);
		Test.stopTest();
	}

	@IsTest
	static void testDuplicateEstimateIds() {
		System.assert(testDuplicateEstimateIds, 'Test Disabled');
		initData(true);

		EstimatingSystemEmailHandler emailHandler = new EstimatingSystemEmailHandler();
		
		Messaging.InboundEmailResult result = null;
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEmail.TextAttachment csvFile = new Messaging.InboundEmail.TextAttachment();
		csvFile.body = CSV_WITH_DUP_ID_FILE_CONTENTS;
		csvFile.mimeTypeSubType = 'text/csv';
		email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
		email.textAttachments.add(csvFile);
		Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

		Test.startTest();
			result = emailHandler.handleInboundEmail(email, envelope);
		Test.stopTest();

		// verify that second row was ignored.
		System.assertEquals(0, [select Id, Hours__c from Opportunity where Hours__c=200].size(),
			 'Did not recoginze duplicate row and processed it.');
	}


	@IsTest
	static void testSameJobNum() {
		System.assert(testSameJobNum, 'Test Disabled');
		initData(true);

		EstimatingSystemEmailHandler emailHandler = new EstimatingSystemEmailHandler();
		
		Messaging.InboundEmailResult result = null;
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEmail.TextAttachment csvFile = new Messaging.InboundEmail.TextAttachment();
		csvFile.body = CSV_WITH_SAME_JOB_NUM_FILE_CONTENTS;
		csvFile.mimeTypeSubType = 'text/csv';
		email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
		email.textAttachments.add(csvFile);
		Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

		Test.startTest();
			result = emailHandler.handleInboundEmail(email, envelope);
		Test.stopTest();

		// verify that second row was processed as we can have multiple rows with same job number.
		System.assertEquals(2, [select Id from Job_Quote__c where EGS_Job__c='J300'].size(),
			 'Did not allow Job Quote rows for same Job and processed it.');
	}

	@IsTest
	static void testInvalidJobId() {
		System.assert(testInvalidJobId, 'Test Disabled');
		initData(false);

		EstimatingSystemEmailHandler emailHandler = new EstimatingSystemEmailHandler();
		
		Messaging.InboundEmailResult result = null;
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		Messaging.InboundEmail.TextAttachment csvFile = new Messaging.InboundEmail.TextAttachment();
		csvFile.body = CSV_WITH_INVALID_JOB_ID_CONTENTS;
		csvFile.mimeTypeSubType = 'text/csv';
		email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
		email.textAttachments.add(csvFile);
		Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

		Test.startTest();
			result = emailHandler.handleInboundEmail(email, envelope);
		Test.stopTest();

		// verify that row was no processed.
		System.assertEquals(0, [select id from Job_Quote__c where EGS_Job__c='J200'].size(),
			 'Did not recoginze invalid Job Idand processed it.');

	}

	private static void initData(Boolean blnCreateOpps) {
		List<CSV_Mapping__c> lstMappings = new List<CSV_Mapping__c>();
		lstMappings.add(new CSV_Mapping__c(Name='1', Target_Object_Name__c = 'Opportunity',
			Column_Name__c='Estimate ID', Field_Name__c = 'Estimate_ID__c', 
			Use_Email_Recipients__c=false, Email_Recipients__c=null));
		lstMappings.add(new CSV_Mapping__c(Name='2', Target_Object_Name__c = 'Opportunity',
			Column_Name__c='Hours', Field_Name__c = 'Hours__c'));
		lstMappings.add(new CSV_Mapping__c(Name='3', Target_Object_Name__c = 'Opportunity',
			Column_Name__c='Labor Rate Carried', Field_Name__c = 'Labor_Rate_Carried__c'));
		lstMappings.add(new CSV_Mapping__c(Name='4', Target_Object_Name__c = 'Opportunity',
			Column_Name__c='Project Cost Materials', Field_Name__c = 'Project_Cost_Materials__c'));
		lstMappings.add(new CSV_Mapping__c(Name='5', Target_Object_Name__c = 'Opportunity',
			Column_Name__c='Project Cost Equipment', Field_Name__c = 'Project_Cost_Equipment__c',
			Use_Email_Recipients__c=true, Email_Recipients__c=null));
		lstMappings.add(new CSV_Mapping__c(Name='6', Target_Object_Name__c = 'Job__c',
			Column_Name__c='Job Number', Field_Name__c = 'Job_Number__c'));
		lstMappings.add(new CSV_Mapping__c(Name='7', Target_Object_Name__c = 'Job_Quote__c',
			Column_Name__c='CO #', Field_Name__c = 'EGS_Job_Quote__c'));
		lstMappings.add(new CSV_Mapping__c(Name='8', Target_Object_Name__c = 'Job_Quote__c',
			Column_Name__c='Status', Field_Name__c = 'Quote_Status__c'));
		lstMappings.add(new CSV_Mapping__c(Name='9', Target_Object_Name__c = 'Opportunity',
			Column_Name__c='Status', Field_Name__c = 'StageName'));
		
		insert lstMappings;
		

		List<Quote_Job_Phase_Field_Mappings__c> lstJobQuoteMappings = new List<Quote_Job_Phase_Field_Mappings__c>();
		lstJobQuoteMappings.add(new Quote_Job_Phase_Field_Mappings__c(Name='Mapping 1',
			Job_Quote_Field_Name__c='Phase_1_Labor_Hours__c', Job_Field_Name__c='Phase_1_Change_Order_Labor_Hours__c'));
		lstJobQuoteMappings.add(new Quote_Job_Phase_Field_Mappings__c(Name='Mapping 2',
			Job_Quote_Field_Name__c='Phase_1_Equipment__c', Job_Field_Name__c='Phase_1_Change_Order_Equipment__c'));
		lstJobQuoteMappings.add(new Quote_Job_Phase_Field_Mappings__c(Name='Mapping 3',
			Job_Quote_Field_Name__c='Phase_1_Materials__c', Job_Field_Name__c='Phase_1_Change_Order_Materials__c'));
		lstJobQuoteMappings.add(new Quote_Job_Phase_Field_Mappings__c(Name='Mapping 4',
			Job_Quote_Field_Name__c='Phase_1_Labor_Dollars__c', Job_Field_Name__c='Phase_1_Change_Order_Labor_Dollars__c'));
		insert lstJobQuoteMappings;


		Account specialAcct = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');
		insert specialAcct;
		
		if (blnCreateOpps) {
			lstOpps = new List<Opportunity>();
			lstOpps.add(new Opportunity(Name='Opp 1', Estimate_Id__c='EI-100', 
				StageName='Open', CloseDate=Date.today()));
			lstOpps.add(new Opportunity(Name='Opp 2', Estimate_Id__c='EI-200',
				StageName='Open', CloseDate=Date.today()));
			lstOpps.add(new Opportunity(Name='Opp 3', Estimate_Id__c='EI-300',
				StageName='Open', CloseDate=Date.today()));

			insert lstOpps;
		}

		List<Job__c> lstJobs = new List<Job__c>();
		lstJobs.add(new Job__c(Job_Number__c='J200'));
		lstJobs.add(new Job__c(Job_Number__c='J300'));
		insert lstJobs;

		Job_Quote__c jobQuote = new Job_Quote__c(EGS_Job_Quote__c='C03', Job__c=lstJobs.get(1).Id, 
			Quote_Status__c='Open');
		insert jobQuote;
	}

	private static List<Opportunity> lstOpps = null;

	private static final String CSV_WITH_EXTRA_COLUMN_FILE_CONTENTS = 
		'Estimate ID,Job Number,CO #,Status,Hours,Labor Rate Carried,Project Cost Materials,Project Cost Equipment\n' +
		'EI-100,,,Rejected,100,10,100,999\n' +
		'EI-200,,,Awarded,200,20,200,400,999\n' +
		'EI-300,,,,300,30,300,600,999\n';

	private static final String CSV_FILE_CONTENTS = 
		'Estimate ID,Job Number,CO #,Status,Hours,Labor Rate Carried,Project Cost Materials,Project Cost Equipment\n' +
		'EI-100,,,Awarded,100,10,100,100\n' +
		'EI-200,J200,,Rejected,200,20,200,200\n' +
		'EI-300,J300,C03,Pending,300,30,300,300\n' +
		'EI-300,J300,C04,Rejected,400,40,400,400\n';

	private static final String CSV_WITH_DUP_ID_FILE_CONTENTS = 
		'Estimate ID,Job Number,CO #,Status,Hours,Labor Rate Carried,Project Cost Materials,Project Cost Equipment\n' +
		'EI-100,,,Awarded,100,10,100,100\n' +
		'EI-100,,,,200,10,100,100\n';

	private static final String CSV_WITH_SAME_JOB_NUM_FILE_CONTENTS = 
		'Estimate ID,Job Number,CO #,Status,Hours,Labor Rate Carried,Project Cost Materials,Project Cost Equipment\n' +
		',J300,C03,Awarded,100,10,100,100\n' +
		',J300,C04,Reject,200,10,100,100\n';

	private static final String CSV_WITH_INVALID_JOB_ID_CONTENTS = 
		'Estimate ID,Job Number,CO #,Status,Hours,Labor Rate Carried,Project Cost Materials,Project Cost Equipment\n' +
		',J999,C04,Rejected,400,40,400,400\n';
}