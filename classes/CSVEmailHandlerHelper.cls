/**
** Class:  CSVEmailHandlerHelper
** Created by OpFocus on 08/10/2015
** Description: Class that provides common functionality used by CSV File Email Handlers.
**/
public with sharing class CSVEmailHandlerHelper {
	public List<List<String>> lstRows = new List<List<String>>();
	public String[] columnHeadings = null;
	public Map<String, Map<String, String>> mapSettingsByTargetObject = new Map<String, Map<String, String>>();
	public Map<String, Schema.SobjectField> mapFieldsByName = new Map<String, Schema.SobjectField> ();
 	public Set<Integer> setRowsToIgnore = new Set<Integer>();

	private String reportName = null;
	public CSVImportFileStatusEmail statusEmail = null;
	private static final String CSV = 'text/csv';
	private static final String OCTET_STREAM = 'application/octet-stream';

	public CSVEmailHandlerHelper(String reportName, CSVImportFileStatusEmail statusEmail) {
		this.reportName  = reportName;
		this.statusEmail = statusEmail;
	}


	public void createFieldMappings(String[] targetObjectNames) {
		// map column headings to field names using custom setting. Order by Name which is column position in file
		for (CSV_Mapping__c mapping : [select Column_Name__c, Target_Object_Name__c,
				Field_Name__c, Use_Email_Recipients__c, Email_Recipients__c 
				from CSV_Mapping__c 
				where Target_Object_Name__c in :targetObjectNames 
				or Use_Email_Recipients__c = true order by Target_Object_Name__c]) {
			if (mapping.Use_Email_Recipients__c) {
				statusEmail.emailRecipients = mapping.Email_Recipients__c;
			}
			// first setting for this target object - create new Map based on column names
			if (mapSettingsByTargetObject.get(mapping.Target_Object_Name__c) == null) {
				mapSettingsByTargetObject.put(mapping.Target_Object_Name__c, new Map<String, String>());
			}
			System.debug('=========> Putting Mapping for Column ' + mapping.Column_Name__c + 
				' to Field ' + mapping.Field_Name__c);
			mapSettingsByTargetObject.get(mapping.Target_Object_Name__c).put(mapping.Column_Name__c, mapping.Field_Name__c);
		}
	}

	public TargetObject getTargetObject(List<String> row, Integer lookupIdColNum, Integer rowIndex, 
			Map<String, Id> mapIdsByLookupId) {
		// If there was an error with this row earlier, then ignore it now
		if (setRowsToIgnore.contains(rowIndex)) { 
			System.debug('==========> Ignoring row ' + rowIndex);
			return null;
		}

	    // create our object to populate
	    System.debug('=========> row.get(lookupIdColNum) = ' + row.get(lookupIdColNum) + ', lookupIdColNum = ' + lookupIdColNum);
	    String lookupId = row.get(lookupIdColNum).normalizeSpace();
	    Id objId  = mapIdsByLookupId.get(lookupId);
	    if (objId == null) {
	    	System.debug('==========> No object found for lookup Id ' + lookupId + 
	    		', so ignoring this row.');
	    	return new TargetObject(null, lookupId);
	    }
	    return new TargetObject(objId, lookupId);
	}

	public Messaging.InboundEmail.TextAttachment getAttachment(Messaging.InboundEmail email) {
		// read attachment. There may be more than 1 attachment as signatures & images count too
		// so we need to filter them to find the CSV file that we want
		System.debug('=======> There are ' + 
			(email.textAttachments == null || email.textAttachments.size() == 0 ?
			'none' : '' + email.textAttachments.size()) + ' Text Attachments = ' + email.textAttachments);
		System.debug('=======> There are ' + 
			(email.binaryAttachments == null || email.binaryAttachments.size()==0 ?
			'no' : '' + email.binaryAttachments.size()) + ' Binary Attachments = ' + email.binaryAttachments);
		if (email.textAttachments == null || email.textAttachments.size() == 0) {
			// there are no text attchments - try to get it as a binary attachment.
			if (email.binaryAttachments == null || email.binaryAttachments.size() == 0) {
				// there is no attachment - text or binary. return an error now
				String errorMsg = 'There should have been a Text attachment that is a CSV file. Instead there were '  +
					' no binary or text attachments found. Aborting import of ' + reportName + ' data.';
				System.debug('==========> ' + errorMsg);

				statusEmail.addError(errorMsg);
				statusEmail.send();
		
				return null;
			}
			System.debug('==========> Attachment received as binary rather than text-converting it.');
			for (Messaging.InboundEmail.BinaryAttachment attachment : email.binaryAttachments) {
				System.debug('==========> Binary Attachment ' + attachment.fileName + ' received has mimeSubType = ' + 
					attachment.mimeTypeSubType);
				if (attachment.mimeTypeSubType != null && 
					(attachment.mimeTypeSubType.equals(CSV) || attachment.mimeTypeSubType.equals(OCTET_STREAM))) {
					System.debug('==========> Found CSV file as a binary - converting it now.');
					// we found our csv file
					Messaging.InboundEmail.TextAttachment txtAttachment = new 
						Messaging.InboundEmail.TextAttachment();
					txtAttachment.body = attachment.body.toString();
					System.debug('==========> Attachment body = "' + txtAttachment.body + '"');

					return txtAttachment;					
				} else {
					String errorMsg = 'Binary Attachment does not have expected mimeType of ' + CSV + 
						' but rather is ' + attachment.mimeTypeSubType + '. Cannot process file ' + 
						reportName;
					System.debug('==========> ' + errorMsg);
					statusEmail.addError(errorMsg);
					statusEmail.send();
					return null;
				}
			}
		}
		// we have a text attachment : find the CSV file as there may be more than 1 attachment
		for (Messaging.InboundEmail.TextAttachment attachment : email.textAttachments) {
			System.debug('==========> Text Attachment ' + attachment.fileName + ' received has mimeSubType = ' + 
					attachment.mimeTypeSubType);
			if (attachment.mimeTypeSubType != null && attachment.mimeTypeSubType.equals(CSV)) {
				System.debug('==========> CSV file sent as a text attachment');
				return attachment;					
			}
		}
		// if we get here, then none of the attachments are of the correct Mime type - return an error
		String errorMsg = 'There should have been an attachment of type text/CSV but none were found. ' +
			'Please resend attaching a file of the correct type. Aborting import of ' + 
			reportName + ' data.';
		System.debug('==========> ' + errorMsg);

		statusEmail.addError(errorMsg);
		statusEmail.send();

		return null;
	}


	public void readCsvFile(Messaging.InboundEmail.TextAttachment attachment) {
		CSVReader csvReader = new CSVReader(attachment.body);

		// first get the column headings (should be first row in csv file)
		columnHeadings = csvReader.readLine();
		for (String heading : columnHeadings) {
			System.debug('==========> Heading = ' + heading);
		}

  
		// get the data we need to act on
		String[] row = csvReader.readLine();
		while (row != null) {
			System.debug('==========> Adding row ' + row + ' to lstRows');
		    lstRows.add(row);
		    row = csvReader.readLine();
		}

		// set number of rows for use in status email 
		statusEmail.numRows = lstRows.size();
	}

	public List<SObject> getTargetObjects(String queryString, Integer lookupColIdx, 
			List<List<String>> lstTargetRows, Boolean blnAllowDuplicateIds) {
 		// get the target objects for these rows
 		Set<String> setLookupIds = new Set<String>();
 		Integer index = 0;
 		String whereClause = '';
 		System.debug('==========> there are ' + lstTargetRows.size() + 'rows, LookupId column index = ' + lookupColIdx);
 		for (List<String> lstCells : lstTargetRows) { 		
			String lookupId = lstCells.get(lookupColIdx).normalizeSpace();
			System.debug('==========> Lookup Id = ' + lookupId);
			if (setLookupIds.contains(lookupId)) {
				if (blnAllowDuplicateIds) {
					// if we allow duplicate ids, go on to next row
					System.debug('==========> Found row with duplicat id ' + lookupId + ' but allowing it.');
					continue;
				}
				setRowsToIgnore.add(index);
				String errorMsg = 'Found row with duplicate Id ' + lookupId + 
					'. Ignoring second row.'; 
				System.debug('==========> ' + errorMsg);
				statusEmail.addError(errorMsg);
				index++;
				continue;					
			}
			setLookupIds.add(lookupId);
			whereClause += '\'' + lookupId + '\', ';
			index++;
 		}

		whereClause = whereClause.substringBeforeLast(',');
		String soqlQuery = queryString + ' ' + whereClause + ')';	
		System.debug('==========> queryString = ' + soqlQuery);
		return Database.query(soqlQuery);
	}

	public Boolean mapFields(SObject destObj, String objectName, String lookupColName, List<String> currentRow) {
		Integer colIndex = 0;
		System.debug('==========> Lookup Column Name = ' + lookupColName + ' for Object ' + objectName);
		Map<String, String> mapFieldsByCol = mapSettingsByTargetObject.get(objectName);
	    for (String columnName : columnHeadings) {

	    	if (columnName.equals(lookupColName)) {
				colIndex++;
	    		continue; // skipping lookup column as it has already been set
	    	}
	    	String fieldName = mapFieldsByCol.get(columnName);
	    	
	    	// If there is no mapping for this column, then just continue to next column.
	    	// This allows spreadsheet to contain columns we dont care about
	    	if (fieldName == null) {
	    		colIndex++;
	    		continue;
	    	}
	    	// because our values are stored in a CSV as Strings, we need to map them to their 
	    	// actual SF type before assigning the field on the destination object
	    	if (currentRow.get(colIndex) == null || currentRow.get(colIndex) == '') {
	    		colIndex++;
	    		continue;
	    	}
	    	if (mapFieldsByName.get(fieldName) == null) {
				System.debug('==========> Could not find ' + objectName + ' field ' + fieldName + 
					'. Ignoring this field.');
	    		colIndex++;
	    		continue;
	    	}
	    	try {
		    	Schema.DisplayType fieldType = mapFieldsByName.get(fieldName).getDescribe().getType();
		    	if (fieldType.equals(Schema.DisplayType.Currency)) {
                    String sanitizedValue = currentRow.get(colIndex).replace('$', '').replace(',','').replace(' ','');
                    destObj.put(fieldName, Decimal.valueOf(sanitizedValue));		    		
		   		} else if (fieldType.equals(Schema.DisplayType.Boolean)) {
			    	destObj.put(fieldName, Boolean.valueOf(currentRow.get(colIndex)));
		   		} else if (fieldType.equals(Schema.DisplayType.String)) {
			    	destObj.put(fieldName, currentRow.get(colIndex));
		   		} else if (fieldType.equals(Schema.DisplayType.Double)) {
                    String sanitizedValue = currentRow.get(colIndex).replace('$', '').replace(',','').replace(' ','');
			    	destObj.put(fieldName, Double.valueOf(sanitizedValue));
			    } else if (fieldType.equals(Schema.DisplayType.Date)) {
			    	destObj.put(fieldName, Date.parse(currentRow.get(colIndex)));
			    } else if (fieldType.equals(Schema.DisplayType.Picklist)) {
			    	destObj.put(fieldName, currentRow.get(colIndex));
				} else {
			    	System.debug('==========> Field Type is UNMAPPED : ' + fieldType);
			    }
			    System.debug('==========> Field ' + fieldName + ' = ' + destObj.get(fieldName));
	    	}
	    	catch (Exception e) {
	    		// if there is an error populating this field, log it in and move on to the next field
	    		System.debug('===========> ERROR Populating field ' + fieldName +' to value ' + 
	    			currentRow.get(colIndex) + ', Exception Details = ' + e);
	    	}
    		colIndex++;
		}
        return true;
	}

	public class TargetObject {
		public Id id {get; set;}
		public String lookupId {get; set;}
		public Boolean blnFoundObject {get; set;}

		public TargetObject(Id objid, String lookupId) {
			this.id = objId;
			this.lookupId = lookupId;
			blnFoundObject = (objId != null);
		}
	}
}