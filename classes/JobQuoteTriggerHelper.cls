/* Class: JobQuoteTriggerHelper
** Created On: 08/28/2015
** Created by: OpFocus Team
** Description: 
**     When a Job Quote is updated to have a Status of Awarded, we will update the associated Job
**     Change Order fields (Phase 1 Change Order Materials, Phase 1 Change Order Equipment, etc)
**	   by adding the Job Quote's related Phase fields
**  
*/
public with sharing class JobQuoteTriggerHelper {
	public static String AWARDED = 'Awarded';

	private static Map<String, Schema.SobjectField> mapFieldsByName = new Map<String, Schema.SobjectField> ();

	public static void updateJobChangeOrderFields(Map<Id, List<Job_Quote__c>> mapJobQuotesByJobId) {
		// get fields for Job__c 
		mapFieldsByName = Job__c.getSObjectType().getDescribe().fields.getMap();

		// get the mappings
		List<Quote_Job_Phase_Field_Mappings__c> mappings = 
			Quote_Job_Phase_Field_Mappings__c.getAll().values();

		String jobIds = '(';
		for (Id jobId : mapJobQuotesByJobId.keyset()) {
			jobIds += '\'' + jobId + '\',';
		}
		
		jobIds = jobIds.substring(0, jobIds.length()-1);
		jobIds += ')';

		// get the Jobs and their current Change Order fields for our list of JobQuotes
		String queryString = 'select Id, ' + buildQueryString(mappings) + 
			' from Job__c where Id in ' + jobIds;
		System.debug('==========> Query String = ' + queryString);
		List<Job__c> lstJobs = Database.query(queryString);

		Map<Id, Job__c> mapJobsById = new Map<Id, Job__c>();
		for (Job__c job : lstJobs) {
			mapJobsById.put(job.Id, job);
		}

		// loop through the Jobs updating them for each JobQuote found
		for (Id jobId : mapJobsById.keyset()) {
			Job__c job = mapJobsById.get(jobId); // get Job to update
			// loop through each field mapping & add them together for all the Quotes associated with a job
			for (Quote_Job_Phase_Field_Mappings__c mapping : mappings) {
				// handle mapping differently based on field type as we are doing summatios, not
				// just straight copying
				Schema.DisplayType fieldType = mapFieldsByName.get(mapping.Job_Field_Name__c).getDescribe().getType();
		    	if (fieldType.equals(Schema.DisplayType.Currency)) {
					handleCurrencyField(job, mapping.Job_Quote_Field_Name__c, 
						mapping.Job_Field_Name__c, mapJobQuotesByJobId);
				} else if (fieldType.equals(Schema.DisplayType.Double)) {
					handleDoubleField(job, mapping.Job_Quote_Field_Name__c, 
						mapping.Job_Field_Name__c, mapJobQuotesByJobId);
				}
			}	
		}
		if (!mapJobsById.values().isEmpty()) {
			update mapJobsById.values();
		}
	}

	// Handle mapping Currency fields from Job Quote to Job Change Order fields
	private static void handleCurrencyField(Job__c job, String srcFieldName, String targetFieldName, 
			Map<Id, List<Job_Quote__c>> mapJobQuotesByJobId) {
		Decimal targetValue = (Decimal)(job.get(targetFieldName)) == null ? 0 : 
			(Decimal)(job.get(targetFieldName));

		for (Job_Quote__c jobQuote : mapJobQuotesByJobId.get(job.Id)) {
			// if JobQuote does not have a value, just continue to next field
			if (jobQuote.get(srcFieldName) == null)
 			{
 				continue;
 			}
			targetValue += (Decimal)(jobQuote.get(srcFieldName));
		}
		job.put(targetFieldName, targetValue);
	}

	// Handle mapping Double fields from Job Quote to Job Change Order fields
	private static void handleDoubleField(Job__c job, String srcFieldName, String targetFieldName, 
			Map<Id, List<Job_Quote__c>> mapJobQuotesByJobId) {
		Double targetValue = (Double)(job.get(targetFieldName)) == null ? 0 : (Double)(job.get(targetFieldName));
		for (Job_Quote__c jobQuote : mapJobQuotesByJobId.get(job.Id)) {
			// if JobQuote does not have a value, just continue to next field
			if (jobQuote.get(srcFieldName) == null)
 			{
 				continue;
 			}
			targetValue += (Double)(jobQuote.get(srcFieldName));
		}
		job.put(targetFieldName, targetValue);
	}

	private static String buildQueryString(List<Quote_Job_Phase_Field_Mappings__c> lstMappings) {
		String queryString = '';
		for (Quote_Job_Phase_Field_Mappings__c mapping : lstMappings) {
			queryString += mapping.Job_Field_Name__c + ', ';
		}
		return queryString.substring(0, queryString.length()-2);
	}

}