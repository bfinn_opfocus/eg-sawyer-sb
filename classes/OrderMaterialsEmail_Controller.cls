global with sharing class OrderMaterialsEmail_Controller {

	global Id theOrderId {get; set;}
	
	global Order theOrder {
			get {
				theOrder = 
					[select 
						Type, TotalAmount, Status, Requested_Delivery_Date__c, Request_Date__c, Job_Name__r.Project_Manager__c, Job_Name__r.Project_Manager__r.Name, Pricebook2Id, OrderNumber, 
						Job_Number__c, Job_Name__c, Job_Name__r.Name, Job_Name__r.Job_Number__c, Job_Name__r.Division__c, AccountId, 
						Delivery_Requirements_Instruction__c, Shipping_Contact__r.Name,
						Job_Name__r.General_Foreman__r.Name, Order_Notes__c, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,
							(select 
								Id, OrderId, PricebookEntryId, PricebookEntry.Product2Id, PricebookEntry.Product2.Name, PricebookEntry.Product2.ProductCode,
								PricebookEntry.Product2.Family, PricebookEntry.Product2.Product_Type__c, PricebookEntry.Product2.Product_Sub_Type__c,
								AvailableQuantity, Quantity, UnitPrice, ListPrice, OrderItemNumber, Date_Needed__c
							 from OrderItems
							 order by PricebookEntry.Product2.Name) 
					 from Order 
					 where Id = :theOrderId];
				return theOrder;
			} 
			set;
		}
		
	
}