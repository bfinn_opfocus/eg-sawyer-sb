/*
** Class:  OrderMaterials_Controller
** Created by OpFocus on 7/24/2014
** Description: Controller for the OrderMaterials VF page
*/
public with sharing class OrderMaterials_Controller {

	public Order theOrder {get; set;}
	public Id theOrderId {get; set;}
	public String jobNameLink {get; set;}
	public String defaultDateNeeded {get; set;}
	public Boolean isForeman {get; set;}
	public String productData {get; set;}
	public String productDataFamily {get; set;}
	public String productDataType {get; set;}
	public String productDataSubtype {get; set;}
	public String materials {get; set;}
	private ApexPages.Standardcontroller std;
	
	public OrderMaterials_Controller(ApexPages.Standardcontroller std) {
		this.std = std;
		
		theOrder = 
			[select 
				Type, TotalAmount, Status, Requested_Delivery_Date__c, Request_Date__c, Job_Name__r.Project_Manager__c, Job_Name__r.Project_Manager__r.Name, Pricebook2Id, OrderNumber, 
				Job_Number__c, Job_Name__c, Job_Name__r.Name, Job_Name__r.Job_Number__c, Job_Name__r.Division__c, AccountId, 
				 Job_Name__r.General_Foreman__r.Name,
					(select 
						Id, OrderId, PricebookEntryId, PricebookEntry.Product2Id, PricebookEntry.Product2.Name, 
						PricebookEntry.Product2.Family, PricebookEntry.Product2.Product_Type__c, PricebookEntry.Product2.Product_Sub_Type__c,
						AvailableQuantity, Quantity, UnitPrice, ListPrice, OrderItemNumber, Date_Needed__c
					 from OrderItems
					 order by PricebookEntry.Product2.Name) 
			 from Order 
			 where Id = :std.getId()];

		jobNameLink = (Utils.isSF1() ? 'javascript:getLink(\''+theOrder.Job_Name__c+'\');' : '/' + theOrder.Job_Name__c);

		SlipAdjustmentSettings__c settings = SlipAdjustmentSettings__c.getInstance();

		Set <String> setProfileNames = new Set <String> ();
	    if (settings != null 
	    	&& settings.ProfilesPricingVisible__c != null) {
	    		for (String s : settings.ProfilesPricingVisible__c.split(',')) {
	    			setProfileNames.add(s);
	    		}
	    		
	    }
	    isForeman = true;//TODO



		// get product data where pricebook.name = job.division
		List <PricebookEntry> lstPBE = 
			[select 
				UnitPrice, Pricebook2Id, ProductCode, Product2Id, Product2.Product_Type__c, Product2.Product_Sub_Type__c, 
				Product2.Name, Product2.Family, Product2.Description, Product2.Product_Size__c
			 from PricebookEntry
			 where 
			 	Pricebook2.Name = :theOrder.Job_Name__r.Division__c
			 	or Pricebook2.Job__c = :theOrder.Job_Name__c
			 order by Pricebook2Id, Product2.Family, Product2.Product_Type__c, Product2.Product_Sub_Type__c, Product2.Product_Size__c];

		Set <String> setFamilies = new Set <String> ();
		Set <String> setTypes = new Set <String> ();
		Set <String> setSubtypes = new Set <String> ();
		Set <String> setNames = new Set <String> ();
		
		system.debug('lstPBE: '+lstPBE);
		system.debug('lstPBE.size(): '+lstPBE.size());	
		String pbId = '';
		if (lstPBE.size() > 0) {
			pbId = lstPBE[0].Pricebook2Id;
		}
		system.debug('pbId: '+pbId);
		
		productData = 
			'{"pricebookId":"' + pbId + '","products":[';
		for (PricebookEntry p : lstPBE) {
			productData
				+= '{'
				+   '"pbeid":"'     + p.Id + '",'
				+   '"pid":"'       + p.Product2Id + '",'
				+   '"family":"'    + p.Product2.Family + '",'
				+   '"type":"'      + p.Product2.Product_Type__c + '",'
				+   '"subtype":"'   + nameString(p.Product2.Product_Sub_Type__c) + '",'
				+   '"name":"'      + nameString(p.Product2.Name) + '",'
				+   '"listprice":"' + p.UnitPrice + '",'
				+   '"size":"'      + p.Product2.Product_Size__c + '"'
				+  '},';
//productData +='{"pbeid":"' + p.Id + '","pid":"' + p.Product2Id + '","family":"' + p.Product2.Family + '","type":"' + p.Product2.Product_Type__c + '","subtype":"' + p.Product2.Product_Sub_Type__c + '","size":"' + p.Product2.Product_Size__c + '"},';
			
			if (p.Product2.Family != null) setFamilies.add(p.Product2.Family);
			if (p.Product2.Product_Type__c != null) setTypes.add(p.Product2.Product_Type__c);
			if (p.Product2.Product_Sub_Type__c != null) setSubtypes.add(nameString(p.Product2.Product_Sub_Type__c));
			if (p.Product2.Name != null) setNames.add(nameString(p.Product2.Name));
		}
		productData = productData.removeEnd(',')
			+ '],';

		List <String> lstFamilies = new List <String> (setFamilies);  lstFamilies.sort();
		List <String> lstTypes = new List <String> (setTypes);  lstTypes.sort();
		List <String> lstSubtypes = new List <String> (setSubtypes);  lstSubtypes.sort();
		List <String> lstNames = new List <String> (setNames);  lstNames.sort();

		productData +=
			'"productfamilies":[';
		for (String s : lstFamilies) {
			productData += '"' + s + '",';
		}
		productData = productData.removeEnd(',') + '],';

		productData +=
			'"producttypes":[';
		for (String s : lstTypes) {
			productData += '"' + s + '",';
		}
		productData = productData.removeEnd(',') + '],';

		productData +=
			'"productsubtypes":[';
		for (String s : lstSubtypes) {
			productData += '"' + nameString(s) + '",';
		}
		productData = productData.removeEnd(',') + '],';

		productData +=
			'"productnames":[';
		for (String s : lstNames) {
			productData += '"' + nameString(s) + '",';
		}
		productData = productData.removeEnd(',') + ']}';

		system.debug('===>>> productData: '+productData);

		String matlist = '';
		for (OrderItem o : theOrder.OrderItems) {
			matlist
				+= '{'
				+   '"sfid":"' + o.Id + '",'
				+   '"family":"' + o.PricebookEntry.Product2.Family + '",'
				+   '"type":"' + o.PricebookEntry.Product2.Product_Type__c + '",'
				+   '"subtype":"' + o.PricebookEntry.Product2.Product_Sub_Type__c + '",'
				+   '"name":"'    + String.escapeSingleQuotes(o.PricebookEntry.Product2.Name).replace('"', '\\\\"') + '",'
				+   '"cnt":"' + Integer.valueOf(o.Quantity) + '",'
				+   '"pid":"' + o.PricebookEntry.Product2Id + '",'
				+   '"pbeid":"' + o.PricebookEntryId + '",'
				+   '"dateneeded":"' + (o.Date_Needed__c == null ? '' : DateTime.newInstance(o.Date_Needed__c, Time.newInstance(0, 0, 0, 0)).format('MM/dd/yyyy')) + '"'
				+  '},';
		}
		materials = 
			'{"materials":['
			+ matlist.removeEnd(',')
			+ ']}';

		// Date Needed defaults to the next business day, 2 business days if it is after 3pm ET
		Boolean isBefore3 = Datetime.now().hour() < 15;
		Date knownSunday = Date.newInstance(2015, 8, 2);
		Date defDate = Date.today();
		Integer daydiff = Math.mod(defDate.daysBetween(knownSunday), 7);
		Integer addDays = 0;
		if (isBefore3) {
			if (daydiff < 5)       addDays = 1; // before Fri
			else if (daydiff == 5) addDays = 3; // Fri
			else if (daydiff == 6) addDays = 3; // Sat
			else if (daydiff == 0) addDays = 2; // Sun
		}
		else {
			if (daydiff < 4)       addDays = 2; // before Thurs
			else if (daydiff == 4) addDays = 4; // Thurs
			else if (daydiff == 5) addDays = 4; // Fri
			else if (daydiff == 6) addDays = 3; // Sat
			else if (daydiff == 0) addDays = 2; // Sun
		}
		defaultDateNeeded = DateTime.newInstance(defDate.addDays(addDays), Time.newInstance(0, 0, 0, 0)).format('MM/dd/yyyy');

	}

	public static String nameString(String str) {
		if (str == null) {
			return '';
		}
		if (str.length() > 75) {
			str = str.substring(0, 75) + '...';
		}
		return str.replace('\'', 'in').replace('"', 'ft');
		//return String.escapeSingleQuotes(str).replace('"', '\\\\"');
	}
	



    @RemoteAction
    public static String createCustomProduct(String custJson) {
    	
		System.SavePoint sp = Database.setSavePoint();
		try {

			JSONParser parser = JSON.createParser(custJson);
			SaveData sd = (SaveData)parser.readValueAs(SaveData.class);
			system.debug('===>>> sd.oid: '+sd.oid+', sd: '+sd);
	
			Id oid = Id.valueOf(sd.oid);
			Order theOrder = [select Id, Job_Name__c, Job_Name__r.Name from Order where Id = :oid limit 1];
						
			// check if we already have a custom pricebook
			Id pbId;
			List <Pricebook2> lstCustPB = [select Id from Pricebook2 where Job__c = :theOrder.Job_Name__c limit 1];
			if (lstCustPB.size() == 1) {
				pbId = lstCustPB[0].Id;
			}
			else {
				Pricebook2 cpb = new Pricebook2(Name='Custom Pricebook - '+theOrder.Job_Name__r.Name,Job__c=theOrder.Job_Name__c);
				insert cpb;
				pbId = cpb.Id;
			}
			
			ProductData pd = sd.materials[0];
			Product2 prod = new Product2(Name=pd.name, Job__c=theOrder.Job_Name__c, IsActive=true);
			Database.Saveresult sr = Database.insert(prod);

			PricebookEntry pbe = new PricebookEntry(Pricebook2Id=pbId, Product2Id=sr.getId(), UnitPrice=1.0);//, UseStandardPrice=false, IsActive=true));
			Database.Saveresult srPbe = Database.insert(pbe);
			
			String resultsJson = 
				'{"status":"success",'
				+ '"pid":"' + sr.getId() + '",'
				+ '"pbeid":"' + srPbe.getId() + '",'
				+ '}';
				
			return resultsJson;
		}
		catch(Exception e) {
			Database.rollback(sp);
			return 'Error, unable to add custom product: '+e;
		}

    }






	/* save string is json, like this
		
		labor:
			role:
			
			employee:
		
		materials:
		
		other:
	*/

    @RemoteAction
    public static String saveAll(String saveJson) {
		System.SavePoint sp = Database.setSavePoint();
		try {
			return save(saveJson);
		}
		catch(Exception e) {
			Database.rollback(sp);
			return 'Error: '+e;
		}
/*
    	
        JSONParser parser = JSON.createParser(saveJson);
		SaveData sd = (SaveData)parser.readValueAs(SaveData.class);
		system.debug('===>>> sd.oid: '+sd.oid+', sd: '+sd);

		System.SavePoint sp = Database.setSavePoint();
		try {

			Id oid = Id.valueOf(sd.oid);
			Order theOrder = [select Id, Job_Name__c, Job_Name__r.Name from Order where Id = :oid limit 1];
			
			// save product changes

			// check if we need a custom pricebook
			Boolean custPB = false;
			Set <Id> setPBEIds = new Set <Id> ();
			for (ProductData pd : sd.materials) {
				if (!pd.deleted) {
					if (pd.std != null && pd.std.equals('custom')) {
						custPB = true;
					}
					else if (pd.pbeid != null && !pd.pbeid.equals('')) {
						setPBEIds.add(Id.valueOf(pd.pbeid));
					}
				}
			}
			Id pbId;
			if (custPB) {
				List <Pricebook2> lstCustPB = [select Id from Pricebook2 where Job__c = :theOrder.Job_Name__c limit 1];
				if (lstCustPB.size() == 1) {
					pbId = lstCustPB[0].Id;
				}
				else {
					Pricebook2 cpb = new Pricebook2(Name='Custom Pricebook - '+theOrder.Job_Name__r.Name,Job__c=theOrder.Job_Name__c);
					insert cpb;
					pbId = cpb.Id;
				}
			}
			
			// get the list price of any pricebookentries
			Map <Id, PricebookEntry> mapPBE = new Map <Id, PricebookEntry> ([select Id, UnitPrice from PricebookEntry where Id in :setPBEIds]);
			

			List <OrderItem> lstMatDeletes = new List <OrderItem> ();
			List <OrderItem> lstMatUpserts = new List <OrderItem> ();
			for (ProductData pd : sd.materials) {
				system.debug('===>>> pd: '+pd);
				OrderItem sm;
				if (pd.sfid != null && !pd.sfid.equals('')) {
					sm = new OrderItem(Id=Id.valueOf(pd.sfid));
					if (pd.deleted) {
						lstMatDeletes.add(sm);
					}
				}
				else {
					sm = new OrderItem(OrderId=theOrder.Id, PricebookEntryId=Id.valueOf(pd.pbeid));
				}

				if (!pd.deleted) {
					Decimal lp = 1.0;
					if (mapPBE.get(Id.valueOf(pd.pbeid)) != null && mapPBE.get(Id.valueOf(pd.pbeid)).UnitPrice != null) {
						lp = mapPBE.get(Id.valueOf(pd.pbeid)).UnitPrice;
					}
					sm.UnitPrice =  lp;
					sm.Quantity = Decimal.valueOf(pd.pcount);
					if (pd.dateneeded != null && !pd.dateneeded.equals('')) {
						sm.Date_Needed__c = Date.parse(pd.dateneeded);							
					}
					lstMatUpserts.add(sm);
				}	
			}
			if (lstMatDeletes.size() > 0) {
				delete lstMatDeletes;
			}
			if (lstMatUpserts.size() > 0) {
				upsert lstMatUpserts;
			}

	
	
			String resultsJson = '{"saveresults":[';
			resultsJson = resultsJson.removeEnd(',') +
			']' + ',"messages":"saved, materials: '+sd.materials.size() + '"}';
			
			return resultsJson;//'saved, hours: '+sd.hours.size()+', materials: '+sd.materials.size()+', other, saved: '+lstOther.size()+', deleted: '+lstDeleteOther.size();
		}
		catch(Exception e) {
			Database.rollback(sp);
			return 'Error: '+e;
		}
		*/
    }
    
    public static Boolean isTest = false;
    
    @RemoteAction
    public static String submitOrder(String saveJson) {
    	
		System.SavePoint sp = Database.setSavePoint();
		try {
			String ret = save(saveJson);
			
			JSONParser parser = JSON.createParser(saveJson);
			SaveData sd = (SaveData)parser.readValueAs(SaveData.class);
			system.debug('===>>> sd.oid: '+sd.oid+', sd: '+sd);	
			Id oid = Id.valueOf(sd.oid);
			
			Order theOrder = [select Job_Name__r.OwnerId from Order where Id = :oid limit 1];
			
			EmailTemplate temp = [select Id from EmailTemplate where DeveloperName='Order_Submitted' limit 1];//'00XJ0000000EGFV';
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setTargetObjectId(theOrder.Job_Name__r.OwnerId);//Id.valueOf('0031100000c4gAg'));
			mail.setWhatId(oid);
			mail.setSaveAsActivity(false);
			mail.setTemplateId(temp.Id);

			// sandboxes have email deliverability set to 'System email only' by default
			if (!isTest) {
				Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});					
			}
			
			update new Order(Id=oid, Status='Submitted');
			
			return 'Order Submitted';
		}
		catch(Exception e) {
			Database.rollback(sp);
			return 'Error: '+e;
		}
    }
    

    public static String save(String saveJson) {

		JSONParser parser = JSON.createParser(saveJson);
		SaveData sd = (SaveData)parser.readValueAs(SaveData.class);
		system.debug('===>>> sd.oid: '+sd.oid+', sd: '+sd);

		Id oid = Id.valueOf(sd.oid);
		Order theOrder = [select Id, Job_Name__c, Job_Name__r.Name, Job_Name__r.Division__c, Pricebook2Id from Order where Id = :oid limit 1];

		// set the order pricebook if not already set
		if (theOrder.Pricebook2Id == null) {
			Pricebook2 pb = [select Id from Pricebook2 where Pricebook2.Name = :theOrder.Job_Name__r.Division__c limit 1];// or Pricebook2.Job__c = :theOrder.Job_Name__c];
			update new Order(Id=theOrder.Id, Pricebook2Id=pb.Id);
		}


			
		// save product changes

		// check if we need a custom pricebook
		Boolean custPB = false;
		Set <Id> setPBEIds = new Set <Id> ();
		for (ProductData pd : sd.materials) {
			if (!pd.deleted) {
				if (pd.std != null && pd.std.equals('custom')) {
					custPB = true;
				}
				else if (pd.pbeid != null && !pd.pbeid.equals('')) {
					setPBEIds.add(Id.valueOf(pd.pbeid));
				}
			}
		}
		Id pbId;
		if (custPB) {
			List <Pricebook2> lstCustPB = [select Id from Pricebook2 where Job__c = :theOrder.Job_Name__c limit 1];
			if (lstCustPB.size() == 1) {
				pbId = lstCustPB[0].Id;
			}
			else {
				Pricebook2 cpb = new Pricebook2(Name='Custom Pricebook - '+theOrder.Job_Name__r.Name,Job__c=theOrder.Job_Name__c);
				insert cpb;
				pbId = cpb.Id;
			}
		}
			
		// get the list price of any pricebookentries
		Map <Id, PricebookEntry> mapPBE = new Map <Id, PricebookEntry> ([select Id, UnitPrice from PricebookEntry where Id in :setPBEIds]);
			

		List <OrderItem> lstMatDeletes = new List <OrderItem> ();
		List <OrderItem> lstMatUpserts = new List <OrderItem> ();
		for (ProductData pd : sd.materials) {
			system.debug('===>>> pd: '+pd);
			OrderItem sm;
			if (pd.sfid != null && !pd.sfid.equals('')) {
				sm = new OrderItem(Id=Id.valueOf(pd.sfid));
				if (pd.deleted) {
					lstMatDeletes.add(sm);
				}
			}
			else {
				sm = new OrderItem(OrderId=theOrder.Id, PricebookEntryId=Id.valueOf(pd.pbeid));
			}

			if (!pd.deleted) {
				Decimal lp = 1.0;
				if (mapPBE.get(Id.valueOf(pd.pbeid)) != null && mapPBE.get(Id.valueOf(pd.pbeid)).UnitPrice != null) {
					lp = mapPBE.get(Id.valueOf(pd.pbeid)).UnitPrice;
				}
				sm.UnitPrice =  lp;
				sm.Quantity = Decimal.valueOf(pd.pcount);
				if (pd.dateneeded != null && !pd.dateneeded.equals('')) {
					sm.Date_Needed__c = Date.parse(pd.dateneeded);							
				}
				lstMatUpserts.add(sm);
			}	
		}
		if (lstMatDeletes.size() > 0) {
			delete lstMatDeletes;
			}
		if (lstMatUpserts.size() > 0) {
			upsert lstMatUpserts;
		}

	
	
		String resultsJson = '{"saveresults":[';
		resultsJson = resultsJson.removeEnd(',') +
		']' + ',"messages":"saved, materials: '+sd.materials.size() + '"}';
			
		return resultsJson;//'saved, hours: '+sd.hours.size()+', materials: '+sd.materials.size()+', other, saved: '+lstOther.size()+', deleted: '+lstDeleteOther.size();
    }






	// parsed from json	
	public class SaveData {
		public String oid;

		public List <ProductData> materials;

	}


	public class ProductData {
		public Boolean deleted;
		public String sfid;
		public String pid;
		public String pbeid;
		public String family;
		public String ptype;
		public String subtype;
		public String name;
		public String pcount;
		public String std;
		public String dateneeded;
	}
	

}