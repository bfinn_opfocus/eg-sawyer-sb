/*
** Class:  WeeklyJobCostEmailHandler
** Created by OpFocus on 07/23/2015
** Description: Class contains email handler for WeeklyJobCosts CSV file
**   that is submitted to SF from external system. Map columns in CSV
**   file to a new WeeklyJobCost record associated with given Job.
**  Assumptions
**   1. This email handler relies on the Weekly_Job_Cost_CSV_Mapping__c custom setting
**  to get the mappings from Column -> Weekly_Job_Cost__c field. See Setup->Develop->Custom Settings
**  2. It expects the input CSV file to have columnn headings that match 
**    what is in the above Custom Setting.
**  3. It expects the first column in the CSV file to be the Job Number. Other than this, there is
**    no requirement on the ordering of the columns.
**   4. If a Job Number is not found in SF, it is ignored.
**   5. If a row has extra columns, they are ignored.
**  6. If an email is received with no attachment or more than 1, it is not processed.
**  7. If a row does not have enough columns, a Weekly_Job_Cost__c record is still created
**    only populating the fields from the columns that exist.
**  8. There is an EmailService associated with this EmailHandler that will invoke this Email Handler
**    when it receives an Email. See Setup->Develop->Email Services for details.
**   9. Following each import, an email is generated and sent to the recipients determined as follows.
**
**    The list of recipients of the Status message are defined in 
**     the CSV_Mapping__c custom setting. It defaults to activations@opfocus.com. The record with 
**    Use_Email_Recipients__c set to true should define the Email_Recipients__c.  
*/
global class WeeklyJobCostEmailHandler implements Messaging.InboundEmailHandler {
  global WeeklyJobCostEmailHandler() {    
  }

  // handle email received from Email Service.
   global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
        Messaging.InboundEnvelope envelope) {
     Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

    CSVImportFileStatusEmail statusEmail = new CSVImportFileStatusEmail(REPORT_NAME);
    CSVEmailHandlerHelper helper = new CSVEmailHandlerHelper(REPORT_NAME, statusEmail);
    
    Messaging.InboundEmail.TextAttachment attachment = helper.getAttachment(email);
    // if we are missing an attachment or have more than 1, return failure now.
    if (attachment == null) {
      result.success = false;
      return result;  
    }

    // read the CSV file and get the column headings
    helper.readCsvFile(attachment);
    
    // map column headings to field names using custom setting. Order by Name which is column position in file
    helper.createFieldMappings(new String[]{'Weekly_Job_Cost__c'});
    helper.mapFieldsByName = Weekly_Job_Cost__c.getSObjectType().getDescribe().fields.getMap();
    // find out where in the file our lookup field is
    Integer colIndex = 0;
    for (String columnHeading : helper.columnHeadings) {
      if (columnHeading.equalsIgnoreCase(JOB_NUMBER)) {
        jobNumColNum = colIndex;
        break;
      }
      colIndex++;
    }
    // build query to find Jobs for given Job Number.
      List<SObject> lstJobs = helper.getTargetObjects(
       'select Id, Job_Number__c from Job__c where Job_Number__c in (', jobNumColNum, helper.lstRows, false);
    
    Map<String, Id> mapJobIdsByNumber = new Map<String, Id>();
    for (SObject sobj : lstJobs) {
      Job__c job = (Job__c)sobj;
      System.debug('==========> Found Job Number = ' + job.Job_Number__c);
      mapJobIdsByNumber.put(job.Job_Number__c, job.Id);
    }

/*
    String jobNumbersToQuery = '';
    Set<String> setJobNumbers = new Set<String>();
    Set<Integer> setRowsToIgnore = new Set<Integer>();
    Integer index = 0;
    for (List<String> lstCells : helper.lstRows) {
      String jobNumber = lstCells.get(0).normalizeSpace();
      if (setJobNumbers.contains(jobNumber)) {
        setRowsToIgnore.add(index);
        String errorMsg = 'Found duplicate Job Number ' + jobNumber + 
          '. Ignoring second row.'; 
        System.debug('==========> ' + errorMsg);
        statusEmail.addError(errorMsg);
        index++;
        continue;
      }
      setJobNumbers.add(jobNumber);
      jobNumbersToQuery += '\'' + jobNumber + '\', ';
      index++;
    }
    jobNumbersToQuery = jobNumbersToQuery.substringBeforeLast(',');

    System.debug('==========> jobNumbersToQuery = ' + jobNumbersToQuery);
    String soqlQuery = 'select Id, Job_Number__c from Job__c where Job_Number__c in (' + jobNumbersToQuery + ')';
    Map<String, Id> mapJobIdsByNumber = new Map<String, Id>();
    for (SObject sobj : Database.query(soqlQuery)) {
      Job__c job = (Job__c)sobj;
      System.debug('==========> Found Job Number = ' + job.Job_Number__c);
      mapJobIdsByNumber.put(job.Job_Number__c, job.Id);
    }
*/

    List<Weekly_Job_Cost__c> lstWeeklyJobCosts = new List<Weekly_Job_Cost__c>();
    Integer rowIndex = 0;
    for (List<String> lstCells : helper.lstRows) {
      CSVEmailHandlerHelper.TargetObject target = helper.getTargetObject(lstCells, jobNumColNum, 
        rowIndex, mapJobIdsByNumber);
      if (target == null) {
        rowIndex++;
        continue;
      }
      if (!target.blnFoundObject) {
        helper.statusEmail.addError('Could not find Job with Job Number ' + target.lookupId);
        rowIndex++;
        continue;
      }
      rowIndex++;

        System.debug('==========> Creating Weekly_Job_Cost object for Job Number ' + target.lookupId);
      Weekly_Job_Cost__c weeklyJobCost = new Weekly_Job_Cost__c(Job_Name__c = target.id);
        if (!helper.mapFields(weeklyJobCost, 'Weekly_Job_Cost__c', JOB_NUMBER, lstCells)) {
          helper.statusEmail.addError('Failed to map fields for Job Number ' + target.lookupId);
        } else {
          helper.statusEmail.addSuccess('Job Number ' + target.lookupId);
          lstWeeklyJobCosts.add(weeklyJobCost);            
        }

    }
    if (!lstWeeklyJobCosts.isEmpty()) {
      insert lstWeeklyJobCosts;
    }

/*    helper.setRowsToIgnore = new Set<Integer>(); // empty its contents for next object


    Integer numColumns = helper.columnHeadings.size();
    Integer rowIndex = 0;
    for (List<String> lstCells : helper.lstRows) {
      // If there was an error with this row earlier, then ignore it now
      if (setRowsToIgnore.contains(rowIndex)) { 
        System.debug('==========> Ignoring row ' + rowIndex);
        rowIndex++;
        continue;
      } else {
        rowIndex++;
      }

//        Integer colIndex = 0;
        // create our object to populate, setting Job Name to Id for Job found above.
        String jobNumber = lstCells.get(0).normalizeSpace();
        Id jobId  = mapJobIdsByNumber.get(jobNumber);
        if (jobId == null) {
          System.debug('==========> No Job object found for Job Number ' + jobNumber + ', so ignoring this row.');
          statusEmail.addError('Failed to find Job for Job Number ' + jobNumber);
          continue;
        }
        System.debug('==========> Creating weeklyJobCost object for Job Number ' + jobNumber);
        Weekly_Job_Cost__c weeklyJobCost = new Weekly_Job_Cost__c(Job_Name__c = jobId);
        helper.mapFields(weeklyJobCost, REPORT_NAME, JOB_NUMBER, lstCells);
        /*
        for (String columnName : helper.columnHeadings) {
          if (columnName.equals(JOB_NUMBER)) {
            colIndex++;
            continue; // skipping Job Number column
          }
          String fieldName = helper.mapFieldsByCol.get(columnName);
          
          // If there is no mapping for this column, then just continue to next column.
          // This allows spreadsheet to contain columns we dont care about
          if (fieldName == null) {
            colIndex++;
            continue;
          }
          // because our values are stored in a CSV as Strings, we need to map them to their 
          // actual SF type before assigning the field on the destination object
          if (lstCells.get(colIndex) == null || lstCells.get(colIndex) == '') {
            colIndex++;
            continue;
          }
          if (mapFields.get(fieldName) == null) {
          System.debug('==========> Could not find Weekly_Job_Cost__c field ' + fieldName + 
            '. Ignoring this field.');
            colIndex++;
            continue;
          }
          Schema.DisplayType fieldType = mapFields.get(fieldName).getDescribe().getType();
          if (fieldType.equals(Schema.DisplayType.Currency)) {
            weeklyJobCost.put(fieldName, Decimal.valueOf(lstCells.get(colIndex)));
           } else if (fieldType.equals(Schema.DisplayType.Boolean)) {
            weeklyJobCost.put(fieldName, Boolean.valueOf(lstCells.get(colIndex)));
           } else if (fieldType.equals(Schema.DisplayType.String)) {
            weeklyJobCost.put(fieldName, lstCells.get(colIndex));
           } else if (fieldType.equals(Schema.DisplayType.Double)) {
            weeklyJobCost.put(fieldName, Double.valueOf(lstCells.get(colIndex)));
          } else if (fieldType.equals(Schema.DisplayType.Date)) {
            weeklyJobCost.put(fieldName, Date.parse(lstCells.get(colIndex)));
          } else {
            System.debug('==========> Field Type is UNMAPPED : ' + fieldType);
          }
           System.debug('==========> ' + fieldName + ' = ' + lstCells.get(colIndex));
            colIndex++;
        }
        statusEmail.addSuccess('Job Number', jobNumber + '');
        lstWeeklyJobCosts.add(weeklyJobCost);
    }

    if (!lstWeeklyJobCosts.isEmpty()) {
      insert lstWeeklyJobCosts;
    }
            */

    statusEmail.send();
     result.success = true;
    return result;     
   }

   private Integer jobNumColNum = 0;
   private static final String JOB_NUMBER = 'Job Number';
   private static final String REPORT_NAME = 'Weekly Job Cost';
}