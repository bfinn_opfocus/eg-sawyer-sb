/*
** Class:  Test_WeeklyJobCostTrigger
** Created by OpFocus on 03/27/2015
** Description: Class contains unit tests for Job Trigger, Job Staffing Trigger and Contact Trigger
*/
@isTest
private class Test_WeeklyJobCostTrigger{

  static testMethod void testNewJob() {
    Account a1 = UnitTestUtils.CreateAccount('Test Co1');
    Account a2 = UnitTestUtils.CreateAccount('Test Co2');
    Account specialAcct = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');
    insert new List<Account>{a1, a2, specialAcct};

    Contact emp1 = UnitTestUtils.createContact('testlastname1', a1.Id, '7098908909', empRtId);
    Contact emp2 = UnitTestUtils.createContact('testlastname2', a1.Id, '7778905555', empRtId);
    Contact emp3 = UnitTestUtils.createContact('testlastname3', a1.Id, '5558888909', empRtId);
    Contact emp4 = UnitTestUtils.createContact('testlastname4', a1.Id, '4442221234', empRtId);
    emp4.Project_Manager__c = true;
    // Employee Contact needs to have Primary, Secondary or Tertiary Role ALL set to one of these values:
    //  1. Office - Executive Office
    //  2. Office - Project Manager
    emp4.Primary_Role__c = 'Office - Project Manager';
    emp4.Secondary_Role__c = 'Office - Project Manager';
    emp4.Tertiary_Role__c = 'Office - Project Manager';
    emp4.Employee_Status__c = 'Active'; // status needs to be Active for Project_Manager_c flag to be set

    Contact emp5 = UnitTestUtils.createContact('testlastname5', a1.Id, '4442220000', empRtId);
    emp5.Primary_Role__c = 'General Foreman';
    emp5.Secondary_Role__c = 'General Foreman';
    emp5.Tertiary_Role__c = 'General Foreman';
    emp5.Employee_Status__c = 'Active';
    emp5.General_Foreman__c = true;
    
    // create LCN Network Employees
    Contact lcnEmployee1 =  UnitTestUtils.createContact('lcnNetworkEmployee1', a1.Id, '888888888888', empRtId);
    lcnEmployee1.Employee_Type__c='LCN Networks';
    lcnEmployee1.Employee_Status__c='Active';

    Contact lcnEmployee2 =  UnitTestUtils.createContact('lcnNetworkEmployee2', a1.Id, '888888888888', empRtId);
    lcnEmployee2.Employee_Type__c='LCN Networks';
    lcnEmployee2.Employee_Status__c='Active';
    
    insert new List<Contact>{emp1, emp2, emp3, emp4, emp5, lcnEmployee1, lcnEmployee2};


    Opportunity opp1 = UnitTestUtils.createOpp('opp1', a1.Id);
    Opportunity opp2 = UnitTestUtils.createOpp('opp2', a1.Id);
    // set Type to LCN Network so Job's Division is set to this value & trigger fires correctly
    // opp2 is associated with job2 later
    opp2.Type = 'LCN Networks';

    Opportunity opp3 = UnitTestUtils.createOpp('opp3', a1.Id);
    Opportunity opp4 = UnitTestUtils.createOpp('opp4', a1.Id);
    insert new List<Opportunity>{opp1, opp2, opp3, opp4};

    System.debug('==========> Creating job1 that is eligible for Job Board BUT not in LCN Networks.');
    Job__c job1 = UnitTestUtils.createJob('Job1', a1.Id, opp1.Id, 'a1 job1');
    job1.Job_Short_Name__c = 'job1';
    job1.Project_Manager__c = emp4.Id;
    job1.General_Contractor__c = a2.Id;
    job1.General_Foreman__c    = emp5.Id;
    job1.Eligible_to_Merge_on_Job_Board__c = true;
    job1.Job_Status__c = 'In Progress - Active';
    insert job1;

    System.debug('==========> job1.Id = ' + job1.Id);
    Job_Staffing__c js1 = UnitTestUtils.createJS(job1.Id, 'Lead Foreman', emp1.Id, true);
    Job_Staffing__c js2 = UnitTestUtils.createJS(job1.Id, 'Foreman',      emp2.Id, true);
    Job_Staffing__c js3 = UnitTestUtils.createJS(job1.Id, 'Technician',   emp3.Id, true);
    insert new List<Job_Staffing__c>{js1, js2, js3};


    List <Weekly_Job_Cost__c> lstWJC1s = new List <Weekly_Job_Cost__c> {
      new Weekly_Job_Cost__c(
        Sub_Contractor_Costs__c=100.0, 
        Phase_5_Materials__c=100.0, Phase_5_Labor__c=100.0, Phase_5_Labor_Dollars__c=100.0, Phase_5_Equipment__c=100.0, 
        Phase_4_Materials__c=100.0, Phase_4_Labor__c=100.0, Phase_4_Labor_Dollars__c=100.0, Phase_4_Equipment__c=100.0, 
        Phase_3_Materials__c=100.0, Phase_3_Labor__c=100.0, Phase_3_Labor_Dollars__c=100.0, Phase_3_Equipment__c=100.0, 
        Phase_2_Materials__c=100.0, Phase_2_Labor__c=100.0, Phase_2_Labor_Dollars__c=100.0, Phase_2_Equipment__c=100.0, 
        Phase_1_Materials__c=100.0, Phase_1_Labor__c=100.0, Phase_1_Labor_Dollars__c=100.0, Phase_1_Equipment__c=100.0, 
        Job_Name__c=job1.Id, DJE_Costs__c=100.0
      )
    };
    insert lstWJC1s;
    
    List <Weekly_Job_Cost__c> lstWJC2s = new List <Weekly_Job_Cost__c> {
      new Weekly_Job_Cost__c(
        Sub_Contractor_Costs__c=101.0, 
        Phase_5_Materials__c=101.0, Phase_5_Labor__c=101.0, Phase_5_Labor_Dollars__c=101.0, Phase_5_Equipment__c=101.0, 
        Phase_4_Materials__c=101.0, Phase_4_Labor__c=101.0, Phase_4_Labor_Dollars__c=101.0, Phase_4_Equipment__c=101.0, 
        Phase_3_Materials__c=101.0, Phase_3_Labor__c=101.0, Phase_3_Labor_Dollars__c=101.0, Phase_3_Equipment__c=101.0, 
        Phase_2_Materials__c=101.0, Phase_2_Labor__c=101.0, Phase_2_Labor_Dollars__c=101.0, Phase_2_Equipment__c=101.0, 
        Phase_1_Materials__c=101.0, Phase_1_Labor__c=101.0, Phase_1_Labor_Dollars__c=101.0, Phase_1_Equipment__c=101.0, 
        Job_Name__c=job1.Id, DJE_Costs__c=101.0
      )
    };
    insert lstWJC2s;
  }


  private static Id empRtId;
  
  static {
     empRtId = [Select Id, sObjectType, Name From RecordType where SObjectType = 'Contact' and Name='Employee Contact' limit 1].Id;  
  } 

}