/*
** Class:  Test_Job
** Created by OpFocus on 07/25/2014
** Description: Class contains unit tests for Job Trigger, Job Staffing Trigger and Contact Trigger
*/
@isTest
private class Test_Job{

	static testMethod void testNewJob() {
		initData();
/*		Account a1 = UnitTestUtils.CreateAccount('Test Co1');
		Account a2 = UnitTestUtils.CreateAccount('Test Co2');
		Account a3 = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');
		insert new List<Account>{a1, a2, a3};

		Contact emp1 = UnitTestUtils.createContact('testlastname1', a1.Id, '7098908909', empRtId);
		Contact emp2 = UnitTestUtils.createContact('testlastname2', a1.Id, '7778905555', empRtId);
		Contact emp3 = UnitTestUtils.createContact('testlastname3', a1.Id, '5558888909', empRtId);
		Contact emp4 = UnitTestUtils.createContact('testlastname4', a1.Id, '4442221234', empRtId);
		emp4.Project_Manager__c = true;
		// Employee Contact needs to have Primary, Secondary or Tertiary Role ALL set to one of these values:
		//	1. Office - Executive Office
		//  2. Office - Project Manager
		emp4.Primary_Role__c = 'Office - Project Manager';
		emp4.Secondary_Role__c = 'Office - Project Manager';
		emp4.Tertiary_Role__c = 'Office - Project Manager';
		emp4.Employee_Status__c = 'Active'; // status needs to be Active for Project_Manager_c flag to be set

		Contact emp5 = UnitTestUtils.createContact('testlastname5', a1.Id, '4442220000', empRtId);
		emp5.Primary_Role__c = 'General Foreman';
		emp5.Secondary_Role__c = 'General Foreman';
		emp5.Tertiary_Role__c = 'General Foreman';
		emp5.Employee_Status__c = 'Active';
		emp5.General_Foreman__c = true;
		
		// create LCN Network Employees
		Contact lcnEmployee1 =  UnitTestUtils.createContact('lcnNetworkEmployee1', a1.Id, '888888888888', empRtId);
		lcnEmployee1.Employee_Type__c='LCN Networks';
		lcnEmployee1.Employee_Status__c='Active';

		insert new List<Contact>{emp1, emp2, emp3, emp4, emp5, lcnEmployee1};


		Opportunity opp1 = UnitTestUtils.createOpp('opp1', a1.Id);
		Opportunity opp2 = UnitTestUtils.createOpp('opp2', a1.Id);
		// set Type to LCN Network so Job's Division is set to this value & trigger fires correctly
		// opp2 is associated with job2 later
		opp2.Type = 'LCN Networks';

		Opportunity opp3 = UnitTestUtils.createOpp('opp3', a1.Id);
		Opportunity opp4 = UnitTestUtils.createOpp('opp4', a1.Id);
		insert new List<Opportunity>{opp1, opp2, opp3, opp4};
*/
		Test.startTest();
		System.debug('==========> Creating job1 that is eligible for Job Board BUT not in LCN Networks.');
		Job__c job1 = UnitTestUtils.createJob('Job1', mapAccountsByCount.get(1).Id, 
			mapOppsByCount.get(1).Id, 'a1 job1');
		job1.Job_Short_Name__c = 'job1';
		job1.Project_Manager__c = mapContactsByCount.get(4).Id;
		job1.General_Contractor__c = mapAccountsByCount.get(2).Id;
		job1.General_Foreman__c    = mapContactsByCount.get(5).Id;
		job1.Eligible_to_Merge_on_Job_Board__c = true;
		job1.Job_Status__c = 'In Progress - Active';
		insert job1;

		System.debug('==========> job1.Id = ' + job1.Id);
		Job_Staffing__c js1 = UnitTestUtils.createJS(job1.Id, 'Lead Foreman', mapContactsByCount.get(1).Id, true);
		Job_Staffing__c js2 = UnitTestUtils.createJS(job1.Id, 'Foreman',      mapContactsByCount.get(2).Id, true);
		Job_Staffing__c js3 = UnitTestUtils.createJS(job1.Id, 'Technician',   mapContactsByCount.get(3).Id, true);
		insert new List<Job_Staffing__c>{js1, js2, js3};

		Job__c job2 = UnitTestUtils.createJob('Job2', mapAccountsByCount.get(1).Id, 
			mapOppsByCount.get(2).Id, 'a1 job2');
		job2.Job_Short_Name__c = 'job2';
		job2.Project_Manager__c    = mapContactsByCount.get(4).Id;
		job2.General_Contractor__c = mapAccountsByCount.get(2).Id;
		job2.General_Foreman__c    = mapContactsByCount.get(5).Id;
		job2.Eligible_to_Merge_on_Job_Board__c = true;
		job2.Job_Status__c = 'In Progress - Active';

		// now create a job that is eligible for job board but is not in LCN Networks.
		Job__c job3 = UnitTestUtils.createJob('Job3', mapAccountsByCount.get(1).Id, mapOppsByCount.get(3).Id, 'a1 job3');
		job3.Job_Short_Name__c = 'job3';
		job3.Project_Manager__c    = mapContactsByCount.get(4).Id;
		job3.General_Contractor__c = mapAccountsByCount.get(2).Id;
		job3.General_Foreman__c    = mapContactsByCount.get(5).Id;
		job3.Eligible_to_Merge_on_Job_Board__c = true;
		job3.Job_Status__c = 'In Progress - Active';
		
		System.debug('==========> Inserting Job2 (LCN Network) and Job3 (eligible for job board)');

		Test.stopTest();

		insert new List<Job__c>{job2, job3};
		System.debug('==========> job2.Id = ' + job2.Id);
		System.debug('==========> job3.Id = ' + job3.Id);

		// Verify that we created a JobStaffing Record for our LCN Networks job.
		List<Contact> lstEmployees = [SELECT Id, Primary_Role__c FROM Contact where Employee_Type__c='LCN Networks'];
		
		// Since this is being created for a job that is eligible for the job board, the isActive flag should be true.
		List<Job_Staffing__c> lstJobStaffings = [select Id, Job_Name__c from Job_Staffing__c where Job_Name__c=:job2.Id and IsActive__c=true];
		System.assertEquals(lstJobStaffings.get(0).Job_Name__c, job2.Id);

		// there should be 1 JobStaffing object for each LCN Networks Employee plus 3 for the JobStaffings
		// created above on lines 56-58 for the Job Board not linked to LCN Networks 
		System.assertEquals(lstJobStaffings.size(), lstEmployees.size() + 3);

		// Now verify that we created Contact record for our new Job record.
		String sanitizedJobNumber= job2.Job_Number__c.replaceAll('[ ]', '_');
		Contact dbContact = [SELECT AccountId, LastName  FROM Contact where Email=:sanitizedJobNumber + '@egsawyer.com'];
		System.assertEquals(dbContact.LastName, job2.Name);
		System.assertEquals(dbContact.AccountId, mapAccountsByCount.get(3).Id);
	}
/*
	static testMethod void testUpdateJob() {
		initData();
		// START NEW TEST //
		// now update job Number and re-test for update situation.
		job2.Job_Number__c = 'job2Num';
		update job2;
		dbContact = [SELECT Email  FROM Contact where Job__c=:job2.Id];
		System.assert(dbContact.Email.equalsIgnoreCase('job2Num@egsawyer.com'));

		// now test for an update that has no change to job_number - no new contact should be created
		job2.Job_Status__c = 'In Progress - Active';
		update job2;

		List<Contact> lstContacts = [SELECT Email  FROM Contact where Job__c=:job2.Id];
		System.assertEquals(lstContacts.size(), 1);
		
		// test situation where only LCN Networks creates JobStaffing Records. opp2 has Type = LCN Networks.
		Job__c job5 = UnitTestUtils.createJob('Job1', a1.Id, opp2.Id, 'a1 job5');
		job5.Job_Short_Name__c = 'job5';
		job5.Project_Manager__c = emp4.Id;
		job5.General_Contractor__c = a2.Id;
		job5.General_Foreman__c    = emp5.Id;
		job5.Job_Status__c = 'In Progress - Active';
		insert job5;

		// there should only be the JobStaffing records added for LCN Networks.
		lstJobStaffings = [select Id from Job_Staffing__c where Job_Name__c = :job5.Id];
		System.assertEquals(lstJobStaffings.size(), lstEmployees.size());

		// test situation where only Job Board Job Staffing records are created
		Job__c job6 = UnitTestUtils.createJob('Job6', a1.Id, opp1.Id, 'a1 job6');
		job6.Job_Short_Name__c = 'job6';
		job6.Project_Manager__c = emp4.Id;
		job6.General_Contractor__c = a2.Id;
		job6.General_Foreman__c    = emp5.Id;
		job6.Eligible_to_Merge_on_Job_Board__c = true;
		job6.Job_Status__c = 'In Progress - Active';
		insert job6;
		
		Test.startTest();

		// there should only be 3 JobStaffing records added
		lstJobStaffings = [select Id from Job_Staffing__c where Job_Name__c = :job6.Id];
		// number of JobStaffing objects created above 
		// NOTE: it will be 3 created above (lines 56-58) plus the ones created for employee in LCN Networks 
		// They get added in because they are for the same account and general foreman.
		System.assertEquals(lstJobStaffings.size(), lstEmployees.size() + 3); 

		// now test the update trigger. First, change Eligible flag to false.
		job2.Eligible_to_Merge_on_Job_Board__c = false;
		update job2;

		Job_Staffing__c js4 = UnitTestUtils.createJS(job1.Id, 'Project Manager', emp4.Id, true);
		Job_Staffing__c js5 = UnitTestUtils.createJS(job1.Id, 'Foreman', emp5.Id, true);
		insert new List<Job_Staffing__c>{js4, js5};

		// now change it to true, which will cause logic in trigger to fire and create new JobStaffing objects
		job2.Eligible_to_Merge_on_Job_Board__c = true;
		update job2;

		lstJobStaffings = [select Id from Job_Staffing__c where Job_Name__c = :job2.Id];
		// we can expect a new JobStaffing record for each JobStaffing object (5) 
		// plus the ones created above (lstEmployees.size()
		System.assertEquals(lstEmployees.size() + 5, lstJobStaffings.size());		

		Contact emp6 = UnitTestUtils.createContact('testlastname6', a1.Id, 'T442220000', empRtId);
		emp6.Primary_Role__c = 'Foreman';
		emp6.Employee_Status__c = 'Active';
		emp6.General_Foreman__c = true;
		insert emp6;

		insert new List <Weekly_Job_Cost__c> {
			new Weekly_Job_Cost__c(Job_Name__c = job1.Id, Phase_1_Materials__c=1.1, Phase_1_Labor__c=1.1, Phase_1_Labor_Dollars__c=1.1, Phase_1_Equipment__c=1.1),
			new Weekly_Job_Cost__c(Job_Name__c = job1.Id, Phase_1_Materials__c=1.1, Phase_1_Labor__c=1.1, Phase_1_Labor_Dollars__c=1.1, Phase_1_Equipment__c=1.1)
		};

		List <Weekly_Job_Cost__c> lstWJC1s = new List <Weekly_Job_Cost__c> {
			new Weekly_Job_Cost__c(
				Sub_Contractor_Costs__c=100.0, 
				Phase_5_Materials__c=100.0, Phase_5_Labor__c=100.0, Phase_5_Labor_Dollars__c=100.0, Phase_5_Equipment__c=100.0, 
				Phase_4_Materials__c=100.0, Phase_4_Labor__c=100.0, Phase_4_Labor_Dollars__c=100.0, Phase_4_Equipment__c=100.0, 
				Phase_3_Materials__c=100.0, Phase_3_Labor__c=100.0, Phase_3_Labor_Dollars__c=100.0, Phase_3_Equipment__c=100.0, 
				Phase_2_Materials__c=100.0, Phase_2_Labor__c=100.0, Phase_2_Labor_Dollars__c=100.0, Phase_2_Equipment__c=100.0, 
				Phase_1_Materials__c=100.0, Phase_1_Labor__c=100.0, Phase_1_Labor_Dollars__c=100.0, Phase_1_Equipment__c=100.0, 
				Job_Name__c=job1.Id, DJE_Costs__c=100.0
			)
		};
		insert lstWJC1s;
		
		List <Weekly_Job_Cost__c> lstWJC2s = new List <Weekly_Job_Cost__c> {
			new Weekly_Job_Cost__c(
				Sub_Contractor_Costs__c=101.0, 
				Phase_5_Materials__c=101.0, Phase_5_Labor__c=101.0, Phase_5_Labor_Dollars__c=101.0, Phase_5_Equipment__c=101.0, 
				Phase_4_Materials__c=101.0, Phase_4_Labor__c=101.0, Phase_4_Labor_Dollars__c=101.0, Phase_4_Equipment__c=101.0, 
				Phase_3_Materials__c=101.0, Phase_3_Labor__c=101.0, Phase_3_Labor_Dollars__c=101.0, Phase_3_Equipment__c=101.0, 
				Phase_2_Materials__c=101.0, Phase_2_Labor__c=101.0, Phase_2_Labor_Dollars__c=101.0, Phase_2_Equipment__c=101.0, 
				Phase_1_Materials__c=101.0, Phase_1_Labor__c=101.0, Phase_1_Labor_Dollars__c=101.0, Phase_1_Equipment__c=101.0, 
				Job_Name__c=job1.Id, DJE_Costs__c=101.0
			)
		};
		insert lstWJC2s;

	}

	*/
	static testMethod void testNewEmployeeContact() {
		Account a1 = UnitTestUtils.CreateAccount('Test Co1');
		Account a2 = UnitTestUtils.CreateAccount('Test Co2');
		Account a3 = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');
		insert new List<Account>{a1, a2, a3};

		Contact emp1 = UnitTestUtils.createContact('testlastname1', a1.Id, '7098908909', empRtId);
		Contact emp2 = UnitTestUtils.createContact('testlastname2', a1.Id, '7778905555', empRtId);
		Contact emp3 = UnitTestUtils.createContact('testlastname3', a1.Id, '5558888909', empRtId);
		Contact emp4 = UnitTestUtils.createContact('testlastname4', a1.Id, '4442221234', empRtId);
		emp4.Project_Manager__c = true;
		// Employee Contact needs to have Primary, Secondary or Tertiary Role ALL set to one of these values:
		//	1. Office - Executive Office
		//  2. Office - Project Manager
		emp4.Primary_Role__c = 'Office - Project Manager';
		emp4.Secondary_Role__c = 'Office - Project Manager';
		emp4.Tertiary_Role__c = 'Office - Project Manager';
		emp4.Employee_Status__c = 'Active'; // status needs to be Active for Project_Manager_c flag to be set

		Contact emp5 = UnitTestUtils.createContact('testlastname5', a1.Id, '4442220000', empRtId);
		emp5.Primary_Role__c = 'General Foreman';
		emp5.Secondary_Role__c = 'General Foreman';
		emp5.Tertiary_Role__c = 'General Foreman';
		emp5.Employee_Status__c = 'Active';
		emp5.General_Foreman__c = true;
		insert new List<Contact>{emp1, emp2, emp3, emp4, emp5};

		Contact emp4Contact = [select Id, RecordTypeId, Project_Manager__c, Primary_Role__c, Secondary_Role__c, Employee_Status__c from Contact where LastName='testlastname4'];
		System.debug('==========> emp4Contact Project_Manager__C = ' + emp4Contact.Project_Manager__C);
		System.debug('==========> emp4Contact Secondary_Role__c = ' + emp4Contact.Secondary_Role__c);
		System.debug('==========> emp4Contact Primary_Role__c = ' + emp4Contact.Primary_Role__c);
		System.debug('==========> emp4Contact Employee_Status__c = ' + emp4Contact.Employee_Status__c);
		System.debug('==========> emp4Contact RecordTypeId = ' + emp4Contact.RecordTypeId);


		Opportunity opp1 = UnitTestUtils.createOpp('opp1', a1.Id);
		insert opp1;

		Job__c job1 = UnitTestUtils.createJob('Unassigned', a1.Id, opp1.Id, 'a1 job1');
		job1.Job_Short_Name__c  = 'job1';
		job1.Project_Manager__c = emp4.Id;
		job1.General_Foreman__c = emp5.Id;
		job1.Job_Status__c = 'In Progress - Active';
		insert job1;

		Job_Staffing__c js1 = UnitTestUtils.createJS(job1.Id, 'Lead Foreman', emp1.Id, true);
		Job_Staffing__c js2 = UnitTestUtils.createJS(job1.Id, 'Foreman',      emp2.Id, true);
		Job_Staffing__c js3 = UnitTestUtils.createJS(job1.Id, 'Technician',   emp3.Id, true);
		insert new List<Job_Staffing__c>{js1, js2, js3};

		Contact emp6 = UnitTestUtils.createContact('testlastname6', a1.Id, 'T442220000', empRtId);
		emp6.Primary_Role__c = 'General Foreman';
		emp6.Employee_Status__c = 'Active';
		emp6.General_Foreman__c = true;
		insert emp6;
	}

	private static void initData() {
		mapAccountsByCount.put(1, UnitTestUtils.CreateAccount('Test Co1'));
		mapAccountsByCount.put(1, UnitTestUtils.CreateAccount('Test Co2'));
		mapAccountsByCount.put(3, UnitTestUtils.CreateAccount('EG Sawyer Job Emails'));
		insert mapAccountsByCount.values();

		mapContactsByCount(1, UnitTestUtils.createContact('testlastname1', mapAccountsByCount.get(1).Id, '7098908909', empRtId);
		mapContactsByCount(2, UnitTestUtils.createContact('testlastname2', mapAccountsByCount.get(1).Id, '7778905555', empRtId);
		mapContactsByCount(3, UnitTestUtils.createContact('testlastname3', mapAccountsByCount.get(1).Id, '5558888909', empRtId);
		
		Contact emp4 = UnitTestUtils.createContact('testlastname4', mapAccountsByCount.get(1).Id, '4442221234', empRtId);
		emp4.Project_Manager__c = true;
		// Employee Contact needs to have Primary, Secondary or Tertiary Role ALL set to one of these values:
		//	1. Office - Executive Office
		//  2. Office - Project Manager
		emp4.Primary_Role__c = 'Office - Project Manager';
		emp4.Secondary_Role__c = 'Office - Project Manager';
		emp4.Tertiary_Role__c = 'Office - Project Manager';
		emp4.Employee_Status__c = 'Active'; // status needs to be Active for Project_Manager_c flag to be set
		mapContactsByCount.put(4, emp4);

		Contact emp5 = UnitTestUtils.createContact('testlastname5', mapAccountsByCount.get(1).Id.Id, '4442220000', empRtId);
		emp5.Primary_Role__c = 'General Foreman';
		emp5.Secondary_Role__c = 'General Foreman';
		emp5.Tertiary_Role__c = 'General Foreman';
		emp5.Employee_Status__c = 'Active';
		emp5.General_Foreman__c = true;
		mapContactsByCount.put(5, emp5);
		
		// create LCN Network Employees
		Contact lcnEmployee1 =  UnitTestUtils.createContact('lcnNetworkEmployee1', mapAccountsByCount.get(1).Id.Id, '888888888888', empRtId);
		lcnEmployee1.Employee_Type__c='LCN Networks';
		lcnEmployee1.Employee_Status__c='Active';
		mapContactsByCount.put(6, lcnEmployee1);

		insert mapContactsByCount.values()

		mapOppsByCount.put(1, UnitTestUtils.createOpp('opp1', mapAccountsByCount.Id);
		mapOppsByCount.put(2, UnitTestUtils.createOpp('opp2', mapAccountsByCount.Id);
		// set Type to LCN Network so Job's Division is set to this value & trigger fires correctly
		// opp2 is associated with job2 later
		opp2.Type = 'LCN Networks';

		mapOppsByCount.put(3, UnitTestUtils.createOpp('opp3', mapAccountsByCount.Id));
		mapOppsByCount.put(4, UnitTestUtils.createOpp('opp4', mapAccountsByCount.Id);
		insert mapOppsByCount.values();
	}

	private static Map<Integer, Account> mapAccountsByCount = new Map<Integer, Account>();
	private static Map<Integer, Contact> mapContactsByCount = new Map<Integer, Contact>();
	private static Map<Integer, Opportunity> mapOppsByCount = new Map<Integer, Opportunity>();
	private static Id empRtId;
	
	static {
 		empRtId = [Select Id, sObjectType, Name From RecordType where SObjectType = 'Contact' and Name='Employee Contact' limit 1].Id;	
	} 

}