/*
** Class:  Test_WeeklyJobCostEmailHandler
** Created by OpFocus on 07/23/2015
** Description: Class contains unit tests for WeeklyJobCost Email Handler that maps
**   values from an attached CSV file into a new WeeklyJobCost record keyed off Job Id.
*/
@IsTest
private  class Test_WeeklyJobCostEmailHandler {
  private static final Boolean testNoAttachment = true;
  private static final Boolean testSuccess = true;
  private static final Boolean testExtraColumns = true;
  private static final Boolean testDuplicateJobNumbers = true;
  private static final Boolean testEmptyEmailRecipients = true;

  @IsTest
  static void testNoAttachment() {
    System.assert(testNoAttachment, 'Test Disabled');
    initData(false);
    WeeklyJobCostEmailHandler emailHandler = new WeeklyJobCostEmailHandler();
    
    Messaging.InboundEmailResult result = null;
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

    Test.startTest();
      result = emailHandler.handleInboundEmail(email, envelope) ;
    Test.stopTest();
    System.assertEquals(false, result.success, 'Did not return false when there was no attachment.');
  }

  @IsTest
  static void testSuccess() {
    System.assert(testSuccess, 'Test Disabled');
    initData(true);

    WeeklyJobCostEmailHandler emailHandler = new WeeklyJobCostEmailHandler();
    
    Messaging.InboundEmailResult result = null;
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.InboundEmail.TextAttachment csvFile = new Messaging.InboundEmail.TextAttachment();
    csvFile.mimeTypeSubType='text/csv';
    csvFile.body = CSV_FILE_CONTENTS;
    email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
    email.textAttachments.add(csvFile);
    Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

    Test.startTest();
      result = emailHandler.handleInboundEmail(email, envelope) ;
    Test.stopTest();
    System.assertEquals(true, result.success, 'Did not return true when there was a valid attachment. result details = ' + result.message);
    Job__c jobWithJobCost = [select Id, (select Id from CF_Data_Material_Summaries__r) 
      from Job__c where Id =:lstJobs.get(0).Id limit 1];
    System.assertEquals(jobWithJobCost.CF_Data_Material_Summaries__r.size(), 1, 
      'Did not create WeeklyJobCost as expected.');

    List<Weekly_Job_Cost__c> lstWeeklyJobCosts = [select Id, Phase_1_Equipment__c from Weekly_Job_Cost__c where
      Phase_1_Equipment__c != null];
    System.assertEquals(lstWeeklyJobCosts.size(), 0, 'Phase_1_Equipment__c set when it should have been null.');
  }

  @IsTest
  static void testExtraColumns() {
    System.assert(testExtraColumns, 'Test disabled.');
    initData(true);
    WeeklyJobCostEmailHandler emailHandler = new WeeklyJobCostEmailHandler();
    
    Messaging.InboundEmailResult result = null;
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.InboundEmail.TextAttachment csvFile = new Messaging.InboundEmail.TextAttachment();
    csvFile.body = CSV_WITH_EXTRA_COLUMN_FILE_CONTENTS;
    csvFile.mimeTypeSubType='text/csv';

    email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
    email.textAttachments.add(csvFile);
    Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

    Test.startTest();
      result = emailHandler.handleInboundEmail(email, envelope) ;
    Test.stopTest();

    Job__c jobWithJobCost = [select Id, (select Id from CF_Data_Material_Summaries__r) 
      from Job__c where Id =:lstJobs.get(0).Id limit 1];
    System.assertEquals(jobWithJobCost.CF_Data_Material_Summaries__r.size(), 1, 
      'Did not create WeeklyJobCost as expected when rows had extra columns. result.message = ' + result.message);
  }
  
  @IsTest
  static void testEmptyEmailRecipients() {
    System.assert(testEmptyEmailRecipients, 'Test Disabled');
    initData(true);
    CSV_Mapping__c mapping =[select Id from CSV_Mapping__c 
      where Use_Email_Recipients__c =true limit 1];
    mapping.Email_Recipients__c = 'somebody@somewhere.com';
    update mapping;
    WeeklyJobCostEmailHandler emailHandler = new WeeklyJobCostEmailHandler();
    
    Messaging.InboundEmailResult result = null;
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.InboundEmail.TextAttachment csvFile = new Messaging.InboundEmail.TextAttachment();
    csvFile.body = CSV_FILE_CONTENTS;
    csvFile.mimeTypeSubType='text/csv';
    email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
    email.textAttachments.add(csvFile);
    Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

    Test.startTest();
      result = emailHandler.handleInboundEmail(email, envelope);
    Test.stopTest();
  }

  @IsTest
  static void testDuplicateJobNumbers() {
    System.assert(testDuplicateJobNumbers, 'Test Disabled');
    initData(true);

    WeeklyJobCostEmailHandler emailHandler = new WeeklyJobCostEmailHandler();
    
    Messaging.InboundEmailResult result = null;
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.InboundEmail.TextAttachment csvFile = new Messaging.InboundEmail.TextAttachment();
    csvFile.body = CSV_WITH_DUP_JOB_FILE_CONTENTS;
    csvFile.mimeTypeSubType='text/csv';

    email.textAttachments = new List<Messaging.InboundEmail.TextAttachment>();
    email.textAttachments.add(csvFile);
    Messaging.Inboundenvelope envelope = new Messaging.Inboundenvelope();

    Test.startTest();
      result = emailHandler.handleInboundEmail(email, envelope);
    Test.stopTest();

    List<Weekly_Job_Cost__c> lstWeeklyJobCosts = [select Id from Weekly_Job_Cost__c];
    System.assertEquals(2, lstWeeklyJobCosts.size(), 'Created a duplicate WeeklyJobCost. result message = ' + result.message);
  }

  private static void initData(Boolean blnCreateJobs) {
    Account specialAcct = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');
    insert specialAcct;

    List<CSV_Mapping__c> lstMappings = new List<CSV_Mapping__c>();
    lstMappings.add(new CSV_Mapping__c(Name='1', Target_Object_Name__c = 'Weekly_Job_Cost__c',
      Column_Name__c='Job Number', Field_Name__c = 'Job_Number__c', 
      Use_Email_Recipients__c=false, Email_Recipients__c=null));
    lstMappings.add(new CSV_Mapping__c(Name='2', Target_Object_Name__c = 'Weekly_Job_Cost__c',
      Column_Name__c='Phase 1 Materials', Field_Name__c = 'Phase_1_Materials__c'));
    lstMappings.add(new CSV_Mapping__c(Name='3', Target_Object_Name__c = 'Weekly_Job_Cost__c',
      Column_Name__c='Phase 1 Labor Hours', Field_Name__c = 'Phase_1_Labor__c'));
    lstMappings.add(new CSV_Mapping__c(Name='4', Target_Object_Name__c = 'Weekly_Job_Cost__c',
      Column_Name__c='Phase 1 Labor Dollars', Field_Name__c = 'Phase_1_Labor_Dollars__c'));
    lstMappings.add(new CSV_Mapping__c(Name='5', Target_Object_Name__c = 'Weekly_Job_Cost__c',
      Column_Name__c='Phase 1 Equipment', Field_Name__c = 'Phase_1_Equipment_Invalid__c'));
    lstMappings.add(new CSV_Mapping__c(Name='6', Target_Object_Name__c = 'Weekly_Job_Cost__c',
      Column_Name__c='Run Date', Field_Name__c = 'Run_Date__c'));
    lstMappings.add(new CSV_Mapping__c(Name='7', Target_Object_Name__c = 'Opportunity',
      Column_Name__c='Id', Field_Name__c = 'Id', 
      Use_Email_Recipients__c=true, Email_Recipients__c=null));
    insert lstMappings;
    
    if (blnCreateJobs) {
      lstJobs = new List<Job__c>();
      lstJobs.add(new Job__c(Name='Job 1', Job_Number__c='JN-100'));
      lstJobs.add(new Job__c(Name='Job 2', Job_Number__c='JN-200'));
      lstJobs.add(new Job__c(Name='Job 3', Job_Number__c='JN-300'));

      insert lstJobs;
    }
  }

  private static List<Job__c> lstJobs = null;

  private static final String CSV_WITH_EXTRA_COLUMN_FILE_CONTENTS = 
    'Job Number,Phase 1 Materials,Phase 1 Labor Hours,Phase 1 Labor Dollars,Phase 1 Equipment\n' +
    'JN-100,100,10,100,200,999\n' +
    'JN-200,200,20,200,400,999\n' +
    'JN-300,300,30,300,600,999\n';

  private static final String CSV_FILE_CONTENTS = 
    'Job Number,Run Date,Phase 1 Materials,Phase 1 Labor Hours,Phase 1 Labor Dollars,Phase 1 Equipment,Unused Column\n' +
    'JN-100,6/6/2015,100,10,100,200,\n' +
    'JN-200,6/6/2015,200,20,200,400,\n' +
    'JN-300,6/6/2015,300,30,300,600,\n' +
    'JN-400,6/6/2015,300,30,300,600,\n' ;

  private static final String CSV_WITH_DUP_JOB_FILE_CONTENTS = 
    'Job Number,Phase 1 Materials,Phase 1 Labor Hours,Phase 1 Labor Dollars,Phase 1 Equipment\n' +
    'JN-100,100,10,100,,999\n' +
    'JN-100,200,20,200,999\n' +
    'JN-300,300,30,300,600,999\n';

}