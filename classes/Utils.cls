/*
** class:  Utils
** Created:  July, 2014 by OpFocus
** Description: This class contains various helper functions:
**
**              ++ a helper function that returns a new Job Staffings to create
**
**              ++ a helper function to check if we are running in SF1
*/
public with sharing class Utils {


	// Determine if this is SF1 app
	public static Boolean isSF1() {
		if(!checkNullOrBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
			!checkNullOrBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
			ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
			(ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
		)
		{
			return true;
		}
		else {
			return false;
		}
	}

	public static Boolean checkNullOrBlank(String str) {
		return (str == null || str.equals(''));
	}

	
	public static Job_Staffing__c createNewStaffing(Job_Staffing__c js) {
		Job_Staffing__c newJs = new Job_Staffing__c();
		newJs.Employee__c = js.Employee__c;
		newJs.Role__c     = js.Role__c;
		newJs.IsActive__c = js.IsActive__c;
		return newJs;
	}
}