/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
** Class:  Test_Timesheet
** Created by OpFocus on 07/25/2014
** Description: Class contains unit tests for Timesheet editor
*/
@isTest
private class Test_Timesheet {

    static testMethod void testTimesheetEG() {
        Account a1 = UnitTestUtils.CreateAccount('Test Co1');
        Account a2 = UnitTestUtils.CreateAccount('Test Co2');
        Account specialAcct = UnitTestUtils.CreateAccount('EG Sawyer Job Emails');

        insert new List<Account>{a1, a2, specialAcct};

        Contact emp1 = UnitTestUtils.createContact('testlastname1', a1.Id, '7098908909', empRtId);
        Contact emp2 = UnitTestUtils.createContact('testlastname2', a1.Id, '7778905555', empRtId);
        Contact emp3 = UnitTestUtils.createContact('testlastname3', a1.Id, '5558888909', empRtId);
        Contact emp4 = UnitTestUtils.createContact('testlastname4', a1.Id, '4442221234', empRtId);
        emp4.Project_Manager__c = true;
        //emp4.Employee_Status__c = 'Active';
        Contact emp5 = UnitTestUtils.createContact('testlastname5', a1.Id, '4442220000', empRtId);
        emp5.Primary_Role__c = 'General Foreman';
        emp5.General_Foreman__c = true;
        emp5.Employee_Status__c = 'Active';
        insert new List<Contact>{emp1, emp2, emp3, emp4, emp5};

        Opportunity opp1 = UnitTestUtils.createOpp('opp1', a1.Id);
        Opportunity opp2 = UnitTestUtils.createOpp('opp2', a1.Id);
        Opportunity opp3 = UnitTestUtils.createOpp('opp3', a1.Id);
        Opportunity opp4 = UnitTestUtils.createOpp('opp4', a1.Id);
        opp1.Type = 'E.G. Sawyer';
        opp2.Type = 'LCN Networks';
        insert new List<Opportunity>{opp1, opp2, opp3, opp4};

        Job__c job1 = UnitTestUtils.createJob('Job1', a1.Id, opp1.Id, 'a1 job1');
        job1.Job_Short_Name__c = 'job1';
        //job1.Project_Manager__c = emp4.Id;
        job1.General_Contractor__c = a2.Id;
        job1.General_Foreman__c    = emp5.Id;
        job1.Eligible_to_Merge_on_Job_Board__c = true;
        job1.Job_Status__c = 'In Progress - Active';
        insert job1;

        Job_Staffing__c js1 = UnitTestUtils.createJS(job1.Id, 'Lead Foreman', emp1.Id, true);
        Job_Staffing__c js2 = UnitTestUtils.createJS(job1.Id, 'Foreman',      emp2.Id, true);
        Job_Staffing__c js3 = UnitTestUtils.createJS(job1.Id, 'Technician',   emp3.Id, true);
        insert new List<Job_Staffing__c>{js1, js2, js3};

        Job__c job2 = UnitTestUtils.createJob('Job2', a1.Id, opp2.Id, 'a1 job2');
        job2.Job_Short_Name__c = 'job2';
        //job2.Project_Manager__c = emp4.Id;
        job2.General_Contractor__c = a2.Id;
        job2.General_Foreman__c    = emp5.Id;
        job2.Eligible_to_Merge_on_Job_Board__c = true;
        job2.Job_Status__c = 'In Progress - Active';
        insert job2;

        List<Job_Staffing__c> lstJob2Staffs = [select Id from Job_Staffing__c where Job_Name__c = :job2.Id];

        //System.assertEquals(3, lstJob2Staffs.size());

        job2.Eligible_to_Merge_on_Job_Board__c = false;
        update job2;

        Job_Staffing__c js4 = UnitTestUtils.createJS(job1.Id, 'Project Manager', emp4.Id, true);
        Job_Staffing__c js5 = UnitTestUtils.createJS(job1.Id, 'Foreman', emp5.Id, true);
        insert new List<Job_Staffing__c>{js4, js5};


        job2.Eligible_to_Merge_on_Job_Board__c = true;
        update job2;

        lstJob2Staffs = [select Id from Job_Staffing__c where Job_Name__c = :job2.Id];
        //System.assertEquals(5, lstJob2Staffs.size());   
            
        Slip__c slip1 = new Slip__c(Slip_Type__c='Phase 5', Job_Name__c=job1.Id, Slip_Status__c='Open');
        insert slip1;

        Test.startTest();
        
        PageReference pg = Page.Timesheet;
        Test.setCurrentPage(pg);
        
        Timesheet_Controller ctl = new Timesheet_Controller(new ApexPages.StandardController(job1));
        ctl.init();
        ctl.selectedWeek = ctl.lstWeeks[0].weekStr;
        ctl.weekSelected();
        
        ctl.selectedDay = ctl.lstDays[0];
        ctl.daySelected();

    String stf = ctl.staffers;
        ctl.newStafferId = emp4.Id;
        ctl.newStafferRole = 'Technician';
        ctl.saveNewStaffer();
        
        
        ctl.lstDayHelpers[0].lstRStr[0] = '1.5';
        ctl.lstDayHelpers[0].slipR = slip1.Id + '::' + '2.5' + ',';
        ctl.lstDayHelpers[0].lstOTStr[0] = '1.5';
        ctl.lstDayHelpers[0].lstDTStr[0] = '1.5';
        ctl.saveAndContinue();
        ctl.saveAndClose();
        
      Timesheet_Controller.createNewSlip('new slip', ' description', 'Phase 5', job1.Id);

        ctl.selectedWeek = ctl.lstWeeks[0].weekStr;
        ctl.weekSelected();
        
        ctl.selectedDay = ctl.lstDays[0];
        ctl.daySelected();
        ctl.saveAndContinue();

    List <Timesheet__c> lstTS = [select Id from Timesheet__c limit 1];
    String ret = Timesheet_Controller.getOtherTimesheetInfo(emp1.Id, lstTS[0].Id, '7/12/2015', 'regular');
        
        List <String> dys = Timesheet_Controller.getDays(Date.today());
        
        Timesheet_Controller.getPrimaryRole(emp1.Id);
        Timesheet_Controller.getPrimaryRole(emp5.Id);
        Timesheet_Controller.getPrimaryRole('asdfasdfasdf');
        
        Test.stopTest();    
    }
/*
    static testMethod void testTimesheetLCN() {
        Account a1 = UnitTestUtils.CreateAccount('Test Co1');
        Account a2 = UnitTestUtils.CreateAccount('Test Co2');
        insert new List<Account>{a1, a2};

        Contact emp1 = UnitTestUtils.createContact('testlastname1', a1.Id, '7098908909', empRtId);
        Contact emp2 = UnitTestUtils.createContact('testlastname2', a1.Id, '7778905555', empRtId);
        Contact emp3 = UnitTestUtils.createContact('testlastname3', a1.Id, '5558888909', empRtId);
        Contact emp4 = UnitTestUtils.createContact('testlastname4', a1.Id, '4442221234', empRtId);
        emp4.Project_Manager__c = true;
        //emp4.Employee_Status__c = 'Active';
        Contact emp5 = UnitTestUtils.createContact('testlastname5', a1.Id, '4442220000', empRtId);
        emp5.Primary_Role__c = 'General Foreman';
        emp5.General_Foreman__c = true;
        emp5.Employee_Status__c = 'Active';
        insert new List<Contact>{emp1, emp2, emp3, emp4, emp5};

        Opportunity opp1 = UnitTestUtils.createOpp('opp1', a1.Id);
        Opportunity opp2 = UnitTestUtils.createOpp('opp2', a1.Id);
        Opportunity opp3 = UnitTestUtils.createOpp('opp3', a1.Id);
        Opportunity opp4 = UnitTestUtils.createOpp('opp4', a1.Id);
        opp1.Type = 'E.G. Sawyer';
        opp2.Type = 'LCN Networks';
        insert new List<Opportunity>{opp1, opp2, opp3, opp4};

        Job__c job1 = UnitTestUtils.createJob('Job1', a1.Id, opp1.Id, 'a1 job1');
        job1.Job_Short_Name__c = 'job1';
        //job1.Project_Manager__c = emp4.Id;
        job1.General_Contractor__c = a2.Id;
        job1.General_Foreman__c    = emp5.Id;
        job1.Eligible_to_Merge_on_Job_Board__c = true;
        job1.Job_Status__c = 'In Progress - Active';
        insert job1;

        Job_Staffing__c js1 = UnitTestUtils.createJS(job1.Id, 'Lead Foreman', emp1.Id, true);
        Job_Staffing__c js2 = UnitTestUtils.createJS(job1.Id, 'Foreman',      emp2.Id, true);
        Job_Staffing__c js3 = UnitTestUtils.createJS(job1.Id, 'Technician',   emp3.Id, true);
        insert new List<Job_Staffing__c>{js1, js2, js3};

        Job__c job2 = UnitTestUtils.createJob('Job2', a1.Id, opp2.Id, 'a1 job2');
        job2.Job_Short_Name__c = 'job2';
        //job2.Project_Manager__c = emp4.Id;
        job2.General_Contractor__c = a2.Id;
        job2.General_Foreman__c    = emp5.Id;
        job2.Eligible_to_Merge_on_Job_Board__c = true;
        job2.Job_Status__c = 'In Progress - Active';
        insert job2;

        List<Job_Staffing__c> lstJob2Staffs = [select Id from Job_Staffing__c where Job_Name__c = :job2.Id];

        //System.assertEquals(3, lstJob2Staffs.size());

        job2.Eligible_to_Merge_on_Job_Board__c = false;
        update job2;

        Job_Staffing__c js4 = UnitTestUtils.createJS(job1.Id, 'Project Manager', emp4.Id, true);
        Job_Staffing__c js5 = UnitTestUtils.createJS(job1.Id, 'Foreman', emp5.Id, true);
        insert new List<Job_Staffing__c>{js4, js5};


        job2.Eligible_to_Merge_on_Job_Board__c = true;
        update job2;

        lstJob2Staffs = [select Id from Job_Staffing__c where Job_Name__c = :job2.Id];
        //System.assertEquals(5, lstJob2Staffs.size());   
            
        Slip__c slip1 = new Slip__c(Slip_Type__c='Phase 5', Job_Name__c=job1.Id, Slip_Status__c='Open');
        
        Timesheet__c ts1 = new Timesheet__c(Job_Name__c=job1.Id, Start_Date__c=Date.today());
        insert ts1;

        Test.startTest();
   
        PageReference pg = Page.Timesheet;
        Test.setCurrentPage(pg);
        
        Timesheet_Controller ctl = new Timesheet_Controller(new ApexPages.StandardController(job2));
        ctl.init();
        ctl.selectedWeek = ctl.lstWeeks[0].weekStr;
        ctl.weekSelected();
        
        ctl.selectedDay = ctl.lstDays[0];
        ctl.daySelected();
        
        ctl.lstDayHelpers[0].lstRStr[0] = '1.5';
        ctl.lstDayHelpers[0].slipR = slip1.Id + '::' + '2.5';
        ctl.lstDayHelpers[0].lstOTStr[0] = '1.5';
        ctl.lstDayHelpers[0].lstDTStr[0] = '1.5';
        //ctl.saveHours();
        ctl.saveAndContinue();
        
      Timesheet_Controller.createNewSlip('new slip', ' description', 'Phase 5', job1.Id);
      Timesheet_Controller.getOtherTimesheetInfo(emp1.Id, ts1.Id, '2014-01-01', 'regular');
        
        ctl.saveAndContinue();

        ctl.saveAndClose();
        ctl.selectedWeek = ctl.lstWeeks[0].weekStr;
        ctl.weekSelected();
        
        ctl.selectedDay = ctl.lstDays[0];
        ctl.daySelected();
        ctl.saveAndClose();
        
        List <String> dys = Timesheet_Controller.getDays(Date.today());
        Test.stopTest();    
    }
*/
    private static Id empRtId;
    
    static {
        empRtId = [Select Id, sObjectType, Name From RecordType where SObjectType = 'Contact' and Name='Employee Contact' limit 1].Id;  
    } 

}