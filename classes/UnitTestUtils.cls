/*
** Class:  UnitTestUtils
** Created:  June, 2014 by OpFocus
** Description: Utilities used by unit tests.
**
*/	
public class UnitTestUtils {
	// Create Methods
    public static Account CreateAccount(String name) {
    	System.assert(Test.isRunningTest());
    	Account a = new Account();
    	a.name    = name;
    	a.BillingStreet     = '123';
    	a.BillingState      = 'MA';
    	a.BillingCity       = 'Big City';
    	a.BillingCountry    = 'US';
    	a.BillingPostalCode = '08900';
    	return a;
    }
	public static Contact createContact(String name, Id aId, String phone, Id rtId) {
		System.assert(Test.isRunningTest());
		Contact c = new Contact();
		c.LastName        = name;
		c.FirstName       = 'Test';
		c.AccountId       = aId;
		c.Phone           = phone;
		c.RecordTypeId    = rtId;
		return c; 
	}

	public static Opportunity createOpp(String name, Id aId) {
		System.assert(Test.isRunningTest());
		Opportunity opp = new Opportunity();
		opp.AccountId   = aId;
		opp.Name        = name;
		opp.StageName   = 'Test';
		opp.CloseDate   = date.today().adddays(30);
		return opp;
	}
		
	public static Job__c createJob(String name, Id aId, Id oppId, String jobNumber) {
		System.assert(Test.isRunningTest());
		Job__c job = new Job__c();
		job.Name            = name;
		job.Account_Name__c = aId;
		job.Opportunity__c  = oppId;
		job.Job_Number__c   = jobNumber;
		job.Job_Status__c   = 'In Progress - Active';
		return job; 
	}

	public static Job_Quote__c createJobQuote(String jqNumber, Id jobId, Integer phaseValue) {
		return new Job_Quote__c(EGS_Job_Quote__c=jqNumber, Job__c=jobId, 
			Phase_1_Equipment__c=phaseValue, Phase_1_Materials__c=phaseValue, Phase_1_Labor_Hours__c=phaseValue, 
			Phase_2_Equipment__c=phaseValue, Phase_2_Materials__c=phaseValue, Phase_2_Labor_Hours__c=phaseValue,
			Phase_3_Equipment__c=phaseValue, Phase_3_Materials__c=phaseValue, Phase_3_Labor_Hours__c=phaseValue,
			Phase_4_Equipment__c=phaseValue, Phase_4_Materials__c=phaseValue, Phase_4_Labor_Hours__c=phaseValue,
			Phase_5_Equipment__c=phaseValue, Phase_5_Materials__c=phaseValue, Phase_5_Labor_Hours__c=phaseValue);
	}


	public static Job_Staffing__c createJS(Id jobId, String role, Id empId, Boolean isActive) {
		System.assert(Test.isRunningTest());
		Job_Staffing__c js = new Job_Staffing__c();
		js.Job_Name__c     = jobId;
		js.Role__c         = role;
		js.Employee__c     = empId;
		js.IsActive__c     = isActive;
		return js; 
	}

	public static Quote_Job_Phase_Field_Mappings__c createPhaseMapping(Integer index, 
			String srcFieldName, String targetFieldName) {
		return new Quote_Job_Phase_Field_Mappings__c(Name=index + '', Job_Quote_Field_Name__c=srcFieldName,
			Job_Field_Name__c=targetFieldName);	
	}
}