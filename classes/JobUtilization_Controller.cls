/*
** Class:  JobUtilization_Controller
** Created by OpFocus on 10/2/2014
** Description: A controller for Utilized Vs Budgeted - Labor, Materials and Equipments charts on the Job detail page
*/
public without sharing class JobUtilization_Controller {

    public String jobId      {get; set;}
    public Decimal maxLabor1 {get; set;}
    public Decimal maxMat1   {get; set;}
    public Decimal maxEquip1 {get; set;}
    public Decimal maxLabor2 {get; set;}
    public Decimal maxMat2   {get; set;}
    public Decimal maxEquip2 {get; set;}
    public Decimal maxLabor3 {get; set;}
    public Decimal maxMat3   {get; set;}
    public Decimal maxEquip3 {get; set;}
    public Decimal maxLabor4 {get; set;}
    public Decimal maxMat4   {get; set;}
    public Decimal maxEquip4 {get; set;}
    public Decimal maxLabor5 {get; set;}
    public Decimal maxMat5   {get; set;}
    public Decimal maxEquip5 {get; set;}

    public Decimal actualLabor1 {get; set;}
    public Decimal actualMat1   {get; set;}
    public Decimal actualEquip1 {get; set;}
    public Decimal actualLabor2 {get; set;}
    public Decimal actualMat2   {get; set;}
    public Decimal actualEquip2 {get; set;}
    public Decimal actualLabor3 {get; set;}
    public Decimal actualMat3   {get; set;}
    public Decimal actualEquip3 {get; set;}
    public Decimal actualLabor4 {get; set;}
    public Decimal actualMat4   {get; set;}
    public Decimal actualEquip4 {get; set;}
    public Decimal actualLabor5 {get; set;}
    public Decimal actualMat5   {get; set;}
    public Decimal actualEquip5 {get; set;}

	// 75% - 90% splits for green/yellow/red
    public Decimal actualLabor751 {get; set;}
    public Decimal actualLabor901 {get; set;}
    public Decimal actualMat751   {get; set;}
    public Decimal actualMat901   {get; set;}
    public Decimal actualEquip751 {get; set;}
    public Decimal actualEquip901 {get; set;}
    public Decimal actualLabor752 {get; set;}
    public Decimal actualLabor902 {get; set;}
    public Decimal actualMat752   {get; set;}
    public Decimal actualMat902   {get; set;}
    public Decimal actualEquip752 {get; set;}
    public Decimal actualEquip902 {get; set;}
    public Decimal actualLabor753 {get; set;}
    public Decimal actualLabor903 {get; set;}
    public Decimal actualMat753   {get; set;}
    public Decimal actualMat903   {get; set;}
    public Decimal actualEquip753 {get; set;}
    public Decimal actualEquip903 {get; set;}
    public Decimal actualLabor754 {get; set;}
    public Decimal actualLabor904 {get; set;}
    public Decimal actualMat754   {get; set;}
    public Decimal actualMat904   {get; set;}
    public Decimal actualEquip754 {get; set;}
    public Decimal actualEquip904 {get; set;}

    public Decimal p1LaborVal {get; set;}
    public Decimal p1MatVal   {get; set;}
    public Decimal p1EquipVal {get; set;}
    public Decimal p2LaborVal {get; set;}
    public Decimal p2MatVal   {get; set;}
    public Decimal p2EquipVal {get; set;}
    public Decimal p3LaborVal {get; set;}
    public Decimal p3MatVal   {get; set;}
    public Decimal p3EquipVal {get; set;}
    public Decimal p4LaborVal {get; set;}
    public Decimal p4MatVal   {get; set;}
    public Decimal p4EquipVal {get; set;}

    public Decimal allMaxLabor    {get; set;}
    public Decimal allMaxMat      {get; set;}
    public Decimal allMaxEquip    {get; set;}
    public Decimal allActualLabor {get; set;}
    public Decimal allActualMat   {get; set;}
    public Decimal allActualEquip {get; set;}
    public Decimal allLaborVal    {get; set;}
    public Decimal allMatVal      {get; set;}
    public Decimal allEquipVal    {get; set;}
    public Decimal allLabor75     {get; set;}
    public Decimal allLabor90     {get; set;}
    public Decimal allMat75     {get; set;}
    public Decimal allMat90     {get; set;}
    public Decimal allEquip75     {get; set;}
    public Decimal allEquip90     {get; set;}

    public Job__c theJob {get; set;}
 
    public JobUtilization_Controller(ApexPages.StandardController controller){
        jobId = controller.getRecord().Id;
        // Get all the Phase # fields that for the charts
        
        theJob = [Select Name, Phase_1_Utilized_Equipment__c, Phase_1_Utilized_Labor__c, Phase_1_Utilized_Materials__c,
                               Phase_1_Total_Equipment_Budget__c, Phase_1_Total_Labor_Hours__c, Phase_1_Total_Materials_Budget__c, 
                               Phase_2_Utilized_Equipment__c, Phase_2_Utilized_Labor__c, Phase_2_Utilized_Materials__c,
                               Phase_2_Total_Equipment_Budget__c, Phase_2_Total_Labor_Hours__c, Phase_2_Total_Materials_Budget__c,
                               Phase_3_Utilized_Equipment__c, Phase_3_Utilized_Labor__c, Phase_3_Utilized_Materials__c,
                               Phase_3_Total_Equipment_Budget__c, Phase_3_Total_Labor_Hours__c, Phase_3_Total_Materials_Budget__c,
                               Phase_4_Utilized_Equipment__c, Phase_4_Utilized_Labor__c, Phase_4_Utilized_Materials__c,
                               Phase_4_Total_Equipment_Budget__c, Phase_4_Total_Labor_Hours__c, Phase_4_Total_Materials_Budget__c,
                               Total_Budgeted_Equipment__c, Total_Equipment_Used__c, Total_Labor_Hours_Budgeted__c,
                               Total_Labor_Hours_Used__c, Total_Materials_Budgeted__c, Total_Materials_Used__c
                 From   Job__c 
                 Where  Id=:jobId limit 1];

        // Assign the Total numbers for each phase max item (i.e. Phase 1 Total Labor Hours goes to maxLabor1).
        // Set the null field to 0
        maxLabor1 = (theJob.Phase_1_Total_Labor_Hours__c!=null)?theJob.Phase_1_Total_Labor_Hours__c:0;
        maxMat1   = (theJob.Phase_1_Total_Materials_Budget__c!=null)?theJob.Phase_1_Total_Materials_Budget__c:0;
        maxEquip1 = (theJob.Phase_1_Total_Equipment_Budget__c!=null)?theJob.Phase_1_Total_Equipment_Budget__c:0;
        maxLabor2 = (theJob.Phase_2_Total_Labor_Hours__c!=null)?theJob.Phase_2_Total_Labor_Hours__c:0;
        maxMat2   = (theJob.Phase_2_Total_Materials_Budget__c!=null)?theJob.Phase_2_Total_Materials_Budget__c:0;
        maxEquip2 = (theJob.Phase_2_Total_Equipment_Budget__c!=null)?theJob.Phase_2_Total_Equipment_Budget__c:0;
        maxLabor3 = (theJob.Phase_3_Total_Labor_Hours__c!=null)?theJob.Phase_3_Total_Labor_Hours__c:0;
        maxMat3   = (theJob.Phase_3_Total_Materials_Budget__c!=null)?theJob.Phase_3_Total_Materials_Budget__c:0;
        maxEquip3 = (theJob.Phase_3_Total_Equipment_Budget__c!=null)?theJob.Phase_3_Total_Equipment_Budget__c:0;
        maxLabor4 = (theJob.Phase_4_Total_Labor_Hours__c!=null)?theJob.Phase_4_Total_Labor_Hours__c:0;
        maxMat4   = (theJob.Phase_4_Total_Materials_Budget__c!=null)?theJob.Phase_4_Total_Materials_Budget__c:0;
        maxEquip4 = (theJob.Phase_4_Total_Equipment_Budget__c!=null)?theJob.Phase_4_Total_Equipment_Budget__c:0;

        // Assign the Utilized numbers for each phase actual item (i.e. Phase 1 Labor Utilized goes to actualLabor1).
        // Set the null field to 0

        actualLabor1 = (theJob.Phase_1_Utilized_Labor__c!=null)?theJob.Phase_1_Utilized_Labor__c:0;
        actualMat1   = (theJob.Phase_1_Utilized_Materials__c!=null)?theJob.Phase_1_Utilized_Materials__c:0;
        actualEquip1 = (theJob.Phase_1_Utilized_Equipment__c!=null)?theJob.Phase_1_Utilized_Equipment__c:0;
        actualLabor2 = (theJob.Phase_2_Utilized_Labor__c!=null)?theJob.Phase_2_Utilized_Labor__c:0;
        actualMat2   = (theJob.Phase_2_Utilized_Materials__c!=null)?theJob.Phase_2_Utilized_Materials__c:0;
        actualEquip2 = (theJob.Phase_2_Utilized_Equipment__c!=null)?theJob.Phase_2_Utilized_Equipment__c:0;
        actualLabor3 = (theJob.Phase_3_Utilized_Labor__c!=null)?theJob.Phase_3_Utilized_Labor__c:0;
        actualMat3   = (theJob.Phase_3_Utilized_Materials__c!=null)?theJob.Phase_3_Utilized_Materials__c:0;
        actualEquip3 = (theJob.Phase_3_Utilized_Equipment__c!=null)?theJob.Phase_3_Utilized_Equipment__c:0;
        actualLabor4 = (theJob.Phase_4_Utilized_Labor__c!=null)?theJob.Phase_4_Utilized_Labor__c:0;
        actualMat4   = (theJob.Phase_4_Utilized_Materials__c!=null)?theJob.Phase_4_Utilized_Materials__c:0;
        actualEquip4 = (theJob.Phase_4_Utilized_Equipment__c!=null)?theJob.Phase_4_Utilized_Equipment__c:0;

        actualLabor751 = (maxLabor1==0)?0:maxLabor1 * .75;
        actualLabor901 = (maxLabor1==0)?0:maxLabor1 * .90;
        actualMat751 = (maxMat1==0)?0:maxMat1 * .75;
        actualMat901 = (maxMat1==0)?0:maxMat1 * .90;
        actualEquip751 = (maxEquip1==0)?0:maxEquip1 * .75;
        actualEquip901 = (maxEquip1==0)?0:maxEquip1 * .90;
        actualLabor752 = (maxLabor2==0)?0:maxLabor2 * .75;
        actualLabor902 = (maxLabor2==0)?0:maxLabor2 * .90;
        actualMat752 = (maxMat2==0)?0:maxMat2 * .75;
        actualMat902 = (maxMat2==0)?0:maxMat2 * .90;
        actualEquip752 = (maxEquip2==0)?0:maxEquip2 * .75;
        actualEquip902 = (maxEquip2==0)?0:maxEquip2 * .90;
        actualLabor753 = (maxLabor3==0)?0:maxLabor3 * .75;
        actualLabor903 = (maxLabor3==0)?0:maxLabor3 * .90;
        actualMat753 = (maxMat3==0)?0:maxMat3 * .75;
        actualMat903 = (maxMat3==0)?0:maxMat3 * .90;
        actualEquip753 = (maxEquip3==0)?0:maxEquip3 * .75;
        actualEquip903 = (maxEquip3==0)?0:maxEquip3 * .90;
        actualLabor754 = (maxLabor4==0)?0:maxLabor4 * .75;
        actualLabor904 = (maxLabor4==0)?0:maxLabor4 * .90;
        actualMat754 = (maxMat4==0)?0:maxMat4 * .75;
        actualMat904 = (maxMat4==0)?0:maxMat4 * .90;
        actualEquip754 = (maxEquip4==0)?0:maxEquip4 * .75;
        actualEquip904 = (maxEquip4==0)?0:maxEquip4 * .90;


        // Max (total) values for all the phases
        allMaxLabor = (theJob.Total_Labor_Hours_Budgeted__c!=null)?theJob.Total_Labor_Hours_Budgeted__c:0;
        allMaxMat   = (theJob.Total_Materials_Budgeted__c!=null)?theJob.Total_Materials_Budgeted__c:0;
        allMaxEquip = (theJob.Total_Budgeted_Equipment__c!=null)?theJob.Total_Budgeted_Equipment__c:0;
        // Actual (utilized) values for all the phases
        allActualLabor = (theJob.Total_Labor_Hours_Used__c!=null)?theJob.Total_Labor_Hours_Used__c:0;
        allActualMat   = (theJob.Total_Materials_Used__c!=null)?theJob.Total_Materials_Used__c:0;
        allActualEquip = (theJob.Total_Equipment_Used__c!=null)?theJob.Total_Equipment_Used__c:0;

        // Percentage of Utilized per Total budget
        // If the max is 0, then set the percent to 0
        p1LaborVal = (maxLabor1==0)?0:(actualLabor1/maxLabor1)*100;
        p1MatVal   = (maxMat1==0)?0:(actualMat1/maxMat1)*100;
        p1EquipVal = (maxEquip1==0)?0:(actualEquip1/maxEquip1)*100;

        p2LaborVal = (maxLabor2==0)?0:(actualLabor2/maxLabor2)*100;
        p2MatVal   = (maxMat2==0)?0:(actualMat2/maxMat2)*100;
        p2EquipVal = (maxEquip2==0)?0:(actualEquip2/maxEquip2)*100;

        p3LaborVal = (maxLabor3==0)?0:(actualLabor3/maxLabor3)*100;
        p3MatVal   = (maxMat3==0)?0:(actualMat3/maxMat3)*100;
        p3EquipVal = (maxEquip3==0)?0:(actualEquip3/maxEquip3)*100;

        p4LaborVal = (maxLabor4==0)?0:(actualLabor4/maxLabor4)*100;
        p4MatVal   = (maxMat4==0)?0:(actualMat4/maxMat4)*100;
        p4EquipVal = (maxEquip4==0)?0:(actualEquip4/maxEquip4)*100;


        allLaborVal = (allMaxLabor==0)?0:(allActualLabor/allMaxLabor)*100;
        allMatVal   = (allMaxMat==0)?0:(allActualMat/allMaxMat)*100;
        allEquipVal = (allMaxEquip==0)?0:(allActualEquip/allMaxEquip)*100;
        
        allLabor75  = (allMaxLabor==0)?0:allMaxLabor*.75;
        allLabor90  = (allMaxLabor==0)?0:allMaxLabor*.9;
        allMat75  = (allMaxMat==0)?0:allMaxMat*.75;
        allMat90  = (allMaxMat==0)?0:allMaxMat*.9;
        allEquip75  = (allMaxEquip==0)?0:allMaxEquip*.75;
        allEquip90  = (allMaxEquip==0)?0:allMaxEquip*.9;
	   	//ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, 'allLabor75: '+allLabor75));           
    }
}